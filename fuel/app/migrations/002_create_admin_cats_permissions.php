<?php

namespace Fuel\Migrations;

class Create_admin_cats_permissions
{
	public function up()
	{
		\DBUtil::create_table('admin_cats_permissions', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'cats_id' => array('constraint' => 11, 'type' => 'int'),
			'group' => array('constraint' => 3, 'type' => 'tinyint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('admin_cats_permissions');
	}
}