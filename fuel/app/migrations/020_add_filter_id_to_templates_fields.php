<?php

namespace Fuel\Migrations;

class Add_filter_id_to_templates_fields
{
	public function up()
	{
		\DBUtil::add_fields('templates_fields', array(
			'filter_id' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('templates_fields', array(
			'filter_id'
		));
	}
}