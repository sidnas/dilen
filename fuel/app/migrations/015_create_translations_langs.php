<?php

namespace Fuel\Migrations;

class Create_translations_langs
{
	public function up()
	{
		\DBUtil::create_table('translations_langs', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'translation_id' => array('constraint' => 11, 'type' => 'int'),
			'language_id' => array('constraint' => 3, 'type' => 'smallint'),
			'value' => array('type' => 'text', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true)
		), array('id'));
		
		\DBUtil::create_index('translations_langs', array('translation_id', 'language_id'), 'TRANSLATION_LANG', 'unique');
		
	}

	public function down()
	{
		\DBUtil::drop_table('translations_langs');
	}
}