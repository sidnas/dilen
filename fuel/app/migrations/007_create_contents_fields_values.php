<?php

namespace Fuel\Migrations;

class Create_contents_fields_values
{
	public function up()
	{
		\DBUtil::create_table('contents_fields_values', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'content_id' => array('constraint' => 11, 'type' => 'int'),
			'template_field_id' => array('constraint' => 11, 'type' => 'int'),
			'value' => array('type' => 'text', 'null' => true),
			'language_id' => array('constraint' => 3, 'type' => 'smallint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DBUtil::create_index('contents_fields_values', array('content_id', 'template_field_id', 'language_id'), 'CTL', 'unique');

	}

	public function down()
	{
		\DBUtil::drop_table('contents_fields_values');
	}
}