<?php

namespace Fuel\Migrations;

class Create_contents_langs
{
	public function up()
	{
		\DBUtil::create_table('contents_langs', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'content_id' => array('constraint' => 11, 'type' => 'int'),
			'title' => array('constraint' => 255, 'type' => 'varchar'),
			'slug' => array('constraint' => 255, 'type' => 'varchar'),
			'meta_keywords' => array('constraint' => 255, 'type' => 'varchar'),
			'meta_description' => array('constraint' => 255, 'type' => 'varchar'),
			'visible' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'active' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'language_id' => array('constraint' => 3, 'type' => 'smallint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
		
		\DBUtil::create_index('contents_langs', array('content_id', 'language_id'), 'PAGE_ID_LANG', 'unique');
		\DBUtil::create_index('contents_langs', array('slug', 'language_id'), 'SLUG_LANG', 'unique');
		
	}

	public function down()
	{
		\DBUtil::drop_table('contents_langs');
	}
}