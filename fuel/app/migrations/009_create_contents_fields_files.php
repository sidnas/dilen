<?php

namespace Fuel\Migrations;

class Create_contents_fields_files
{
	public function up()
	{
		\DBUtil::create_table('contents_fields_files', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'file_id' => array('constraint' => 11, 'type' => 'int'),
			'content_field_id' => array('constraint' => 11, 'type' => 'int'),
			'order' => array('constraint' => 3, 'type' => 'smallint', 'null' => true),
			'primary' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'custom_value' => array('type' => 'text', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('contents_fields_files');
	}
}