<?php

namespace Fuel\Migrations;

class Create_options
{
	public function up()
	{
		\DBUtil::create_table('options', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'input_name' => array('constraint' => 50, 'type' => 'char'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
			
		\DBUtil::create_index('options', array('input_name'), 'INPUT_NAME', 'unique');
		
	}

	public function down()
	{
		\DBUtil::drop_table('options');
	}
}