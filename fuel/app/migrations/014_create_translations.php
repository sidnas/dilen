<?php

namespace Fuel\Migrations;

class Create_translations
{
	public function up()
	{
		\DBUtil::create_table('translations', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'template_id' => array('constraint' => 11, 'type' => 'int'),						
			'input_name' => array('constraint' => 100, 'type' => 'varchar'),
			'field_type' => array('constraint' => 5, 'type' => 'char', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DBUtil::create_index('translations', array('template_id', 'input_name'), 'TEMPLATE_INPUT', 'unique');
		
	}

	public function down()
	{
		\DBUtil::drop_table('translations');
	}
}