<?php

namespace Fuel\Migrations;

class Create_contents_posters
{
	public function up()
	{
		\DBUtil::create_table('contents_posters', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'content_id' => array('constraint' => 11, 'type' => 'int'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'price' => array('type' => 'float', 'null' => true),
			'quantity' => array('constraint' => 10, 'type' => 'int', 'null' => true),
			'discount' => array('type' => 'float', 'null' => true),
			'discount_from' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'discount_to' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('contents_posters');
	}
}