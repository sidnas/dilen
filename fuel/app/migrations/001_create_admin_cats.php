<?php

namespace Fuel\Migrations;

class Create_admin_cats
{
	public function up()
	{
		\DBUtil::create_table('admin_cats', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'title' => array('constraint' => 50, 'type' => 'varchar'),
			'controller' => array('constraint' => 50, 'type' => 'varchar'),
			'css_class' => array('constraint' => 20, 'type' => 'char', 'null' => true),
			'parent_id' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'order' => array('constraint' => 3, 'type' => 'smallint', 'null' => true),
			'hide' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('admin_cats');
	}
}