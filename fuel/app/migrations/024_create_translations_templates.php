<?php

namespace Fuel\Migrations;

class Create_translations_templates
{
	public function up()
	{
		\DBUtil::create_table('translations_templates', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'translation_id' => array('constraint' => 11, 'type' => 'int'),
			'template_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DBUtil::create_index('translations_templates', array('translation_id', 'template_id'), 'TRANSLATION_TEMPLATE', 'unique');
		
	}

	public function down()
	{
		\DBUtil::drop_table('translations_templates');
	}
}