<?php

namespace Fuel\Migrations;

class Delete_template_id_from_templates
{
	public function up()
	{
		\DBUtil::drop_fields('translations', array(
			'template_id'
		));

	}

	public function down()
	{
		\DBUtil::add_fields('translations', array(
			'template_id' => array('constraint' => 11, 'type' => 'int'),

		));
	}
}