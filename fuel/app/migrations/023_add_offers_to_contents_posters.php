<?php

namespace Fuel\Migrations;

class Add_offers_to_contents_posters
{
	public function up()
	{
		\DBUtil::add_fields('contents_posters', array(
			'allow_best_offer' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'offer_accept' => array('type' => 'float', 'null' => true),
			'offer_decline' => array('type' => 'float', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('contents_posters', array(
			'allow_best_offer'
,			'offer_accept'
,			'offer_decline'

		));
	}
}