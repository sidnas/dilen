<?php

namespace Fuel\Migrations;

class Create_templates
{
	public function up()
	{
		\DBUtil::create_table('templates', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 50, 'type' => 'char'),
			'type' => array('constraint' => 10, 'type' => 'char'),
			'controller' => array('constraint' => 30, 'type' => 'char'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('templates');
	}
}