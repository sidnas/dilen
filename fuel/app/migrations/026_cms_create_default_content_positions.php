<?php 

namespace Fuel\Migrations;

class Cms_create_default_content_positions
{
	private $pos = array(
		array( 
			'id' => 1,
			'title' => 'Main menu',
			'key_name' => 'main_menu',
			'order' => 1
		),
		array( 
			'id' => 2,
			'title' => 'Hidden menu',
			'key_name' => 'hidden',
			'order' => 2
		),
		array( 
			'id' => 3,
			'title' => 'Component',
			'key_name' => 'Component',
			'order' => 3
		)		
	);

	public function up()
	{
		foreach($this->pos as $pos)
		{
			$model_pos = new \Model_Contents_Position($pos);
			$model_pos->save();
		}


	}
	
	public function down()
	{
		foreach($this->pos as $pos)
		{
			$model_pos = \Model_Contents_Position::find($pos->id);
			$model_pos->delete();
		}

	}
	
	
}