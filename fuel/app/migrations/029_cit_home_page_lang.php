<?php

namespace Fuel\Migrations;

class Cit_home_page_lang
{
	public function up()
	{
		$tpl =  array('id' => '1','name' => 'Home page','type' => 'home','controller' => 'home/index');

		if(current(
			\Model_Template::find('all', array(
				'where' => array(
					array('type', '=', 'home')
				)
			))
		))
		{
			\Cli::write('home page already exists');
			return false;
		}


		$template = new \Model_Template($tpl);
		$template->save();

		$tpl_field = array('id' => '1','template_id' => $template->id,'name' => 'tmp','key_name' => 'tmp','type' => 'text_field','custom_value' => '','searchable' => '0','active' => '1','required' => '0','order' => '1','filter_id' => '0');
		$template_field = new \Model_Templates_Field($tpl_field);
		$template_field->save();

		$tpl_parent = array('id' => '1','template_id' => $template->id,'parent_template_id' => '0');
		$template_parent = new \Model_Templates_Parent($tpl_parent);
		$template_parent->save();

		$lang = \Model_Language::find('all', array(
			'where' => array(
				array('val', '=', 'eng')
			)
		));

		$lang = current($lang);

		$lang->val = 'lv';
		$lang->title = 'Latviešu';
		$lang->save();

	}

	public function down()
	{
		

	}
}