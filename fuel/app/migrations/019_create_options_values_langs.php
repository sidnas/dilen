<?php

namespace Fuel\Migrations;

class Create_options_values_langs
{
	public function up()
	{
		\DBUtil::create_table('options_values_langs', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'option_value_id' => array('constraint' => 11, 'type' => 'int'),
			'value' => array('type' => 'varchar', 'constraint' => 100, 'null' => true),
			'slug' => array('type' => 'varchar', 'constraint' => 100, 'null' => true),
			'language_id' => array('constraint' => 3, 'type' => 'smallint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DBUtil::create_index('options_values_langs', array('option_value_id', 'language_id'), 'OPTION_VALUE_LANG', 'unique');
		\DBUtil::create_index('options_values_langs', array('slug', 'language_id'), 'SLUG_ID_LANG', 'unique');
		
		
	}

	public function down()
	{
		\DBUtil::drop_table('options_values_langs');
	}
}