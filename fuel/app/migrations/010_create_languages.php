<?php

namespace Fuel\Migrations;

class Create_languages
{
	public function up()
	{
		\DBUtil::create_table('languages', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'title' => array('constraint' => 50, 'type' => 'char'),
			'val' => array('constraint' => 5, 'type' => 'char'),
			'icon' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'primary' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'active' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DB::insert('languages')->set(array(
			'title' => 'English',
			'val' => 'eng',
			'primary' => 1,
			'active' => 1
		))->execute();
		
	}

	public function down()
	{
		\DBUtil::drop_table('languages');
	}
}