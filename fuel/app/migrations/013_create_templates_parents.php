<?php

namespace Fuel\Migrations;

class Create_templates_parents
{
	public function up()
	{
		\DBUtil::create_table('templates_parents', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'template_id' => array('constraint' => 11, 'type' => 'int'),
			'parent_template_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('templates_parents');
	}
}