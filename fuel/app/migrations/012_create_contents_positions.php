<?php

namespace Fuel\Migrations;

class Create_contents_positions
{
	public function up()
	{
		\DBUtil::create_table('contents_positions', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'title' => array('constraint' => 25, 'type' => 'char'),
			'key_name' => array('constraint' => 25, 'type' => 'char'),
			'order' => array('constraint' => 2, 'type' => 'tinyint', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('contents_positions');
	}
}