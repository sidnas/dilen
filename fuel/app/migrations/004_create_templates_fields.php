<?php

namespace Fuel\Migrations;

class Create_templates_fields
{
	public function up()
	{
		\DBUtil::create_table('templates_fields', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'template_id' => array('constraint' => 11, 'type' => 'int'),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'key_name' => array('constraint' => 50, 'type' => 'char'),
			'type' => array('constraint' => 20, 'type' => 'char'),
			'custom_value' => array('type' => 'text', 'null' => true),
			'searchable' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'active' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'required' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'order' => array('constraint' => 5, 'type' => 'smallint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('templates_fields');
	}
}