<?php 

namespace Fuel\Migrations;

class Cms_create_admin_cats
{
	private $cats = array(
		array('id' => '1','title' => 'Dashboard','controller' => 'dashboard','css_class' => 'fa-dashboard','parent_id' => '0','order' => '0','hide' => '0'),
		array('id' => '2','title' => 'Contents','controller' => 'content','css_class' => 'fa-align-justify','parent_id' => '0','order' => '2','hide' => '0'),
		array('id' => '3','title' => 'Create','controller' => 'content/create','css_class' => '','parent_id' => '2','order' => '1','hide' => '1'),
		array('id' => '4','title' => 'Edit','controller' => 'content/edit','css_class' => '','parent_id' => '2','order' => '2','hide' => '1'),
		array('id' => '5','title' => 'Developer','controller' => 'developer','css_class' => 'fa-coffee','parent_id' => '0','order' => '10','hide' => '0'),
		array('id' => '6','title' => 'Templates','controller' => 'template','css_class' => 'fa-suitcase','parent_id' => '0','order' => '9','hide' => '0'),
		array('id' => '7','title' => 'Create','controller' => 'template/create','css_class' => '','parent_id' => '6','order' => '1','hide' => '0'),
		array('id' => '8','title' => 'Edit','controller' => 'template/edit','css_class' => '','parent_id' => '6','order' => '2','hide' => '1'),
		array('id' => '9','title' => 'Upload','controller' => 'upload','css_class' => '','parent_id' => '-1','order' => '0','hide' => '1'),
		array('id' => '10','title' => 'Languages','controller' => 'language','css_class' => 'fa-flag','parent_id' => '0','order' => '3','hide' => '0'),
		array('id' => '11','title' => 'Parents','controller' => 'template/parents','css_class' => '','parent_id' => '6','order' => '3','hide' => '0'),
		array('id' => '12','title' => 'User groups','controller' => 'developer/user_groups','css_class' => '','parent_id' => '5','order' => '1','hide' => '0'),
		array('id' => '13','title' => 'Users','controller' => 'user','css_class' => 'fa-users','parent_id' => '0','order' => '1','hide' => '0'),
		array('id' => '14','title' => 'Options','controller' => 'option','css_class' => 'fa-filter','parent_id' => '0','order' => '4','hide' => '0'),
		array('id' => '15','title' => 'Create','controller' => 'option/create','css_class' => '','parent_id' => '14','order' => '1','hide' => '0'),
		array('id' => '16','title' => 'Edit','controller' => 'option/edit','css_class' => '','parent_id' => '14','order' => '2','hide' => '1')
	);

	public function up()
	{
		foreach($this->cats as $cat)
		{
			$model_admin_cat = new \Model_Admin_Cat($cat);
			$model_admin_cat->save();

			$model_admin_cat_permission = new \Model_Admin_Cats_Permission(array(
				'cats_id' => $model_admin_cat->id,
				'group' => 100
			));
			$model_admin_cat_permission->save();
		}

	}
	
	public function down()
	{
		foreach($this->cats as $cat)
		{
			$model_admin_cat = \Model_Admin_Cat::find($cat['id']);
			$model_admin_cat->delete();

			$model_admin_cat_permission = current(\Model_Admin_Cats_Permission::find(array(
				'cats_id' => $model_admin_cat->id
			)));
			$model_admin_cat_permission->delete();
		}
	}
	
	
}