<?php

namespace Fuel\Migrations;

class Add_fullname_to_users
{
	public function up()
	{
		\DBUtil::add_fields('users', array(
			'fullname' => array('constraint' => 100, 'type' => 'varchar', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('users', array(
			'fullname'

		));
	}
}