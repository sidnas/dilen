<?php

namespace Fuel\Migrations;

class Create_options_langs
{
	public function up()
	{
		\DBUtil::create_table('options_langs', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'option_id' => array('constraint' => 11, 'type' => 'int'),
			'name' => array('constraint' => 100, 'type' => 'varchar'),
			'slug' => array('type' => 'varchar', 'constraint' => 100, 'null' => true),			
			'language_id' => array('constraint' => 3, 'type' => 'smallint'),			
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DBUtil::create_index('options_langs', array('option_id', 'language_id'), 'OPTION_ID_LANG', 'unique');
		\DBUtil::create_index('options_langs', array('slug', 'language_id'), 'SLUG_ID_LANG', 'unique');

	}

	public function down()
	{
		\DBUtil::drop_table('options_langs');
	}
}