<?php

namespace Fuel\Migrations;

class Create_contents_filters
{
	public function up()
	{
		\DBUtil::create_table('contents_filters', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'content_id' => array('constraint' => 11, 'type' => 'int'),
			'option_id' => array('constraint' => 11, 'type' => 'int'),
			'option_value_id' => array('constraint' => 11, 'type' => 'int'),
			'priority' => array('constraint' => 2, 'type' => 'tinyint'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('contents_filters');
	}
}