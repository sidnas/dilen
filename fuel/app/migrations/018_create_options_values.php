<?php

namespace Fuel\Migrations;

class Create_options_values
{
	public function up()
	{
		\DBUtil::create_table('options_values', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'option_id' => array('constraint' => 11, 'type' => 'int'),
			'order' => array('constraint' => 5, 'type' => 'int'),
			'equal_lang' => array('constraint' => 1, 'type' => 'tinyint', 'null' => true),
			'parent_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
		
		\DBUtil::create_index('options_values', array('option_id'), 'OPTION_ID', 'index');
		
		
	}

	public function down()
	{
		\DBUtil::drop_table('options_values');
	}
}