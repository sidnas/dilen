<?php

namespace Fuel\Migrations;

class Cit_create_header_component
{
	public function up()
	{
		$tpl =  array('name' => 'Header','type' => 'component','controller' => 'header');

		if(current(
			\Model_Template::find('all', array(
				'where' => array(
					array('type', '=', $tpl['type'])
				)
			))
		))
		{
			\Cli::write('Template already exists');
			return true;
		}


		$template = new \Model_Template($tpl);
		$template->save();

		$tpl_fields =   array(
			array('template_id' => $template->id,'name' => 'Facebook url','key_name' => 'facebook_url','type' => 'text_field','custom_value' => '','searchable' => '0','active' => '1','required' => '0','order' => '1', 'filter_id' => '0'),
			array('template_id' => $template->id,'name' => 'Twitter url','key_name' => 'twitter_url','type' => 'text_field','custom_value' => '','searchable' => '0','active' => '1','required' => '0','order' => '2','filter_id' => '0'),
			array('template_id' => $template->id,'name' => 'Dribbble url','key_name' => 'dribbble_url','type' => 'text_field','custom_value' => '','searchable' => '0','active' => '1','required' => '0','order' => '3','filter_id' => '0'),
			array('template_id' => $template->id,'name' => 'Logo','key_name' => 'logo','type' => 'images','custom_value' => '{
			"resize" : [
			{
			"width" : 315,
			"height" : 95,
			"folder" : "logo"
			}		
			]
			}','searchable' => '0','active' => '1','required' => '0','order' => '4','created_at' => '1416735572','updated_at' => '1416736211','filter_id' => '0'),
			array('template_id' => $template->id,'name' => 'Header background','key_name' => 'header_bg','type' => 'color_picker','custom_value' => '','searchable' => '0','active' => '1','required' => '0','order' => '5','filter_id' => '0')

  	);
		foreach($tpl_fields as $tpl_field)
		{
			$template_field = new \Model_Templates_Field($tpl_field);
			$template_field->save();			
		}


		$tpl_parent = array('template_id' => $template->id,'parent_template_id' => '0');
		$template_parent = new \Model_Templates_Parent($tpl_parent);
		$template_parent->save();


	}

	public function down()
	{
		

	}
}