<?php

namespace Fuel\Migrations;

class Create_contents_tree_nodes
{
	public function up()
	{
		\DBUtil::create_table('contents_tree_nodes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'content_id' => array('constraint' => 11, 'type' => 'int'),
			'parent_content_id' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'position' => array('constraint' => 10, 'type' => 'int', 'null' => true),
			'order' => array('constraint' => 4, 'type' => 'smallint', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('contents_tree_nodes');
	}
}