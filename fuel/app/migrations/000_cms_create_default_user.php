<?php 

namespace Fuel\Migrations;

class Cms_create_default_user
{
	public function up()
	{
		$this->auth = \Auth::instance('Simpleauth');
		
		$this->auth->create_user('admin', '62cc2d8b4bf2d8728120d052163a77df', 'admin@admin.com', 100);

	}
	
	public function down()
	{
		$this->auth = \Auth::instance('Simpleauth');
		$this->auth->delete_user('admin');
		
		
	}
	
	
}