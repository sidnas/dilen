<?php
/**
 * Frype strategy for Opauth
 * More information on Opauth: http://opauth.org
 * 
 * @copyright    Copyright © 2013 Matiss Roberts Treinis
 * @link         http://marolind.com
 * @license      MIT License
 */
class FrypeStrategy extends OpauthStrategy {

	/**
	 * Compulsory config keys, listed as unassociative arrays
	 */
	public $expects = array (
			'app_id',
			'app_secret' 
	);

	/**
	 * Optional config keys, without predefining any default values.
	 */
	public $optionals = array (
			'redirect_uri' 
	);

	/**
	 * Optional config keys with respective default values, listed as associative arrays
	 * eg.
	 * array('scope' => 'email');
	 */
	public $defaults = array (
			'redirect_uri' => '{complete_url_to_strategy}oauth2callback' 
	);

	/**
	 * Auth request
	 */
	public function request() {
		$draugiem = new DraugiemApi ( $this->strategy ['app_id'], $this->strategy ['app_secret'] );
		$session = $draugiem->getSession ();
		if ($session == false || empty ( $_GET ['dr_auth_code'] )) {
			$hash = md5 ( $this->strategy ['app_secret'] . $this->strategy ['redirect_uri'] );
			$url = 'http://api.draugiem.lv/authorize/' . '?app=' . $this->strategy ['app_id'] . '&hash=' . $hash . '&redirect=' . urlencode ( $this->strategy ['redirect_uri'] );
			header('Location: '.$url);
		}
	}

	/**
	 * Internal callback, after auth
	 */
	public function oauth2callback() {
		$draugiem = new DraugiemApi ( $this->strategy ['app_id'], $this->strategy ['app_secret'] );
		$session = $draugiem->getSession (); // Try to authenticate user
		if ($session && ! empty ( $_GET ['dr_auth_code'] )) {
			$userinfo = $draugiem->getUserData ();
			if ($userinfo) {
				$this->auth = array (
						'uid' => $userinfo ['uid'],
						'info' => array (
								'name' => $userinfo ['name'] . ' ' . $userinfo ['surname'],
								'first_name' => $userinfo ['name'],
								'last_name' => $userinfo ['surname'],
								'image' => $userinfo ['img'],
								'location' => $userinfo ['place'],
								'nickname' => $userinfo ['nick'] 
						)
						,
						'raw' => $userinfo 
				);
				return $this->callback ();
			}
		}
		$error = array (
				'code' => 'auth_error',
				'message' => 'Authentication failed',
				'raw' => array (
						'response' => $response,
						'headers' => $headers
				)
		);		
		$this->errorCallback ( $error );
	}

}