<?php
return array(
	'_root_'  => 'welcome/index',  // The default route

	
	'auth/register' => 'auth/register',
	'auth/session/(:provider)' => 'auth/session/$1',
	'auth/callback/(:provider)' => 'auth/callback/$1',	
	
	'product/(:any)' => 'web/product/$1',
	'(:any)/ajax/(:any)' => 'web/ajax/$2',
	'admin' => 'admin/base/login',
	'admin/login' => 'admin/base/login',	
	'admin/(:any)' => 'admin/$1',

	'(:any)' => 'web/main',
	'_root_'  => 'web/main',  // The default route	
	
	'_404_'   => 'welcome/404',    // The main 404 route	
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
);