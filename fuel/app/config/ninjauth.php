<?php
/**
 * Configuration for NinjAuth
 */
return array(

	/**
	 * Adapter
	 * 
	 * NinjAuth can use different adapters, so it will work with 'auth', 'sentry' or 'warden'.
	 */
	'adapter' => 'SimpleAuth',

	/**
	 * Providers
	 * 
	 * Providers such as Facebook, Twitter, etc all use different Strategies such as oAuth, oAuth2, etc.
	 * oAuth takes a key and a secret, oAuth2 takes a (client) id and a secret, optionally a scope.
	 */
	'providers' => array(
		
		'facebook' => array(			
			'id' => '730281290358645',
			'secret' => '8678ec90239d3c09f5823c08a1f41df9',
			/*
			'id' => '257694220918221',
			'secret' => '4163b15f027b1eec90a298f13d658337',
			*/
			'scope' => array('email', 'offline_access'),
			//'app_url' => 'http://apps.facebook.com/test-hashtag/'
		),
		
		'frype' => array(
			'id' => '15017939',
			'secret' => 'b5010a8871db9653bee37fa3acb043f1'
		),	
		
		'twitter' => array(
			'key' => 'oQXj6oxxQLuvEp8bWoeNsIjsd',
			'secret' => 'CJYqHI9e2wdbGakO5L1MFNoCJtDcxBCGMCGvobCgPgibepoB1j',
			'user_token' => '111980811-ykxj89YlWinkZ5yqxx3wjAcBjtoMvLfyc2hI5RnZ',
			'user_secret' => 'G964qhArgeik60NnDuySDOClkUKSOKY8PmQ3NQGL4p1Jg',
			'callback' => \Uri::create('auth/callback/twitter')
		),

		'dropbox' => array(
			'key' => '',
			'secret' => '',
		),

		'linkedin' => array(
			'key' => '',
			'secret' => '',
		),

		'flickr' => array(
			'key' => '',
			'secret' => '',
		),

		'youtube' => array(
			'key' => '',
			'scope' => 'http://gdata.youtube.com',
		),

		'openid' => array (
			'identifier_form_name' => 'openid_identifier',
			'ax_required' => array('contact/email', 'namePerson/first', 'namePerson/last'),
			'ax_optional' => array('namePerson/friendly', 'birthDate', 'person/gender', 'contact/country/home'),
		),

	),

	/**
	 * link_multiple_providers
	 * 
	 * Can multiple providers be attached to one user account
	 */
	'link_multiple_providers' => true,

	/**
	 * default_group
	 * 
	 * How should users be signed up
	 */
	'default_group' => 1,
);
