<?php

namespace Controller;

class Rules{

	public static function _validation_category_exists($category_id)
	{
		if(\Model_Content::find($category_id, array(
			'related' => array(
				'templates' => array(
					'where' => array(
						array('type', 'in', array('view_base', 'view_cat'))
					)
				)
			)
		))) return true;
		return false;
	}

	public static function _validation_region_exists($regions_id)
	{	
		if(\Model_Options_Value::find($regions_id)) return true;
		return false;
	
	}

	public static function _validation_email_dont_exists($email)
	{
		if(!current(\Model_User::find_by('email', $email))) return true;
		return false;
	}

	public static function _validation_email_exists($email)
	{
		if(current(\Model_User::find_by('email', $email))) return true;
		return false;
	}

}