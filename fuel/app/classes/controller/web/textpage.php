<?php
	class Controller_Web_Textpage extends Controller_Web_Base
	{
		private $current_page;
		
		public function before()
		{
			parent::before();
			
			$this->current_page = \Model_Content::get_by_content_id($this->current_page_id, $this->current_language['id'], true);
			$this->update_tempalte_values($this->current_page);
		}

		public function action_index()
		{
			$view = array();
			$view['current_page'] = $this->current_page;
			$this->template->content = \View::forge('web/textpage/index', $view, false);
		}

		public function action_contact()
		{
			$view = array();
			$view['current_page'] = $this->current_page;
			$this->template->content = \View::forge('web/textpage/contact', $view, false);
		}
	}
