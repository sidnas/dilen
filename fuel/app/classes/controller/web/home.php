<?php

class Controller_Web_Home extends Controller_Web_Base {

    public function before() {
        parent::before();
        $this->current_page = \Model_Select_Content::get_by_content_ids($this->current_page_id, $this->current_language['id']);
        /* Updates meta tags, title... */
        $this->update_tempalte_values($this->current_page);

    }

    public function action_index() {
    
    
        $view = array();

//            $this->template->css = array(
//                'home_news.css',
//                'tpl_slider.css',
//                'tpl_boxes.css',
//                'tpl_counters.css'
//                );

        
        $content_lists = \Model_Select_Content::get_list($this->current_language['id'], 1, 0);
        $contents = array();

        foreach ($content_lists as $content) {
            $contents[$content['type']] = array(
                'title' => $content['title'],
                'slug' => $content['slug'],
                'value' => $content['value']
            );
        }

//        echo '<pre>' . print_r($contents, 1) . '</pre>';
//        die();
        
        $offerCats = array();
        $offerProd = array();
        $offerHolderId = 0;

        foreach ($content_lists as $content) {
            if ($content['type'] == 'holder') {
                $offerHolderId = $content['id'];
            }
        }

        if ($offerHolderId > 0) {
            $offerCats = \Model_Select_Content::get_list($this->current_language['id'], 1, $offerHolderId);

            foreach ($offerCats as $category) {
                $offerProd[$category['slug']] = \Model_Select_Content::get_list($this->current_language['id'], 1, $category['id']);
            }
        }

        //echo '<pre>'.print_r($offers,1).'</pre>';

        $view['contents'] = $contents;
        $view['offers'] = array(
            'offerCats' => $offerCats,
            'offerProd' => $offerProd
        );
        
        //echo '<pre>'.print_r($this->current_page,1).'</pre>';

        $this->template->content = \View::forge('web/home/index', $view, false);
    }

}