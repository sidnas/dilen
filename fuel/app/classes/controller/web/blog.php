<?php
	class Controller_Web_Blog extends Controller_Web_Base
	{
		private $current_page;
		
		private $multi_images = null;

		private $sub = null;

		public function before()
		{
			parent::before();
			
			$this->current_page = \Model_Content::get_by_content_id($this->current_page_id, $this->current_language['id'], true);
			$this->update_tempalte_values($this->current_page);
		}

		/**
		 * All blog article views
		 * @param  int $parent_page_id if is integere, returens all childs if this parent page id
		 * @return response 
		 */
		public function action_index($parent_page_id = null)
		{
			$view = array();
			$view['current_page'] = $this->current_page;

			if(!is_numeric($parent_page_id))
			{
				$parent_page_id = $this->current_page['id'];
			}

			$view['blog_items'] = Model_Select_Content::get_list($this->current_language->id, 1, $parent_page_id);

			$view['sub'] = $this->sub;

			$this->template->content = \View::forge('web/blog/index'.($this->multi_images ? '_multi_images' : ''), $view, false);
		}



		/**
		 * Collects sub categories, fetch`s active subcategory
		 * @return $this->action_index();
		 */
		public function action_page_groups()
		{
			$sub_cats = array();
			$active_sub = $this->uri[3];
			$active_page_id = $this->current_page['id'];

			$i = 0;
			foreach(Model_Select_Content::get_list($this->current_language->id, 1, $this->current_page['id']) as $page)
			{
				$sub_cats[$page['slug']] = $page;
				$i++;
			}
			
			if(!empty($sub_cats))
			{
				if(!empty($sub_cats[$active_sub]))
				{
					$this->sub = $sub_cats[$active_sub];
					$active_page_id = $sub_cats[$active_sub]['id'];
					
				}
				else
				{
					$this->sub = current($sub_cats);

					$active_sub = $this->sub['slug'];
					$active_page_id = $this->sub['id'];
				}	
				$sub_cats[$active_sub]['active'] = true;			
			}

			$this->template->sub_menu = $sub_cats;

			if(!empty($this->sub['value']['multiple_images']))
			{
				$this->multi_images = true;
			}

			if(!empty($sub_cats[$active_sub]))
			{
				$this->update_tempalte_values($sub_cats[$active_sub], false);
			}
			return $this->action_index($active_page_id);
		}

		public function action_events()
		{
			$view = array();

			$sub_menu = array(
				'current' => array('id' => 1, 'slug' => 'current', 'title' => $this->main_translation->get('next_events', 'Nākošie')),
				'prev' => array('id' => 2, 'slug' => 'prev', 'title' => $this->main_translation->get('previous_events', 'Notikušie'))
			);
			
			$active_sub = 'current';
			if(!empty($sub_menu[$this->uri[3]]))
			{
				$active_sub = $this->uri[3];
			}

			$sub_menu[$active_sub]['active'] = true;

			$this->update_tempalte_values($sub_menu[$active_sub], false);
			
			$this->template->sub_menu = $sub_menu;

			$view['active_sub'] = $active_sub;
			$view['current_page'] = $this->current_page;


			$params = array();

			$where_elem = '>=';
			if($active_sub != 'current')
			{
				$where_elem = '<';
			}

			$ids = array();
			foreach(\DB::select('contents_tree_nodes.content_id')
				->from('contents_tree_nodes')
					->join('contents')
						->on('contents_tree_nodes.content_id', '=', 'contents.id')
					->join('contents_fields_values')
						->on('contents_fields_values.content_id', '=', 'contents.id')
					->join('templates_fields')
						->on('templates_fields.id', '=', 'contents_fields_values.template_field_id')
				->where('contents_tree_nodes.parent_content_id', '=', $this->current_page['id'])
				->and_where('contents_fields_values.value', $where_elem, time())
				->and_where('templates_fields.key_name', '=', 'date')
				->order_by('contents_fields_values.value', 'asc')->execute()->as_array() as $node)
				{
					$ids[] = $node['content_id'];
				}

			$blog_items = array();
			if(!empty($ids))
			{

				$params['where'] = array(
					array('where', 'content_id', 'in', $ids)
				);	
				$params['order_by'] = array(\DB::expr('field(t0.content_id, '.implode(',', $ids).')'), '');				
				$blog_items = Model_Select_Content::get_list($this->current_language->id, 1, $this->current_page['id'], $params);
			}

			$view['blog_items'] = $blog_items;
			$this->template->content = \View::forge('web/blog/events', $view, false);
		}
	}
