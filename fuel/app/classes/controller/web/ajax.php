<?php 
	class Controller_Web_Ajax extends Controller_Web_Base
	{
		public function before()
		{	
			parent::before();
		}
	
		public function action_load_parent_values($option_value_id, $uri_filter_id = false)
		{
			$status = array(
				'c' => 0,
				'option_value_id' => $option_value_id
			);
	
			if(empty($option_value_id) || !\Input::post('filter_id')) $this->_exit($status);
			
			$values = Model_Options_Value::get_values(\Input::post('filter_id'), $this->current_language->id, $option_value_id);
			
			if(empty($values)) $this->_exit($status);
			$status['c'] = 1;
			$status['values'] = $values;

			if($uri_filter_id)
			{
				$_values = array();
				foreach($values as $v)
				{
					$_values[$v['id']] = !empty($v['option_value_langs']['value']) ? $v['option_value_langs']['value'] : $v['option_value_langs']['value_placeholder'];
				}
				$status['values'] = $_values;
			}
			
			
			
			
			$this->_exit($status);
		}	
	
	
	
	
	
	
	
		private function _exit($status)
		{
			exit(\Format::forge($status)->to_json());
		}
	}