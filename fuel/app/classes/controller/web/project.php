<?php
	class Controller_Web_Project extends Controller_Web_Base
	{
		private $current_page;
		
		public function before()
		{
			parent::before();
			
			$this->current_page = \Model_Content::get_by_content_id($this->current_page_id, $this->current_language['id'], true);
			$this->update_tempalte_values($this->current_page);
		}

		public function action_index()
		{
			$view = array();
			$view['current_page'] = $this->current_page;

			$active_sub = 'actual';

			$sub_menu = array(
				'actual' => array('id' => 1, 'slug' => 'actual', 'title' => $this->main_translation->get('actual', 'Aktuālie')),
				'archive' => array('id' => 2, 'slug' => 'archive', 'title' => $this->main_translation->get('archive', 'Arhīvs'))
			);

			if(!empty($sub_menu[$this->uri[3]]))
			{
				$active_sub = $this->uri[3];
			}

			$sub_menu[$active_sub]['active'] = true;

			$this->update_tempalte_values($sub_menu[$active_sub], false);

			$this->template->sub_menu = $sub_menu;	

			if($this->uri[4] && $lang = current(Model_Contents_Lang::find('all', array(
				'where' => array(
					array('slug', '=', $this->uri[4]),
					array('language_id', '=', $this->current_language->id)
				)
			))))
			{
				return $this->action_single($lang->content_id);

			}

			$params = array();

			$where_elem = '>=';
			if($active_sub != 'actual')
			{
				$where_elem = '<';
			}

			$ids = array();
			foreach(\DB::select('contents_tree_nodes.content_id')
				->from('contents_tree_nodes')
					->join('contents')
						->on('contents_tree_nodes.content_id', '=', 'contents.id')
					->join('contents_fields_values')
						->on('contents_fields_values.content_id', '=', 'contents.id')
					->join('templates_fields')
						->on('templates_fields.id', '=', 'contents_fields_values.template_field_id')
				->where('contents_tree_nodes.parent_content_id', '=', $this->current_page['id'])
				->and_where('contents_fields_values.value', $where_elem, time())
				->and_where('templates_fields.key_name', '=', 'date')
				->order_by('contents_fields_values.value', 'asc')->execute()->as_array() as $node)
				{
					$ids[] = $node['content_id'];
				}

			$blog_items = array();
			if(!empty($ids))
			{

				$params['where'] = array(
					array('where', 'content_id', 'in', $ids)
				);	
				$params['order_by'] = array(\DB::expr('field(t0.content_id, '.implode(',', $ids).')'), '');				
				$blog_items = Model_Select_Content::get_list($this->current_language->id, 1, $this->current_page['id'], $params);
			}


			$view['active_sub'] = $active_sub;
			$view['blog_items'] = $blog_items;

			$this->template->content = \View::forge('web/project/index', $view, false);
		}

		public function action_single($project_id)
		{
			$view = array();

			if(!is_numeric($project_id))
			{
				$project_id = $this->current_page['id'];
			}

			$project = \Model_Content::get_by_content_id($project_id, $this->current_language['id'], true, false);
			$this->update_tempalte_values($project);

			$view['article'] = $project;

			$this->template->content = \View::forge('web/project/single', $view, false);
		}
	}
