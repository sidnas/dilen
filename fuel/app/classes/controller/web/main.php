<?php

class Controller_Web_Main extends Controller
{
	public $uri;
	
	private $spec_uris = array(
		'do-search' => 'search' /* Redirect to search page*/
	);
	
	private $current_language = null;
	
	public function __construct()
	{
		$this->uri = array(
			1 => \Uri::segment(1),
			2 => \Uri::segment(2),
			3 => \Uri::segment(3),
			4 => \Uri::segment(4),
			5 => \Uri::segment(5),
			6 => \Uri::segment(6)
		);
	}

	public function action_index()
	{

		$current_language = $this->current_language = $this->get_language($this->uri[1]);
		
		if(!empty($this->spec_uris[$this->uri[2]])) $this->redirect_to_spec_uri($this->spec_uris[$this->uri[2]]);
		
		
		$routing = \Model\Routing::target($current_language->id, $this->uri[2], $this->uri[3], $this->uri[4]);

		return \Request::forge($routing['controller'], false)->execute(array(
			'uri' => $this->uri,
			'current_page_id' => $routing['id'],
			'current_template_id' => $routing['template_id'],
			'current_language' => $current_language,
			'main_translation' => $routing['main_translation']
		));
		
	}

	private function redirect_to_spec_uri($type)
	{
		$url = '';
		if($type == 'search')
		{
			$url .= \Input::post('search_for');
			$url .= (int)\Input::post('search_in') > 0 ? '?search_in='.\Input::post('search_in') : '';
		}
		
		if($content = \Model_Template::get_content_by_type($type, $this->current_language->id))
		{
			\Response::redirect(\Uri::create($this->current_language->val.'/'.$content['basic']['slug'].'/'.$url), 'location', 301);
		}
		
		return null;
		
	}

	public function get_language($uri = null)
	{
		$language = null;
		if(!$uri) $uri = \Uri::segment(1);
		
		if($uri)
		{
			$language = current(Model_Language::find('all', array(
				'where' => array(
					array('val', '=', $uri),
					array('active', '=', 1)
				)
			)));
		}
		
		if(empty($language->id))
		{
			$language = current(Model_Language::find('all', array(
				'where' => array(
					array('primary', '=', 1),
					array('active', '=', 1)
				)
			)));
		}
		
		if(empty($language->id))
		{
			$language = current(Model_Language::find('all', array(
				'where' => array(
					array('active', '=', 1)
				)
			)));
		}
		
		return $language;
		
	}	
	
}

