<?php

class Controller_Web_Base extends Controller_Template{

	public $template = 'web/template';
	
	public $current_language = null;

	public $auth = null;

	public $main_categories;
	
	public $current_user = null;
	
	public $main_translation = null;
	
	public function before()
	{
		parent::before();

		
		
		if($referrer = \Session::get('referrer'))
		{
			\Session::delete('referrer');
			\Response::redirect($referrer);
		}
		
		$this->auth = \Auth::instance('Simpleauth');	
		
		if(\Input::get('logout'))
		{
			$this->auth->logout();
			\Response::redirect(\Input::referrer());
		}

		$id_info = $this->auth->get_user_id();

		$this->current_user = !empty($id_info[1]) ? Model_User::get_single($id_info[1]) : null;

		\View::set_global('current_user', $this->current_user);

		
		if(\Request::is_hmvc())
		{
			$this->uri = \Request::active()->method_params['uri'];
			$this->current_page_id = \Request::active()->method_params['current_page_id'];
			$this->current_template_id = \Request::active()->method_params['current_template_id'];
			$this->current_language = \Request::active()->method_params['current_language'];
			$this->main_translation = 	\Request::active()->method_params['main_translation'];
		}
		else
		{
			$this->uri = array(
				1 => \Uri::segment(1),
				2 => \Uri::segment(2),
				3 => \Uri::segment(3),
				4 => \Uri::segment(4),
				5 => \Uri::segment(5),
				6 => \Uri::segment(6)
			);

			$home_page = \Model\Routing::home_page();

			$this->current_page_id = $home_page['current_page_id'];
			
			$this->current_template_id = $home_page['template_id'];
			
			$this->main_translation = $home_page['main_translation'];
			
			$mc = new Controller_Web_Main;
			$this->current_language = $mc->get_language($this->uri[1]);			

		}			

		if(\Input::is_ajax()) $this->ajax();

		\View::set_global('main_translation', $this->main_translation, false);
		

		\View::set_global('uri', $this->uri);
		\View::set_global('current_language', $this->current_language);		
		\View::set_global('current_page_id', $this->current_page_id);
		
		$languages = array();
		foreach(Model_Language::find('all', array(
			'where' => array(
				array('active', '=', 1)
			),
			'order_by' => array(
				array('primary', 'desc'),
				array('id', 'desc')
			)
		)) as $v)
		{
			$languages[$v->id] = array(
				'id' => $v->id,
				'val' => $v->val,
				'title' => $v->title,
				'icon' =>\Uri::create('upload/icons/'.$v->icon)
			);
		}
		
		$this->languages = $languages;

		$this->main_menu = Model_Select_Content::get_list($this->current_language->id, 1, 0);

		\View::set_global('main_menu', $this->main_menu);
		\View::set_global('languages', $this->languages);

	}

	/**
	 * update basic tempalte values
	 * @param  array $current_page
	 * @param  bool $update_current_page //if tru updates current_page for global access
	 * @param  array  $params
	 * @return null
	 */
	public function update_tempalte_values($current_page, $update_current_page = true, $params = array())
	{
		if(!empty($current_page['id']))
		{
			/* If title already is set, then popuplate the title text */
			if(!empty($this->template->title))
			{
				$this->template->title .= ' - '.$current_page['title'];
			}
			else
			{
				$this->template->title = $current_page['title'];
			}

			if(!empty($current_page['meta_keywords'])) $this->template->meta_keywords = $current_page['meta_keywords'];
			if(!empty($current_page['meta_description'])) $this->template->meta_description = $current_page['meta_description'];	

			if($update_current_page)
			{
				$this->template->set_global('current_page', $current_page, false);
			}

		}

	}

	private function ajax()
	{
		
	}

	/**
	 * Speed up website loading
	 * @param  response
	 * @return $response 
	 */
   public function after($response)
   {

		$response = new Response($this->template, 200);	

		$time = time() + (60 * 5);
		
		$response->set_header('Cache-Control', 'public, must-revalidate');
		$response->set_header('Expires', date('D, d M Y H:i:s e', $time));//'Mon, 26 Jul 200 05:00:00 GMT');

		$response->send_headers();

		return $response;
   }		
}