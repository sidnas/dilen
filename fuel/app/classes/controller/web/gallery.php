<?php
	class Controller_Web_Gallery extends Controller_Web_Base
	{
		private $current_page;
		
		public function before()
		{
			parent::before();
			
			$this->current_page = \Model_Content::get_by_content_id($this->current_page_id, $this->current_language['id'], true);
			$this->update_tempalte_values($this->current_page);
		}

		/**
		 * Collect galleries
		 * @return $this->action_single();
		 */
		public function action_index()
		{
			$sub_cats = array();
			$active_sub = $this->uri[3];
			$active_page_id = $this->current_page['id'];

			$i = 0;
			foreach(Model_Select_Content::get_list($this->current_language->id, 1, $this->current_page['id']) as $page)
			{
				$sub_cats[$page['slug']] = $page;
				$i++;
			}
			
			if(!empty($sub_cats))
			{
				if(!empty($sub_cats[$active_sub]))
				{
					$active_page_id = $sub_cats[$active_sub]['id'];
				}
				else
				{
					$sub = current($sub_cats);
					$active_sub = $sub['slug'];
					$active_page_id = $sub['id'];
				}	
				$sub_cats[$active_sub]['active'] = true;			
			}

			$this->template->sub_menu = $sub_cats;

			if(!empty($sub_cats[$active_sub]))
			{
				$this->update_tempalte_values($sub_cats[$active_sub], false);
			}
			return $this->action_single($active_page_id);
		}

		public function action_single($gallery_id)
		{
			$view = array();
			if(!is_numeric($gallery_id)) \Response::redirect(\Uri::create($this->current_language->val));

			$gallery = \Model_Select_Content::get_by_content_ids($gallery_id, $this->current_language['id']);

			$images = array();
			if(!empty($gallery['value']['images']))
			{
				foreach($gallery['value']['images'] as $image)
				{
					if(empty($image['thumb']) || empty($image['large'])) continue;
					$images[] = array(
						'orginal' => \Uri::create($image[0]),
						'large' => \Uri::create($image['large']),
						'thumb' => \Uri::create($image['thumb'])
					);
				}
				unset($gallery['value']['images']);
			}

			$view['images'] = $images;
			$this->template->content = \View::forge('web/gallery/single', $view);
		}
	}
