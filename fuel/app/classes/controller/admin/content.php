<?php 
	class Controller_Admin_Content extends Controller_Admin_Base
	{
		private $fields_valid = false;
		
		private $sub_form_errors = array();
		
		private $sub_form = array();
		
		public function before()
		{
			parent::before();

			$this->template->css = array(
				'admin/color-picker/jquery.minicolors.css',
				'admin/wysi/bootstrap-wysihtml5.css'
			);
				
			$this->template->js = array(
				'admin/ckeditor/ckeditor.js',
				'admin/wysi/bootstrap3-wysihtml5.js',
				'admin/jquery.jqote2.min.js',
				'admin/jquery-ui-v1.10.3.js',
				'admin/content.js',
				'admin/color-picker/jquery.minicolors.js'
			);

			if(\Input::is_ajax()) exit($this->ajax());

		}
	
		public function action_index()
		{
			$positions = array();

			$default_position_id = false;
			foreach($_positions = Model_Contents_Position::find('all', array(
				'order_by' => array(
					array('order', 'asc')
				)
			)) as $v)
			{
				$positions[$v->id] = $v->title;
				if(!$default_position_id) $default_position_id = $v->id;
			}

			$current_position = \Input::get('position_id') ? (int)\Input::get('position_id') : null;
			if(empty($positions[$current_position])) $current_position = $default_position_id;

			/* Collect contents */
			
		$template_parent_filter = array(
			'where' => array(
				array('parent_template_id', '=', 0)
			)	
		);
		$parent_content_id = (int)\Uri::segment(4);
		
		if($parent_content_id) $template_parent_filter = array();
		
		$content_tree = array();
		foreach(Model_Content::find('all', array(
			'related' => array(
				'templates' => array(
					'related' => array(
						'parents' => array(
							$template_parent_filter
						)
					)
				),
				'content_langs' => array(
					'where' => array(
						array('language_id', '=', $this->language_id)
					)
				),
				'nodes' => array(
					'related' => array(
						'positions',
					),
					'where' => array(
						array('parent_content_id', '=', $parent_content_id),
						array('position', '=', $current_position)
					),
					'order_by' => array(
						array('order', 'asc')
					)
				)
			),
			'from_cache' => false
		)) as $v)
		{
			$content_langs = !empty($v->content_langs) ? current($v->content_langs) : null;
			
			$status = array('name' => 'inactive', 'status' => 'danger');
			if($content_langs)
			{
				if($content_langs->active == 1 && $content_langs->visible == 0)
				{
					$status = array('name' => 'invisible', 'status' => 'default');
				}
				elseif($content_langs->active == 1 && $content_langs->visible == 1)
				{
					$status = array('name' => 'active', 'status' => 'success');
				}
			}
			
			
			$node = (!empty($v->nodes) ? current($v->nodes) : null);
			
			$content_tree[$v->id] = array(
				'id' => $v->id,
				'node_id' => ($node ? $node->id : null),
				'template_id' => $v->template_id,
				'title' => ($content_langs ? $content_langs->title : null),
				'slug' => ($content_langs ? $content_langs->slug : null),
				'status' => $status,
				'sub_cat_count' => count(Model_Contents_Tree_Node::find('all', array(
					'where' => array(
						array('parent_content_id', '=', $v->id)
					)
				))),
				'allowed_parents' => array(),
				'created_at' => $v->created_at,
				'updated_at' => ($content_langs ? $content_langs->updated_at : null)
			);

			foreach(Model_Templates_Parent::find('all', array(
				'related' => array(
					'templates'
				),
				'where' => array(
					array('parent_template_id', '=', $v->template_id)
				)
			)) as $v2)
			{
				if(!empty($v2->templates)) $content_tree[$v->id]['allowed_parents'][$v2->template_id] = $v2->templates->name;
			}

		}			

			$view['content_tree'] = $content_tree;
			
			$view['current_position'] = $current_position;
			
			$view['positions'] = $positions;
			
			$view['child'] = !empty($parent_content_id) ? Model_Content::get_parent($parent_content_id, $this->language_id) : null;
					
			
			$this->template->content = \View::forge('admin/content/index', $view, false);
		}
		
		public function action_create()
		{
			if(!Model_Contents_Position::find(\Uri::segment(4))) \Response::redirect(\Uri::create('admin/content/index'));
			
			
			$view['form'] = $this->form(null, null, null, \Util\Files::set_session_files(\Session::get('uploads_'.\Input::get('template_id'))));
			
			
			if($this->fields_valid && \Input::method() == 'POST')
			{
				$content = new Model_Content(array(
					'template_id' => \Input::get('template_id')				
				));
				
				$content->save();
				
				if($content->id)
				{
					$content_lang = new Model_Contents_Lang(array(
						'content_id' => $content->id,
						'title' => \Input::post('title'),
						'slug' => \Inflector::friendly_title((\Input::post('slug') ? \Input::post('slug') : \Input::post('title')), '-', true),
						'meta_keywords' => \Input::post('meta_keywords'),
						'meta_description' => \Input::post('meta_description'),
						'visible' => 1,
						'active' => (\Input::post('active') ? 1 : 0),
						'language_id' => $this->language_id
					));
				
					$content_lang->save();
			
			
					$node = new Model_Contents_Tree_Node(array(
						'content_id' => $content->id,
						'parent_content_id' => (int)\Uri::segment(5),
						'position' => (int)\Uri::segment(4),
						'order' => count(Model_Contents_Tree_Node::find('all', array(
							'where' => array(
								array('parent_content_id', '=', (int)\Uri::segment(5)),
								array('position', '=', (int)\Uri::segment(4))
							)
						))) + 1
					));					
					$node->save();
			
					$custom_values = array();
					foreach(Model_Templates_Field::find('all', array(
						'where' => array(
							array('template_id', '=', \Input::get('template_id'))
						)
					)) as $v)
					{
						$custom_values[$v->key_name] = array(
							'field_id' => $v->id,
							'type' => $v->type,
							'value' => (\Input::post('field.'.$v->key_name) ? \Input::post('field.'.$v->key_name) : '')
						);
					}
					
					if($files = \Session::get('uploads_'.\Input::get('template_id')))
					{
						foreach($files as $key_name => $file_row)
						{

							foreach($file_row as $file)
							{
								if(!$field = current(Model_Contents_Fields_Value::find('all', array(
									'where' => array(
										array('template_field_id', '=', $file['template_field_id']),
										array('content_id', '=', $content->id),
										array('language_id', '=', $this->language_id)
									)
								
								))))
								{
									$field = new Model_Contents_Fields_Value(array(
										'template_field_id' => $file['template_field_id'],
										'content_id' => $content->id,
										'language_id' => $this->language_id
									));
									$field->save();
								}

								$field_file = new Model_Contents_Fields_File(array(
									'file_id' => $file['file_id'],
									'content_field_id' => $field->id
								));
								$field_file->save();
							}
							
						}
					
						\Session::delete('uploads_'.\Input::get('template_id'));
					}

					\Model_Contents_Filter::update_content_filters($content->id, \Input::post('filter'), false);
				
					if(!empty($this->sub_form))
					{
						$method = $this->sub_form->method;
						$method::update_values_by_content_id($content->id, $this->sub_form->values, $this->current_user['id']);
					}					

					foreach($custom_values as $v)
					{
						if(in_array($v['type'], array('date_picker', 'date_time_picker')))
						{
							try
							{
								$v['value'] = current(\Date::create_from_string($v['value'], 'eu'));
							}
							catch(Exception $e)
							{
								$v['value'] = time();
							}
						}	

						try
						{

							$field = new Model_Contents_Fields_Value(array(
								'content_id' => $content->id,
								'template_field_id' => $v['field_id'],
								'value' => $v['value'],
								'language_id' => $this->language_id
							));
							$field->save();
						}
						catch(Exception $e)
						{
						
						}
					}
					
					\Util\Cache::delete(array(
						'content.'.$this->language_id.'.'.$content->templates->type,
						'content.'.$this->language_id.'.menu'
					));
					
					
					\Response::redirect(\Uri::create('admin/content/edit/'.$content->id));
				}
			}

			$this->template->content = \View::forge('admin/content/create', $view, false);
		}
		
		public function action_edit($content_id)
		{
			if(!$content = Model_Content::find($content_id, array(
				'related' => array(
					'templates'
				)
			))) \Response::redirect(\Uri::create('admin/content'));

			
			
			$custom_values = array();
			$files = array();
			$_custom_values_obj = array();
			foreach($_custom_values = Model_Contents_Fields_Value::find('all', array(
				'related' => array(
					'template_fields',
					'field_files' => array(
						'related' => array(
							'files'
						)
					)
				),
				'where' => array(
					array('content_id' => $content_id),
					array('language_id' => $this->language_id)
				)
			)) as $v)
			{
				if(!empty($v->template_fields->key_name))
				{
					$custom_values[$v->template_fields->key_name] = $v->value;
					$files[$v->template_fields->key_name] = \Util\Files::set_files($v->field_files, $v->template_field_id);
					$_custom_values_obj[$v->template_fields->key_name] = $v;
				}
			}
			
			$content->content_langs = current(Model_Contents_Lang::find('all', array(
				'where' => array(
					array('content_id', '=', $content->id),
					array('language_id', '=', $this->language_id)
				)	
			)));

			if(empty($content->content_langs))
			{
				$content->content_langs = new Model_Contents_Lang(array(
					'content_id' => $content->id,
					'title' => '',
					'slug' => '',
					'meta_keywords' => '',
					'meta_description' => '',
					'visible' => 1,
					'active' => 0,
					'language_id' => $this->language_id					
				));
			}
			
			$view['form'] = $this->form($content, $custom_values, $_custom_values_obj, $files);
			
			if($this->fields_valid && \Input::method() == 'POST')
			{	
				$content->content_langs->set(array(
					'title' => \Input::post('title'),
					'slug' => \Inflector::friendly_title((\Input::post('slug') ? \Input::post('slug') : \Input::post('title')), '-', true),
					'meta_keywords' => \Input::post('meta_keywords'),
					'meta_description' => \Input::post('meta_description'),
					'visible' => 1,
					'active' => (\Input::post('active') ? 1 : 0),					
				));
				
				$content->content_langs->save();
				
				\Model_Contents_Filter::update_content_filters($content->id, \Input::post('filter'), true);
				
				if(!empty($this->sub_form))
				{
					$method = $this->sub_form->method;
					$method::update_values_by_content_id($content->id, $this->sub_form->values, $this->current_user['id']);
				}
				
				if(\Input::post('field') && is_array(\Input::post('field')))
				{
					foreach(\Input::post('field') as $k => $v)
					{
						if(isset($_custom_values_obj[$k]))
						{
							if(in_array($_custom_values_obj[$k]->template_fields->type, array('date_picker', 'date_time_picker')))
							{
								try
								{
									$v = current(\Date::create_from_string($v, 'eu'));
								}
								catch(Exception $e)
								{
									$v = time();
								}
							}	
							echo 'b: '.$v;
							$_custom_values_obj[$k]->value = $v;
							$_custom_values_obj[$k]->save();
						}
						else
						{
							$template_field = current(Model_Templates_Field::find('all', array(
								'where' => array(
									array('template_id', '=', $content->template_id),
									array('key_name', '=', $k)
								)
							)));
							
							if($template_field)
							{

								if(in_array($template_field->type, array('date_picker', 'date_time_picker')))
								{
									try
									{
										$v = current(\Date::create_from_string($v, 'eu'));
									}
									catch(Exception $e)
									{
										$v = time();
									}
								}	


								$new_field = new Model_Contents_Fields_Value(array(
									'content_id' => $content->id,
									'template_field_id' => $template_field->id,
									'value' => $v,
									'language_id' => $this->language_id
								));
								$new_field->save();
								$_custom_values_obj[$k] = $new_field;
							}
						}
					}
				}
				
				\Util\Cache::delete(array(
					'content.'.$this->language_id.'.'.$content->templates->type,
					'content.'.$this->language_id.'.single.id.'.$content->id,
					'content.'.$this->language_id.'.single.slug.'.$content->content_langs->slug,
					'content.'.$this->language_id.'.menu',
					'posters.'.$content->id
				));
				
			}
			
			$view['child'] = !empty($content->id) ? Model_Content::get_parent($content->id, $this->language_id) : null;
						
			
			$this->template->content = \View::forge('admin/content/edit', $view, false);
		}
	
		
		
		private function form($content = null, $values = array(), $values_obj = null, $files = array())
		{
			$view = array();
			$custom_fields = array();
			
			$template_id = !empty($content->template_id) ? $content->template_id : \Input::get('template_id');
			
			$templates = array(
				0 => \Lang::get('admin.choose')
			);

			
			$parent_template_id = (int)\Uri::segment(5);
			if(!empty($parent_template_id))
			{
				if(!$parent_content = Model_Content::find($parent_template_id)) \Response::redirect(\Uri::create('admin/content/index?parent_template=not-found'));
				
				$parent_template_id = $parent_content->template_id;
			}
			

			$template_check = array(
				'where' => array(
					array('parent_template_id', '=', $parent_template_id)
				)
			);
			if(\Uri::segment(3) == 'edit') $template_check = array();


			foreach($_templates = Model_Template::find('all', array(
				'related' => array(
					'parents' => $template_check					
				)
			)) as $v)
			{
				$templates[$v->id] = $v->name;
			}
			
			$view['templates'] = $templates;

			$picked_template = !empty($_templates[$template_id]) ? $_templates[$template_id] : null;
		
			$sub_form = '';	
			
			
			
			
			if(!empty($picked_template->id))
			{

				$sub_form = $this->sub_form($picked_template->type, !empty($content->id) ? $content->id : null);

				$errors = array();
				if(\Input::method() == 'POST')
				{
					$val = \Validation::forge('basic');
					$val->add('title', \Lang::get('admin.title'))->add_rule('required');
					$val->set_message('required', \Lang::get('admin.field_required'));
					
					if(!$val->run())
					{
						foreach($val->error(null) as $k=>$v) $errors[$k] = (string)$v;	
					}
					
					$val = \Validation::forge('custom_fields');
					foreach(Model_Templates_Field::find('all', array(
						'where' => array(
							array('template_id', '=', $picked_template->id)
						)
					)) as $v)
					{
					
						if(!empty($v->required))
						{
							if(empty($v->filter_id))
							{
								$val->add($v->key_name, $v->name)->add_rule('required');
							}
							else
							{
								/* Filter value validation */
							}
						}
					}

					$val->set_message('required', \Lang::get('admin.field_required'));
					
					
					if($val->run(\Input::post('field')) && empty($this->sub_form_errors))
					{
						$this->fields_valid = true;
					}
					else
					{
						foreach($val->error(null) as $k=>$v) $errors[$k] = (string)$v;							
					}
				
				}
			
				$custom_fields = \ViewModel::forge('admin/template/field')->set(array(
					'content_id' => !empty($content->id) ? $content->id : null,
					'template_id' => $picked_template->id,
					'values' => (\Input::post('field') ? \Input::post('field') : $values),
					'errors' => \Arr::merge($errors, $this->sub_form_errors),
					'update_status' => $this->fields_valid,
					'values_obj' => $values_obj,
					'files' => $files
				));			
			}
			
			$picked_template_id = !empty($picked_template->id) ? $picked_template->id : 0;
			
			$view['picked_template'] = $picked_template_id;
			
			$view['custom_fields'] = $custom_fields;
			
			$view['form_action'] = (\Uri::segment(3) == 'create' ? \Uri::create('admin/content/create/'.(int)\Uri::segment(4).'/'.(int)\uri::segment(5).'/?template_id='.$picked_template_id) : \Uri::create('admin/content/edit/'.\Uri::segment(4).'/?language_id='.$this->language_id));
			
			$view['content'] = $content;
			
			$view['sub_form'] = $sub_form;
			
			$languages = array();
			foreach(Model_Language::find('all', array(
			
				'order_by' => array(
					array('primary', 'desc')
				)
			)) as $v)
			{
				$languages[] = array(
					'id' => $v->id,
					'title' => $v->title
				);
			}
			
			$view['languages'] = $languages;
			
			return \View::forge('admin/content/form', $view, false);
		}
		
		private function sub_form($type, $content_id = null)
		{
			$avaible_class_replacor = array(
				'product' => 'Poster'
			);
		
			if(!empty($avaible_class_replacor[$type])) $type = $avaible_class_replacor[$type];
		
			if(!class_exists('Model_Contents_'.$type)) return null;
			
			if(!method_exists('Model_Contents_'.$type, 'sub_form_fields')) return null;
			
			$method = 'Model_Contents_'.$type;
			$fields = $method::sub_form_fields();

			$fresh_values = array();
			
			if(\Input::method() == 'POST')
			{	
				$val = \Validation::forge('sub_form');
				foreach($fields as $name => $v)
				{
					if(!empty($v['required']) && empty($v['is_child']))
					{
						$val->add($name, \Lang::get('admin.'.$name))->add_rule('required');
					}
					
					if(in_array($v['type'], array('date_time_picker', 'date_picker')) && \Input::post('sub_field.'.$name))
					{
						$val->add($name, \Lang::get('admin.'.$name))->add_rule('valid_date');
					}
	
					if(!empty($v['childs']) && \Input::post('sub_field.'.$name) == 1)
					{
						foreach($v['childs'] as $child_name)
						{
							if(!empty($fields[$child_name]) && !empty($fields[$child_name]['required']))
							{
								$val->add($child_name, \Lang::get('admin.'.$child_name))->add_rule('required');
							}
						}
					}
		
	
					
				}
				
				$val->set_message('valid_date', \Lang::get('admin.field_not_valid_date'));
				$val->set_message('required', \Lang::get('admin.field_required'));	
				
				if(!$val->run(\Input::post('sub_field')))
				{
					foreach($val->error(null) as $k=>$v) $this->sub_form_errors[$k] = (string)$v;	
				}
				else
				{
					$this->sub_form = (object)array(
						'method' => new $method,
						'value' => array()
					);
					foreach($fields as $name => $v)
					{					
						$value = \Input::post('sub_field.'.$name);
						$fresh_values[$name] = $value;
			
						if(in_array($v['type'], array('date_time_picker', 'date_picker')) && !empty($value))
						{
							$value = current(\Date::create_from_string($value, ($v['type'] == 'date_time_picker' ? 'eu_full' : 'eu')));
						}
						$this->sub_form->values[$name] = !empty($value) ? $value : '';
						
					}
				}				

			}

			$view = array();
			
			
			$view['fields'] = $fields;

			if(empty($fresh_values))
			{
				$view['sub_value'] = ($content_id ? $method::get_by_content_id($content_id) : array());
			}
			else
			{
				$view['sub_value'] = $fresh_values;
			}
			return \View::forge('admin/content/sub_form', $view, false);
		}
		
		
		private function ajax()
		{
			$status = array(
				'c' => 0
			);
			switch(\Input::post('action'))
			{
				case 'editFileData':
					$status = $this->edit_file_data();
				break;
				case 'updateNoderOrder':
					$status = $this->update_node_order();
				break;
				case 'deleteContent':
					$status = $this->delete_content(\Input::post('content_id'));
				break;
				case 'deleteFile':
					$status = $this->delete_file(\Input::post('file_id'));
				break;
				case 'fileToOtherLangs':
					$status = $this->add_file_to_other_languages(\Input::post('content_value_id'), \Input::post('file_id'));
				break;
				case 'showStaticTranslations':
					$status = $this->show_static_translations();
				break;
				case 'deleteStaticData':
					$status = $this->delete_static_data();
				break;
				case 'updateStaticData':
					$status = $this->update_static_data();
				break;
				case 'deleteFilterValues':
					$status = $this->remove_filter_values(\Input::post('ids'), \Uri::segment(4));
				break;
			}	
			exit(\Format::forge($status)->to_json());
		}
		
		private function remove_filter_values($filter_ids, $content_id)
		{
			$status = array(
				'c' => 0
			);
			
		
			if(!$content = Model_Content::find($content_id))
			{
				$status['msg'] = \Lang::get('admin.content_not_found');
				return $status;
			}			
			
			$ids = array();
			if(is_array($filter_ids))
			{
				foreach($filter_ids as $filter_id)
				{
					$id = (int)$filter_id;
					if(!empty($id)) $ids[] = $id;

				}
			}
			else
			{
				$id = (int)$filter_ids;
				if(!empty($id)) $ids[] = $id;
			}
			
			if(empty($ids))
			{
				$status['msg'] = \Lang::get('admin.nothing_to_delete');
				return $status;				
			}
			
			$cleared = Model_Contents_Filter::delete_filters_by_ids($ids, $content->id);
			if(!$cleared)
			{
				$status['msg'] = \Lang::get('admin.nothing_to_delete');
				return $status;					
			}
		
			$status['c'] = 1;
			$status['msg'] = \Lang::get('admin.filter_values_deleted');
			return $status;
		}
		
		private function update_static_data()
		{
			$status = array(
				'c' => 0
			);		
		
			if(!$content = Model_Content::find(\Uri::segment(4)))
			{
				$status['msg'] = \Lang::get('admin.content_not_found');
				return $status;
			}
			
		
			if(!\Input::post('values') || !is_array(\Input::post('values')))
			{
				$status['msg'] = \Lang::get('admin.nothing_to_update');
				return $status;			
			}			
			
			foreach(\Input::post('values') as $k => $v)
			{
				$input = current(Model_Translation::find('all', array(
				'where' => array(
					array('input_name', '=', $k)
				))));
				
				if($input)
				{	
					if($lang = current(Model_Translations_Lang::find('all', array(
						'where' => array(
							array('translation_id', '=', $input->id),
							array('language_id', '=', $this->language_id)
						)
					))))
					{
						$lang->value = $v;
						$lang->save();
					}
					else
					{
						$lang = new Model_Translations_Lang(array(
							'translation_id' => $input->id,
							'language_id' => $this->language_id,
							'value' => $v
						));
						$lang->save();
					}
				}

			}
					
			$status['c'] = 1;
			$status['msg'] = \Lang::get('admin.data_updated');

			$translation = new \Util\Translation;			
			\Cache::delete_all($translation->cache_name);
			
			return $status;
		}

		
		private function delete_static_data()
		{
			$status = array(
				'c' => 0
			);		
		
			if(!$content = Model_Content::find(\Uri::segment(4)))
			{
				$status['msg'] = \Lang::get('admin.content_not_found');
				return $status;
			}		
			
			if(!$static = current(Model_Translation::find('all', array(
				'where' => array(
					array('input_name', '=', \Input::post('key'))
				),
				'related' => array(
					'translation_template',
					'translation_lang'
				)
			))))
			{
				$status['msg'] = \Lang::get('admin.static_data_not_found');
				return $status;			
			}
			
			if(!empty($static->translation_template))
			{
				foreach($static->translation_template as $k => $v)
				{
					$static->translation_template[$k]->delete();
					unset($static->translation_template[$k]);
				}
			}			
			
			if(!empty($static->translation_lang))
			{
				foreach($static->translation_lang as $k => $v)
				{
					$static->translation_lang[$k]->delete();
					unset($static->translation_lang[$k]);
				}
			}
			
			$static->delete();
				

			$translation = \Util\Translation::instance();			
			\Cache::delete_all($translation->cache_name);
				
			$status['c'] = 1;
			$status['msg'] = \Lang::get('admin.data_updated');

			return $status;
		}	
		
		private function show_static_translations()
		{
			$status = array(
				'c' => 0
			);
		
			if(!$content = Model_Content::find(\Uri::segment(4)))
			{
				$status['msg'] = \Lang::get('content_not_found');
				return $status;
			}
			

			$view['translations'] = Model_Translation::get_by_template_id($content->template_id, $this->language_id);
			exit(\View::forge('admin/content/translations', $view, false));

		}
		
		private function add_file_to_other_languages($content_value_id, $file_id)
		{
			$status = array(
				'c' => 0
			);
			
			if(!$content_value = Model_Contents_Fields_Value::find($content_value_id))
			{
				$status['c'] = 0;
				$status['msg'] = \Lang::get('file_cannot_be_added_to_other_languages_yet');
				return $status;
			}
		
			$languages = array();
			foreach(Model_Language::find('all', array(
				'where' => array(
					array('id', '!=', $content_value->language_id)
				)	
			)) as $lang)
			{
				$languages[] = $lang->id;
			}
			
			$found = false;
			foreach($languages as $language_id)
			{
				if(!$field = current(Model_Contents_Fields_Value::find('all', array(
					'where' => array(
						array('content_id', '=', $content_value->content_id),
						array('template_field_id', '=', $content_value->template_field_id),
						array('language_id', '=', $language_id)
					)
				))))
				{
					$field = new Model_Contents_Fields_Value(array(
						'content_id' => $content_value->content_id,
						'template_field_id' => $content_value->template_field_id,
						'language_id' => $language_id,
						'value' => null
					
					));
					$field->save();
				}
			
				if(!$file = Model_Contents_Fields_File::find('all', array(
					'where' => array(
						array('file_id', '=', $file_id),
						array('content_field_id', '=', $field->id)
					)
				)))
				{
					$found = true;
					$file = new Model_Contents_Fields_File(array(
						'file_id' => $file_id,
						'content_field_id' => $field->id
					));
					$file->save();
				}
			}
			
			if($found)
			{
				$status['c'] = 1;
				$status['msg'] = \Lang::get('admin.file_linked');
			}	
			else
			{
				$status['msg'] = \Lang::get('admin.file_already_linked_to_all_languages');
			}

			return $status;
		
		}
		
		
		private function delete_file($file_id)
		{
			\Util\Files::remove_file_by_id($file_id);
			return array(
				'c' => 1,
				'msg' => \Lang::get('admin.file_deleted')
			
			);
		
		}
		
		private function delete_content($content_id)
		{
			\Util\Contents::remove_by_id($content_id);
			return array(
				'c' => 1,
				'msg' => \Lang::get('admin.content_deleted')
			);
		}
		
		private function update_node_order()
		{
			$status = array(
				'c' => 1,
				'msg' => \Lang::get('admin.content_order_updated')
			);
			
			if(is_array(\Input::post('nodes')))
			{
				$order = 1;
				foreach(\Input::post('nodes') as $node_id)
				{
					if($node = Model_Contents_Tree_Node::find($node_id))
					{
						$node->order = $order++;
						$node->save();
					}
				}
			}

			return $status;
		}
		
		
		
		private function edit_file_data()
		{				
			$status = array(
				'c' => 0
			);
			$status['msg'] = \Lang::get('admin.error');
			if(!$content = Model_Content::find(\Uri::segment(4)))
			{
				$status['msg'] = 'Content not found!';
				return $status;
			}
				
			parse_str(\Input::post('data'), $data);	
			if(!empty($data['file']))
			{
				if($file = Model_Contents_Fields_File::find(\Input::post('content_file_id')))
				{
					$status['c'] = 1;
					$clear_data = array();
					foreach($data['file'] as $k => $v)
					{
						$clear_data[$k] = \Security::xss_clean(\Security::strip_tags($v));
					}
					$file->custom_value = \Format::forge($clear_data)->to_json();
					$file->save();
					$status['custom_value'] = $clear_data;
					$status['msg'] = \Lang::get('admin.data_updated');
				}
			}

			return $status;
		}
	
	
	
	
	
	
	
	}