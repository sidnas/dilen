<?php 
	class Controller_Admin_Developer extends Controller_Admin_Base
	{
		public $user_groups = array();
		
		public function before()
		{
			parent::before();

			$this->user_groups = Model_User::groups();
			
		}
	
		public function action_index()
		{
			$this->template->content = 'DEV';
		}
	
		public function action_user_groups()
		{
			$current_group = null;
			$admin_cats = array();
			
			if(!empty($this->user_groups[\Input::get('user_group')])) $current_group = \Input::get('user_group');
		
			
			if($current_group)
			{
				if(\Input::method() == 'POST')
				{
					foreach(Model_Admin_Cats_Permission::find('all', array(
						'where' => array(
							array('group', '=', $current_group)
						)
					)) as $v) $v->delete();
					
					$cats = \Input::post('cat_ids');
					if(!empty($cats) && is_array($cats))
					{
						foreach($cats as $cat_id)
						{
							$cat_perm = new Model_Admin_Cats_Permission(array(
								'cats_id' => $cat_id,
								'group' => $current_group
							));
							$cat_perm->save();
						}
					}
				}
				
				
				foreach(Model_Admin_Cats_Permission::find('all', array(
					'where' => array(
						array('group', '=', $current_group)
					)
				)) as $v)
				{
					$admin_cats[$v->cats_id] = $v->cats_id;
				}

			}
			
			$cats = array();
			foreach(Model_Admin_Cat::find('all', array(
				'related' => array(
					'parents'
				),
				'where' => array(
					array('parent_id', 'in', array(-1, 0))
				),
				'order_by' => array(
					array('order', 'asc')
				)
			)) as $v)
			{
				$cats[$v->id] = $v->title;
				if(!empty($v->parents))
				{
					foreach($v->parents as $v2)
					{
						$cats[$v2->id] = ' -- '.$v2->title;
					}
				}
			}
		
		
			$view = array();
			$view['current_group'] = $current_group;
			$view['user_groups'] = $this->user_groups;
			$view['admin_cats'] = $admin_cats;
			$view['cats'] = $cats;
			$this->template->content = \View::forge('admin/developer/user_groups', $view, false);
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
