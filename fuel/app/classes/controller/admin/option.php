<?php 
	class Controller_Admin_Option extends Controller_Admin_Base
	{
		public function before()
		{
			parent::before();
			
			$this->form_action = \Uri::create('admin/option/'.\Uri::segment(3).(\Uri::segment(4) ? '/'.\Uri::segment(4) : '').(\Uri::segment(5) ? '/'.\Uri::segment(5) : '').'?language_id='.\Input::get('language_id'));
			$this->template->js = array(
				'admin/jquery.jqote2.min.js', 
				'admin/jquery-ui-v1.10.3.js',
				'admin/option.js'
			);
			
			if(\Input::is_ajax()) exit($this->ajax());
			
		}
	
		public function action_index()
		{

			$view['options'] = Model_Option::get_options(null, true);			
			$this->template->content = \View::forge('admin/option/index', $view, false);
		}
	
	
	
		public function action_create()
		{		
			
			if(\Input::method() == 'POST')
			{
				$option = new Model_Option(array(
					'input_name' => \Input::post('input_name')
				));
				$option->save();
				
				if(!empty($option->id))
				{
					$option_lang = new Model_Options_Lang(array(
						'option_id' => $option->id,
						'slug' => \Input::post('slug'),
						'name' => \Input::post('name'),
						'language_id' => $this->language_id
					
					));
					$option_lang->save();
				}
				
				if(!empty($option->id) && \Input::post('values')) $this->update_option_values(\Input::post('values'), $option->id, 0);
				
				\Response::redirect(\Uri::create('admin/option/edit/'.$option->id));

			}		
			$view['form']  = $this->form();			
			$this->template->content = \View::forge('admin/option/create', $view, false);
		}
	
		public function action_edit($option_id)
		{	
			$option = Model_Option::query()
				->where('id', $option_id)
				->get_one();
			
			if(empty($option->id)) return $this->action_index();

			$parent_id = (int)\Uri::segment(5);
			
			$option_values = null;
			
			if(\Input::method() == 'POST')
			{
				if(\Input::post('values')) $this->update_option_values(\Input::post('values'), $option_id, $parent_id);
				
				
				$option_langs = Model_Options_Lang::query()
					->where('language_id', $this->language_id)
					->where('option_id', $option->id)
					->get_one();

				if(empty($option_langs))
				{
					$option_langs = new Model_Options_Lang(array(
						'name' => \Input::post('name'),
						'slug' => \Input::post('slug'),
						'option_id' => $option->id,
						'language_id' => $this->language_id
					));
				}
				else
				{
					$option_langs->name = \Input::post('name');
					$option_langs->slug = \Input::post('slug');			
			
				}
				try
				{
					$option_langs->save();
				}
				catch(\Exception $e)
				{
				
				}
				$option->option_langs = $option_langs;
				\Response::redirect($this->form_action);
			}
			else
			{
				$option = Model_Option::get_option((int)$option_id, $this->language_id, true);
			}
			
			$option_values = Model_Options_Value::get_values($option['id'], $this->language_id, $parent_id, 'count');

			$view['form']  = $this->form($option, $option_values, $parent_id);		


			$this->template->content = \View::forge('admin/option/edit', $view, false);		
		}
	
	
	
	
	
		private function form($option = null, $option_values = null, $parent_id = 0)
		{	
			/* VALIDATION!!!! */
			$view = array();
			$view['option_links'] = null;//$option_links;

			$view['option'] = $option;
			$view['option_values'] = $option_values;
			
			$view['languages'] = Model_Language::get_all(false);
			$view['form_action'] = $this->form_action;
			
			$parent_value_id = false;
			if(!empty($parent_id))
			{
				$parent_value = Model_Options_Value::find($parent_id);
				$parent_value_id = isset($parent_value->parent_id) ? $parent_value->parent_id : false;
			}			
			$view['parent_value_id'] = $parent_value_id;

			return \View::forge('admin/option/form', $view, false);
		}

		private function update_option_values($values, $option_id, $parent_id = 0)
		{
			\Util\Cache::delete('options.values.'.$option_id);
			\Util\Cache::delete('options.all');
			\Util\Cache::delete('options.'.$option_id);
			
			if(empty($values)) return null;
			

			foreach($values as $value_id => $data)
			{
				if(empty($data['id'])) continue;
				/* Update basic */
				if($value_id > 0)
				{
					$value = Model_Options_Value::find($value_id);
					if($value_id)
					{
						$value->equal_lang = !empty($data['equal_lang']) ? 1 : 0;
						$value->save();
					}
				}
				else 
				{
					$value = new Model_Options_Value(array(
						'option_id' => $option_id,
						'parent_id' => $parent_id,
						'equal_lang' => !empty($data['equal_lang']) ? 1 : 0	
					));
					$value->save();

				}

				/*  Update value language */
				if(!empty($value->id))
				{
					if($value_lang = current(Model_Options_Values_Lang::find('all', array(
						'where' => array(
							array('option_value_id' => $value->id),
							array('language_id' => $this->language_id)
						)
					))))
					{
						$value_lang->value = !empty($data['value']) ? $data['value'] : '';
						$value_lang->slug = !empty($data['slug']) ? $data['slug'] : '';
						
						try
						{
							$value_lang->save();
						} catch(Exception $e) {}
					}
					else
					{
						$value_lang = new Model_Options_Values_Lang(array(
							'option_value_id' => $value->id,
							'value' => !empty($data['value']) ? $data['value'] : '',
							'slug' => !empty($data['slug']) ? $data['slug'] : '',
							'language_id' => $this->language_id
						));
						$value_lang->save();
					}
				}
			}
		
			return true;
			
		}
		
		private function update_value_order($value_ids)
		{
			$ids = array();
		
			if(empty($value_ids)) return array(33);
			if(!is_array($value_ids)) return array(22);
			
			$order = 1;
			foreach($value_ids as $value_id)
			{
				$value = Model_Options_Value::find($value_id);
				if(!empty($value->id))
				{
					$value->order = $order++;
					$value->save();
				}
			}
			
			$status['c'] = 1;
			$status['msg'] = \Lang::get('admin.value_order_updated');
		
			return $status;
		}
		
		private function delete_value($value_id)
		{
			
			if($value = Model_Options_Value::find($value_id))
			{

				
				foreach(Model_Options_Value::find('all', array(
					'where' => array(
						array('parent_id', '=', $value->id)
					)	
				)) as $child)
				{
					$this->delete_value($child->id);
				}
				
				foreach(Model_Options_Values_Lang::find('all', array(
					'where' => array(
						array('option_value_id', '=', $value->id)
					)
				)) as $lang) $lang->delete();
	
				$value->delete();
				
				$status['c'] = 1;
				$status['msg'] = \Lang::get('admin.value_deleted');
			}
			else
			{	
				$status['msg'] = \Lang::get('admin.nothing_to_delete');
			}
			return $status;
		}
		
		private function ajax()
		{
			$status = $status2 = array(
				'c' => 0
			);
			
			switch(\Input::post('action'))
			{
				case 'updateValueOrder':
					$status2 = $this->update_value_order(\Input::post('nodes'));
				break;
				case 'deleteValue':
					$status2 = $this->delete_value(\Input::post('value_id'));
				break;
			}

			return \Format::forge(\Arr::merge($status, $status2))->to_json();
		}
		
	}