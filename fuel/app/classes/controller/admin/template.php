<?php 
	class Controller_Admin_Template extends Controller_Admin_Base
	{
			
		private $field_types = array(
			'text_field' => array(),		/* Simple text field <inputy type="text"/> */		
			'text' => array(),				/*Textarea*/ 
			'text_editor' => array(),		/* Textarea + wysiwyg*/
			//'select' => array(),			/* <select> dropdown */
			//'select_with_empty' => array()	/* <select> dropdown with default empty value */,
			'date_picker' => array(),
			'date_time_picker' => array(),
			'files' => array(),
			'images' => array(),
			//'component' => array(),
			'checkbox' => array(), /* 1 or 0 */
			//'page_data' => array() /* Add other page data, to template */		
			'filter' => array(),
			'color_picker' => array()
		);	
			
		private $multiples = array(
			0 => '---',
			'select' => 'Select'
		);
	
	
		public function before()
		{
			parent::before();
			
			$this->template->js = array(
				'admin/jquery.jqote2.min.js',
				'admin/jquery-ui-v1.10.3.js',
				'admin/template.js'
			);
		}
	
	
		public function action_parents()
		{
			$view = array();
			$templates = Model_Template::find('all');

			$template_list = $template_list2 = array(-1 => '---');
			$template_list2[0] = \Lang::get('admin.allow_in_base');
			foreach($templates as $v)
			{
				$template_list[$v->id] = $template_list2[$v->id] = $v->name;
			}
			
			$current_template = null;
			$current_parents = array();
			
			if(!empty($templates[\Input::get('template_id')]))
			{
				$current_template = \Input::get('template_id');
				
				$post_parents = \Input::post('parent_template_ids');
				if(empty($post_parents)) $post_parents = array(-1);
				
				if(\Input::method() == 'POST' && is_array($post_parents))
				{				
					$parent_ids = array();
					$_parent_ids = array();
					foreach($post_parents as $parent_id)
					{
						if($parent_id == 0)
						{
							$parent_ids[] = 0;
							break;
						}
					}
					
					foreach(Model_Template::find('all', array(
						'where' => array(
							array('id', 'in', $post_parents),
							array('id', '!=', $current_template)
						)
					)) as $parent)
					{
						$parent_ids[] = $parent->id;
					}

					foreach(Model_Templates_Parent::find('all', array(
						'where' => array(
							array('template_id', '=', $current_template)
						)	
					)) as $parent) $parent->delete();					
					
					foreach($parent_ids as $parent_template_id)
					{
						$parent = new Model_Templates_Parent(array(
							'template_id' => $current_template,
							'parent_template_id' => $parent_template_id
						));
						$parent->save();
						$current_parents[] = $parent_template_id;
					}
					
					
				}
				else
				{				
					foreach(Model_Templates_Parent::find('all', array(
						'where' => array(
							array('template_id', '=', $current_template)
						)	
					)) as $tp)
					{
						$current_parents[] = $tp->parent_template_id;
					}
				}
			}
			
			
			
			
			
			$view['current_template'] = $current_template;
			$view['current_parents'] = $current_parents;
			$view['template_list'] = $template_list;
			$view['template_list2'] = $template_list2;
			$this->template->content = \View::forge('admin/template/parents', $view, false);
		}
	
	
		public function action_index()
		{
			$templates = Model_Template::find('all');
			if(\Input::method() == 'POST')
			{
				if(is_array(\Input::post('delete')))
				{
					foreach(\Input::post('delete') as $template_id)
					{
						if(!empty($templates[$template_id]))
						{
							\Util\Contents::remove_by_template_id($template_id);
							$templates[$template_id]->delete();
							unset($templates[$template_id]);
							
							foreach(Model_Templates_Parent::find('all', array(
							'where' => array(
								array('template_id', '=', $template_id)
							))) as $template)
							{
								$template->delete();
							}							
							
							foreach(Model_Templates_Parent::find('all', array(
							'where' => array(
								array('parent_template_id', '=', $template_id)
							))) as $template)
							{
								$template->delete();
							}	
							
						}
						

						
					}
				}
			}

			$view['templates'] = $templates;
			$this->template->content = \View::forge('admin/template/index', $view, false);
		}

	
		public function action_edit($template_id)
		{
			$template = Model_Template::find($template_id, array(
				'related' => array(
					'fields' => array(
						'order_by' => array(
							array('order', 'asc')
						)
					)
				)
			));
			
			if(empty($template->id)) \Response::redirect(\Uri::create('admin/template/index'));
			
			if(\Input::method() == 'POST')
			{
				$template->set(array(
					'name' => \Input::post('name'),
					'type' => \Input::post('type'),
					'controller' => \Input::post('controller')				
				));
				$template->save();
				
				if(is_array(\Input::post('fields')))
				{		
					$order = 1;
	
					foreach(\Input::post('fields') as $field_id => $loop_field)
					{
						if(!empty($loop_field['delete']))
						{
						
							if(!empty($template->fields[$field_id]))
							{
								\Util\Contents::remove_content_field_values_by_template_field_id($field_id);
								$template->fields[$field_id]->delete();
								
							}
						}
						else
						{
							$custom_value_to_array = !empty($loop_field['custom_value']) ? \Format::forge($loop_field['custom_value'], 'json')->to_array() : array();
							if(!empty($loop_field['custom_value_merge']) && is_array($loop_field['custom_value_merge']))
							{
								$loop_field['custom_value'] = \Format::forge(\Arr::merge($custom_value_to_array, $loop_field['custom_value_merge']))->to_json();
							}

							$params = array(
								'name' => $loop_field['name'],
								'key_name' => $loop_field['key_name'],
								'type' => $loop_field['type'],
								'custom_value' => $loop_field['custom_value'],
								'searchable' => !empty($loop_field['searchable']) ? 1 : 0,
								'active' => !empty($loop_field['active']) ? 1 : 0,
								'required' => !empty($loop_field['required']) ? 1 : 0,
								'order' => $order++					
							);

							if(!empty($template->fields[$field_id]))
							{
								$template->fields[$field_id]->set($params);
								$template->fields[$field_id]->save();
							}
							else
							{
								$params['filter_id'] = !empty($loop_field['filter_id']) ? $loop_field['filter_id'] : 0;
								$params['template_id'] = $template->id;
								$field_row = new Model_Templates_Field($params);
								$field_row->save();
							}
						}
					}
					\Response::redirect(\Uri::create('admin/template/edit/'.$template->id));
				}
			}
			
			
			
			$view['form'] = $this->form($template);
			$this->template->content = \View::forge('admin/template/edit', $view, false);		
		}
	
		public function action_create()
		{
			$view = array();
			
			if(\Input::method() == 'POST')
			{
				$template = new Model_Template(array(
					'name' => \Input::post('name'),
					'type' => \Input::post('type'),
					'controller' => \Input::post('controller')
				));
				
				$template->save();
				
				if($template->id)
				{
					if(is_array(\Input::post('fields')))
					{
						$order = 1;
						foreach(\Input::post('fields') as $field)
						{
							$field_row = new Model_Templates_Field(array(
								'template_id' => $template->id,
								'name' => $field['name'],
								'key_name' => $field['key_name'],
								'type' => $field['type'],
								'custom_value' => $field['custom_value'],
								'searchable' => !empty($field['searchable']) ? 1 : 0,
								'active' => !empty($field['active']) ? 1 : 0,
								'required' => !empty($field['required']) ? 1 : 0,
								'filter_id' => !empty($field['filter_id']) ? $field['filter_id'] : 0,
								'order' => $order++						
							));							
							$field_row->save();
						}
					}
					\Response::redirect(\Uri::create('admin/template/edit/'.$template->id));
				}
			}

			$view['form'] = $this->form(null);
			$this->template->content = \View::forge('admin/template/create', $view, false);
		}
	
		private function form($template)
		{
			$view = array();
			$view['template'] = $template;
			
			$types = array();
			foreach($this->field_types as $key => $v)
			{
				$types[$key] = $key;
			}
			
			$view['field_types'] = $types;
			$view['options'] = \Model_Option::get_options(false, true);
			$view['multiples'] = $this->multiples;
			return \View::forge('admin/template/form', $view, false);
		}
		
		
		
	
	
	
	
	
	
	
	
	
	
	}
