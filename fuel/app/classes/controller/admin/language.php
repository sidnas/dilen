<?php 
	class Controller_Admin_Language extends Controller_Admin_Base
	{
		private $file_config;
		
		public function before()
		{
			parent::before();
	
			$this->file_config = array(
				'path' => DOCROOT.'upload'.DS.'icons',
				'randomize' => true,
				'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
			);
	
		}
	
		public function action_index()
		{
			$view = array();
			
			$status = array(
				'update' => null,
				'create' => null
			);
			
			if(\Input::method() == 'POST')
			{
				if(\Input::post('create')) $status['create'] = $this->create_language();

			}
			
			
			$languages = Model_Language::find('all', array(
				'order' => array(
					array('id', 'desc')
				)	
			));
			
			if(\Input::method() == 'POST')
			{
				if(\Input::post('update'))
				{
					$data = $this->update_languages($languages);
					$languages = $data['languages'];
				}
			}
			

			$view['languages'] = $languages;
			
			$this->template->content = \View::forge('admin/language/index', $view, false);
		}
	
		private function update_languages($languages)
		{

			if(\Input::post('id'))
			{
				$icons = $this->upload_icons();
				
				
				foreach(\Input::post('id') as $language_id)
				{	
					if(empty($languages[$language_id])) continue;
					
					if(\Input::post('delete.'.$language_id))
					{	
						if(empty($languages[$language_id]->primary))
						{
							if(!empty($languages[$language_id]->icon) && file_exists($this->file_config['path'].DS.$languages[$language_id]->icon)) \File::delete($this->file_config['path'].DS.$languages[$language_id]->icon);
							
							$languages[$language_id]->delete();
							
							foreach(Model_Contents_Fields_Value::find('all', array(
								'where' => array(
									array('language_id' => $language_id)
								)
								
							)) as $values)
							{
								$values->delete();
							}	

							foreach(Model_Translations_Lang::find('all', array(
								'where' => array(
									array('language_id' => $language_id)
								)							
							)) as $values)
							{
								$values->delete();
							}
							
							unset($languages[$language_id]);
						}
					}
					else
					{
						if(!empty($icons['icon_'.$language_id]))
						{
							if(!empty($languages[$language_id]->icon) && file_exists($this->file_config['path'].DS.$languages[$language_id]->icon)) \File::delete($this->file_config['path'].DS.$languages[$language_id]->icon);
							$languages[$language_id]->icon = $icons['icon_'.$language_id];
						}
						
						
						$languages[$language_id]->title = \Input::post('title.'.$language_id);
						$languages[$language_id]->active = (\Input::post('active.'.$language_id) ? 1 : 0);
						$languages[$language_id]->primary = 0;
					}
				}
			
			}
			
			$primary = (int)\Input::post('primary');
			$primary_set = false;
			foreach($languages as $k => $v)
			{
				if($v->id == $primary)
				{
					$primary_set = true;
					$languages[$k]->primary = 1;
				}
				
				$languages[$k]->save();
				
			}
			
			if(!$primary_set)
			{
				$languages[$k]->primary = 1;
				$languages[$k]->save();
			}
			
			\Session::set_flash('update_ok', \Lang::get('admin.language_updated'));
			
			return array(
				'languages' => $languages
			);
		}
		
	
		private function create_language()
		{
			if(!\Input::post('val'))
			{
				\Session::set_flash('create_error', \Lang::get('admin.country_code_is_mandatory'));
				return false;
			}
			
			if(current(Model_Language::find('all', array(
				'where' => array(
					array('val', '=', \Inflector::friendly_title(\Input::post('val'), '_', true))
				)
			))))
			{
				\Session::set_flash('create_error', \Lang::get('admin.country_code_is_taken'));
				return false;
			}
			
			$icon = $this->upload_icons();
			if(!empty($icon)) $icon = current($icon);
			
			$language = new Model_Language(array(
				'title' => \Input::post('title'),
				'val' => \Input::post('val'),
				'active' => 0,
				'primary' => 0,
				'icon' => ($icon ? $icon : '')
			));
			
			$language->save();
			
			\Session::set_flash('create_ok', \Lang::get('admin.language_added'));

		}
	
		private function upload_icons()
		{
		
			\Upload::process($this->file_config);
			$icons = null;
			if (Upload::is_valid())
			{
				\Upload::save();
				foreach(\Upload::get_files() as $v)
				{
					if(!empty($v['saved_as']))
					{
						$icons[$v['field']] = $v['saved_as'];
					}
				}
			}
			return $icons;
		}

	}
