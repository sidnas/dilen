<?php 
	class Controller_Admin_Dashboard extends Controller_Admin_Base
	{
		public function before()
		{
			parent::before();

		}
	
		public function action_index()
		{

			$view['charts'] = array(
				$this->chart_active_inactive(),
				$this->char_prod_cat_templates()
			);
			
			$this->template->content = \View::forge('admin/dashboard/index', $view, false);
		}
	
		public function chart_active_inactive()
		{
			$results =  \Util\Admin\Statistic::active_inactive_posters($this->language_id);
			$chart_data = array();
			$chart_data[] = array(\Lang::get('admin.status'), \Lang::get('admin.count'));
			foreach($results as $key => $count) $chart_data[] = array(\Lang::get('admin.'.$key), (int)$count);
			
			$view['chart_data'] = $chart_data;
			
			return \View::forge('admin/dashboard/chart_active_inactive', $view, false);
		}
	
		private function char_prod_cat_templates()
		{
			$results =  \Util\Admin\Statistic::prod_cats();
			$chart_data = array();

			$chart_data[] = array(\Lang::get('admin.count'), \Lang::get('admin.prod_tpls'), \Lang::get('admin.prod_cat_tpls'));
			$chart_data[] = array('-', (int)$results['prod_tpls'], (int)$results['prod_cat_tpls']);

			$view['chart_data'] = $chart_data;
			
			return \View::forge('admin/dashboard/chart_prod_cats', $view, false);
		}
	
	
	
	
	
	
	
	
	
	
	
	
	}