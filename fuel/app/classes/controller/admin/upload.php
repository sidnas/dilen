<?php 
class Controller_Admin_Upload extends Controller_Admin_Base
{
	private $field = false;
	
	private $path = null;
	
	private $allowed_images = array(
		'jpg', 'jpeg', 'png', 'gif'
	);
	
	private $allowed_files = array(
		'doc', 'docx', 'pdf', 'xlsx'
	);
	
	private $content_field_value_id = null;
	
	private $allowed_ext = null;
	
	public function before()
	{
		parent::before();
	}

	public function action_index()
	{
		$status = array(
			'c' => 0,
			'errors' => array()
		);
		
		$template_field_id = (int)\Input::post('template_field_id');
		$key_name = \Input::post('key_name');

		$content_id = \Input::post('content_id');
		
		$status['post'] = \Input::post();
		
		if(!$this->field = Model_Templates_Field::find($template_field_id))
		{
			$status['field']  = $this->field;
			$status['msg'] = 'Template field not found!';
			exit(\Format::forge($status)->to_json());
		}
		
		if($this->field->type == 'images')
		{
			$this->allowed_ext = $this->allowed_images;
		}
		elseif($this->field->type == 'files')
		{
			$this->allowed_ext = \Arr::merge($this->allowed_images, $this->allowed_files);
		}
		else
		{
			$status['msg'] = 'Uknow upload type.';
			exit(\Format::forge($status)->to_json());
		}
		
		$path = date('dmY');
		
		$config = array(
			'path' => DOCROOT.'upload'.DS.$this->field->type.DS,
			'randomize' => true,
			'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);		
		
		if(!file_exists($config['path'].$path.DS)) \File::create_dir($config['path'], $path);
	
		$config['path'] = $config['path'].$path.DS;

		$this->path = '/'.$this->field->type.'/'.$path.'/';
		
		\Upload::process($config);
		
		if (\Upload::is_valid())
		{
			\Upload::save();
			if($file = \Upload::get_files(0))
			{
				$status['file'] = $file;
				$custom_value = \Format::forge((string)$this->field->custom_value, 'json')->to_array();
				
				/* Do custom magic! */
				if(!empty($custom_value))
				{
					try
					{
						switch($this->field->type)
						{
							case 'images':
								$this->do_custom_image_manipulation($file, $custom_value);
							break;
							case 'files':
								$this->do_custom_file_manipulation($file, $custom_value);
							break;
						}
					}
					catch(Exception $e)
					{
						$status['error'] = $e->getMessage();
					}
				}

				$status['file'] = $file;
				$custom_value_save = array();
				$accept_custom = array('resize');
				
				if(!empty($custom_value))
				{
					foreach($custom_value as $k => $v)
					{
						if(in_array($k, $accept_custom)) $custom_value_save[$k] = $v;
					}
				}
				
				$file_id = $this->save_file_to_db($file, $content_id, $custom_value_save);
				
				$status['file_id'] = $file_id;

				$status['file'] = array(
					'url' => \Uri::base(false).'upload'.$this->path.$file['saved_as'],
					'name' => $file['saved_as'],
					'id' => $file_id,
					'content_id' => $content_id,
					'template_field_id' => $this->field->id,
					'content_value_id' => $this->content_field_value_id,
					'created_at' => date('d.m.Y H:i:s')
				);
				
				$status['ses'] = \Session::get('uploads_1');
				
			}
		}
		else
		{
			foreach (Upload::get_errors() as $file)
			{
				print_r($file);
			}
		}

		exit(\Format::forge($status)->to_json());
	}
	
	private function save_file_to_db($file, $content_id, $custom_value_save = array())
	{
		$db_file = new Model_File(array(
			'path' => $this->path,
			'size' => $file['size'],
			'orginal_name' => $file['name'],
			'name' => $file['saved_as'],
			'custom_value' => \Format::forge($custom_value_save)->to_json()
		));
	
		$db_file->save();
		
		if($content_id > 0)
		{
			if(!$field = current(Model_Contents_Fields_Value::find('all', array(
				'where' => array(
					array('template_field_id', '=', $this->field->id),
					array('content_id', '=', $content_id),
					array('language_id', '=', $this->language_id)
				)
			
			))))
			{
				$field = new Model_Contents_Fields_Value(array(
					'template_field_id' => $this->field->id,
					'content_id' => $content_id,
					'language_id' => $this->language_id
				));
			
				$field->save();
			
			}

			$this->content_field_value_id = $field->id;
			
			$field_file = new Model_Contents_Fields_File(array(
				'file_id' => $db_file->id,
				'content_field_id' => $field->id
			));
			$field_file->save();
		}
		else
		{

			$hash = md5(microtime(true).rand(000000, 99999));

			\Session::set('uploads_'.$this->field->template_id.'.'.$this->field->key_name.'.'.$hash, array(
				'data' => array(
					'name' => $file['saved_as'],
					'created_at' => time(),
					'file_id' => $db_file->id
				),
				'0' => $this->path.$file['saved_as'],
				'file_id' => $db_file->id,
				'template_field_id' => $this->field->id,
				'hash' => $hash
			));
			
		}

		return $db_file->id;
		
	}
	
	private function do_custom_image_manipulation($file, $custom_value)
	{
		if(!empty($custom_value['resize']))
		{
			if(!empty($custom_value['resize']) && is_array($custom_value['resize']))
			{
				$dir = $file['saved_to'];
				list($width, $height) = getimagesize($file['saved_to'].$file['saved_as']);
				
				foreach($custom_value['resize'] as $v)
				{
					if(!file_exists($dir.$v['folder'])) \File::create_dir($dir, $v['folder'], 0755);

					$image = \Image::load($dir.$file['saved_as'])->config(array('quality' => 100));
					
					if(empty($v['crop']))
					{
						if($width > $v['width'])
						{					
							$image->resize($v['width'], $v['height'], true);
						}
					}
					else
					{
						$image->crop_resize($v['width'], $v['height']);

					}
					$image->save($dir.$v['folder'].DS.$file['saved_as'], 0777);
					
					/* Set grayscale to bw folder */
					if(!empty($v['bw']))
					{
						$image = \Image::load($dir.$v['folder'].DS.$file['saved_as'])->grayscale();
						if(!file_exists($dir.$v['folder'].DS.'bw')) \File::create_dir($dir.$v['folder'].DS, 'bw', 07777);
						$image->save($dir.$v['folder'].DS.'bw'.DS.$file['saved_as'], 0777);
					}
				}		
			}
		}
	
	}
	
	private function do_custom_file_manipulation($file, $custom_value)
	{
	
	
	}	
	
	
	
}