<?php

class Controller_Admin_Base extends Controller_Template{

	public $template = 'admin/template';
	
	public $current_user = null;
	
	public $current_cat = null;
	
	public $auth = null;
	
	public $css = array();
	
	public $js = array();

	public function before()
	{
		parent::before();
		
		\Config::set('language', 'en');
		\Lang::load('admin', true);


		
		$language = $this->get_language();
		if(empty($language->id)) die('no languages was found');
		\View::set_global('current_language', $language);
		$this->language_id = $language->id;
		
		\View::set_global('language_id', $this->language_id);
		
        $this->auth = \Auth::instance('Simpleauth');

		if(\Uri::segment(2) == 'logout' || \Input::get('logout'))
		{
			$this->auth->logout();
			\Response::redirect(\Uri::create('admin'));
		}
		
		if($this->auth->check())
		{
			if(!$this->current_user)
			{	
				$user_auth = $this->auth->get_user_id();
				if(!empty($user_auth[1]))
				{
					$groups = current($this->auth->get_groups());

					$this->current_user = array(
						'id' => $user_auth[1],
						'username' => $this->auth->get_screen_name(),
						'group' => !empty($groups[1]) ? $groups[1] : 0
					);
					
					
					if(\Uri::segment(2) != 'login') $this->check_cat_permissions();
					
					\View::set_global('current_cat', $this->current_cat);
					
					$this->template->header = ViewModel::forge('admin/cat');
					$this->template->header->set('user_group', $this->current_user['group']);
				}
			}
		}

		if(!$this->current_user && \Uri::segment(2) != 'login')
		{
			
			\Response::redirect(\Uri::create('admin/login'));
		}
		\View::set_global('current_user', $this->current_user);
		\View::set_global('css', $this->css);
		\View::set_global('js', $this->js);
	}

	public function action_login()
	{
		if($this->current_user) \Response::redirect(\Uri::create('admin/dashboard'));
		
		if(\Input::is_ajax() && \Input::method() == 'POST')
		{
			$status = array(
				'c' => 0
			);
			
			$val = \Validation::forge();
			$val->add('username', \Lang::get('admin.email'))->add_rule('required')->add_rule('valid_email');
			$val->add('password', \Lang::get('admin.password'))->add_rule('required');
		
			$val->set_message('valid_email', \Lang::get('admin.email_not_valid'));
			$val->set_message('required', \Lang::get('admin.field_required'));
			
			if($val->run())
			{			
				if ($this->auth->login(Input::post('username'), Input::post('password')))
				{
					$status['c'] = 1;
					$status['msgs'] = \Lang::get('admin.user_loged_in');
				}
				else
				{
					$status['msgs'] = \Lang::get('admin.user_not_found');
				}
			}
			else
			{
				$errors = array();
				foreach($val->error(null) as $k=>$v) $errors[] = (string)$v;				
				$status['msgs'] = $errors;
			}
			exit(\Format::forge($status)->to_json());
		}

		$this->template->js = array(
			'admin/login.js'
		);

		$view = array();
		$this->template->content = \View::forge('admin/login', $view, false);
	}
	
	private function check_cat_permissions()
	{
		$controller = "";
		if(\Uri::segment(2)) $controller .= \Uri::segment(2);
		if(\Uri::segment(3) && \Uri::segment(3) != 'index') $controller .= '/'.\Uri::segment(3);
	
		if(!$current_cat = current(Model_Admin_Cat::find('all', array(
			'related' => array(
				'permission' => array(
					'where' => array(
						array('group', '=', $this->current_user['group'])
					)
				)
			),
			'where' => array(
				array('controller', '=', $controller)
			)
		))))
		{
			$this->auth->logout();
			\Response::redirect(\Uri::create('admin/dashboard'));
		}
		$this->current_cat = array(
			'id' => !empty($current_cat->parent_id) ? $current_cat->parent_id : $current_cat->id,
			'title' => $current_cat->title,
			'controller' => $current_cat->controller
		);
		
		$this->template->title = $this->current_cat['title'];
		
	}
	
	private function get_language()
	{
		$language = null;
		if(\Input::get('language_id'))
		{
			$language = Model_Language::find(\Input::get('language_id'));
		}
		
		if(empty($language->id))
		{
			$language = current(Model_Language::find('all', array(
				'where' => array(
					array('primary', '=', 1)
				)
			)));
		}
		
		if(empty($language->id))
		{
			$language = current(Model_Language::find('all'));
		}
		
		return $language;
		
	}
	
}