<?php 
	class Controller_Admin_User extends Controller_Admin_Base
	{
		public function before()
		{
			parent::before();
			$this->user_groups = Model_User::groups();
		}
		
		public function action_index()
		{
			$where = array();
			
			
			
			$where['where'] = array(
				array('group', '<', 100)
			);
			
			if($this->current_user['group'] == 100) $where = array();
			if($this->current_user['group'] != 100)
			{	
				unset($this->user_groups[0], $this->user_groups[100]);
			}
			
			$where['related'] = array();
			
			$users = Model_User::find('all', \Arr::merge(array('from_cache' => false), $where));
			
			if(\Input::method() == 'POST')
			{
				if(!\Input::post('id'))
				{
					$user = $this->create_user();
					if(!empty($user->id)) $users[$user->id] = $user;
				}
				
				if(is_array(\Input::post('id')))
				{
					foreach(\Input::post('id') as $user_id)
					{
						if(empty($users[$user_id])) continue;

						if(\Input::post('delete.'.$user_id))
						{
							$users[$user_id]->delete();
						}
						else
						{
							$users[$user_id]->fullname = \Input::post('fullname.'.$user_id);
							$users[$user_id]->email = \Input::post('email.'.$user_id);

							if(!empty($this->user_groups[\Input::post('group.'.$user_id)]))
							{
								$users[$user_id]->group = (int)\Input::post('group.'.$user_id);
							}
							if(\Input::post('password.'.$user_id))
							{
								$users[$user_id]->password = $this->auth->hash_password(md5(\Input::post('password.'.$user_id)));
							}

							$users[$user_id]->save();
						}
					}
				}
			}
			
			$view['groups'] = $this->user_groups;
			$view['users'] = $users;
			$this->template->content = \View::forge('admin/user/index', $view, false);
		}
		
		
		private function create_user()
		{
			$val = Validation::forge();
		
			$val->add('username', \Lang::get('admin.username'), array(), array('required'));
			$val->add('email', \Lang::get('admin.email'), array(), array('required', 'valid_email'));
			$val->add('password', \Lang::get('admin.password'), array(), array('required'))->add_rule('min_length', 5);
			
			if(current(Model_User::find('all', array(
				'where' => array(
					array('username', '=', \Input::post('username'))
				)
			))))
			{
				\Session::set_flash('error', \Lang::get('admin.username_exists'));
				return false;
			}
			
			$user_groups = $this->user_groups;
			
			if(!empty($user_groups[0])) unset($user_groups[0]);
			
			if(empty($user_groups[\Input::post('group')]))
			{
				\Session::set_flash('error', \Lang::get('admin.group_error'));
				return false;			
			}
			
			
			if($val->run())
			{
				$user = new Model_User(array(
					'username' => \Input::post('username'),
					'email' => \Input::post('email'),
					'password' => $this->auth->hash_password(md5(\Input::post('password'))),
					'group' => \Input::post('group'),
					'last_login' => 0,
					'login_hash' => '',
					'profile_fields' => '',
					'fullname' => '',
					'updated_at' => 0
				));
				
				$user->save();
				\Session::set_flash('create_ok', \Lang::get('admin.user_created'));
				return $user;
			}
			else
			{
				$errors = "";
				foreach($val->error(null) as $v) $errors .= (string)$v."<br/>";
				\Session::set_flash('error', $errors);
			}
		}
		
		
		
		
		
	}