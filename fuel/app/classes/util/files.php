<?php 
	namespace Util;
	class Files{
	
		public static function set_files($field_files, $template_field_id, $single = false)
		{
			if(empty($field_files)) return array();
			$files = array();
			$upload_folder = 'upload';
			$i = 0;

			foreach($field_files as $field_file)
			{	
				$file_id = $field_file->file_id;;
				if(!empty($field_file->files))
				{
					$custom_value = !empty($field_file->files->custom_value) ? \Format::forge($field_file->files->custom_value, 'json')->to_array() : array();
					$path =  $upload_folder.$field_file->files->path;
					
					$files[$file_id]['data'] = array(
						'file_id' => $field_file->files->id,
						'name' => $field_file->files->name,
						'created_at' => $field_file->files->created_at,
						'template_field_id' => $template_field_id,
						'content_field_id' => $field_file->content_field_id,
						'custom_value' => !empty($field_file->custom_value) ? \Format::forge($field_file->custom_value, 'json')->to_array() : array()
					);
					$files[$file_id][0] = $path.$field_file->files->name;
					
					if(!empty($custom_value['resize']))
					{
						foreach($custom_value['resize'] as $file_data)
						{
							$files[$file_id][$file_data['folder']] = $path.$file_data['folder'].'/'.$field_file->files->name;
							if(!empty($file_data['bw']))
							{
								$files[$file_id][$file_data['folder'].'_bw'] = $path.$file_data['folder'].'/bw/'.$field_file->files->name ;
							}
						}
					}
					$i++;
				}
			}
			return $files;
		}

		public static function set_session_files($session_files)
		{
			$files = array();
			if(empty($session_files)) return null;
			$i = 0;
			foreach($session_files as $key_name => $file_row)
			{
				if(empty($files[$key_name])) $files[$key_name] = array(); 
				foreach($file_row as $file_data)
				{
					if(!empty($file_data['data']))
					{
						$files[$key_name][$i] = array(
							'data' => $file_data['data'],
							0 => 'upload/'.$file_data[0]
						);
						
						$i++;
					}
				}
			}

			return $files;
		}
		
		public static function remove_file_by_field_id($content_field_id)
		{
			foreach(\Model_Contents_Fields_File::find('all', array(
				'where' => array(
					array('content_field_id', '=', $content_field_id)
				)
			)) as $v)
			{	
				self::remove_file_by_id($v->file_id);
			}
		}
		
		public static function remove_file_by_id($file_id)
		{
			if(!$file = \Model_File::find($file_id)) return false;
			
			$path = DOCROOT.'upload'.DS;
			
			$custom_value = !empty($file->custom_value) ? \Format::forge($file->custom_value, 'json')->to_array() : null;
			
			if($custom_value)
			{
				if(!empty($custom_value['resize']))
				{
					foreach($custom_value['resize'] as $sub_file)
					{
						if(empty($sub_file['folder'])) continue;
						if(file_exists($path.$file->path.$sub_file['folder'].DS.$file->name)) \File::delete($path.$file->path.$sub_file['folder'].DS.$file->name);	
						
						if(!empty($sub_file['bw'])) 
						{
							if(file_exists($path.$file->path.$sub_file['folder'].DS.'bw'.DS.$file->name)) \File::delete($path.$file->path.$sub_file['folder'].DS.'bw'.DS.$file->name);
						}
						
					}
				}

			}
		
			if(file_exists($path.$file->path.$file->name)) \File::delete($path.$file->path.$file->name);
		
			foreach(\Model_Contents_Fields_File::find('all', array(
				'where' => array(
					array('file_id', '=', $file_id)
				)
			)) as $link) $link->delete();
		
			$file->delete();

		}
		
		
		
		
		
		
	}