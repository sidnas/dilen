<?php 
	namespace Util;
	class Component{
	
		private static $component;
		
		private static $current_language;
		
		private static $params;
		
		public static function get($component_name, $current_language, $params = array())
		{	
			
			if(!method_exists(get_called_class(), 'component_'.$component_name)) return null;			
			
			self::$current_language = $current_language;
			
			self:$params = $params;
			
			if(!is_int($component_name))
			{
				$components = current(\Model_Template::find('all', array(
					'related' => array(
						'contents' => array(
							'related' => array(
								'content_langs' => array(
									'where' => array(
										array('language_id', '=', $current_language->id),
										array('active', '=', 1)
									)
								),							
								'fields' => array(
									'related' => array(
										'template_fields',
										'field_files' => array(
											'related' => array(
												'files'
											)
										)
									),
									'where' => array(
										array('language_id', '=', $current_language->id)
									)
								)
							),
						)
					),
					'where' => array(
						array('controller', '=', $component_name)
					)				
				)));
				if(empty($components->contents)) return null;
				$content = current($components->contents);
	
			}
			else
			{
				exit('TODO');
			}
	
			if(empty($content->content_langs)) return null;
			$content->content_langs = current($content->content_langs);
				
			$custom_values = array();
			$files = array();
			if(!empty($content->fields))
			{
				foreach($content->fields as $v)
				{
					$custom_values[$v->template_fields->key_name] = !empty($v->value) ? $v->value : '';
					if(!empty($v->field_files)) $files[$v->template_fields->key_name] = \Util\Files::set_files($v->field_files, $v->template_field_id);				
				}
			}
			
			self::$component = array(
				'basic' => array(
					'id' => $content->id,
					'title' => ($content->content_langs ? $content->content_langs->title : null),
					'slug' => ($content->content_langs ? $content->content_langs->slug : null),
					'template_id' => $content->template_id
				),
				'value' => $custom_values,
				'files' => $files
			);

			$class_name = 'component_'.$component_name;
			return self::$class_name();			
			
		}
	
		public static function component_header()
		{
			$view = array();

			$inst = new \Util\Translation;
			$view['translation'] = $inst->init_(self::$component['basic']['template_id'], self::$current_language->id);
			$view['comp'] = self::$component;
			return \View::forge('web/component/header', $view, false);
		}

	}