<?php 
	namespace Util\Admin;
	class Statistic
	{
		public static function active_inactive_posters()
		{	
			$results = array(
				'active' => 0,
				'inactive' => 0
			);
			
			$r = \DB::select(\DB::expr('COUNT(*) as c'), \Model_Contents_Lang::get_table_name().'.active')
				->from(\Model_Contents_Poster::get_table_name())
					->join(\Model_Content::get_table_name())
						->on(\Model_Contents_Poster::get_table_name().'.content_id', '=', \Model_Content::get_table_name().'.id')
					->join(\Model_Contents_Lang::get_table_name())
						->on(\Model_Content::get_table_name().'.id', '=', \Model_Contents_Lang::get_table_name().'.content_id')
				->group_by(\Model_Contents_Lang::get_table_name().'.active')
				->execute()
				->as_array();

			if(empty($r)) return $results;
	
			foreach($r as $v)
			{
				if($v['active'] == 1) $results['active'] = $v['c'];
				if($v['active'] == 0) $results['inactive'] = $v['c'];	
			}
			
			
			return $results;
		}
		
		public static function prod_cats()
		{
			$results = array(
				'prod_tpls' => 0,
				'prod_cat_tpls' => 0
			);		
	
			$r = \DB::select(\DB::expr('COUNT(*) as c'), \Model_Template::get_table_name().'.type')
				->from(\Model_Template::get_table_name())
				->where(\Model_Template::get_table_name().'.type', 'in', array('product', 'product_cat'))
				->group_by(\Model_Template::get_table_name().'.type')
				->execute()
				->as_array();
		
			if(empty($r)) return $results;
	
			foreach($r as $v)
			{
				if($v['type'] == 'product') $results['prod_tpls'] = $v['c'];
				if($v['type'] == 'product_cat') $results['prod_cat_tpls'] = $v['c'];	
			}		
		
			return $results;
		}
		
	}
