<?php 
	namespace Util;
	class Views
	{
		public static function gallery($images)
		{
			if(empty($images)) return null;
			$view['images'] = $images;
			return \View::forge('web/component/gallery', $view, false);
		}
	
	
	}