<?php 
	namespace Util;
	class Cache
	{	
		public static function get($key, $is_array = true)
		{
			try
			{	
				$cache = \Cache::get($key);
				if($is_array) return \Format::forge($cache, 'json')->to_array();
				return $cache;
			}
			catch (\CacheNotFoundException $e)
			{
				return null;
			}
		}
		
		public static function save($key, $value, $is_array = true, $expiration = 360)
		{			
			\Cache::set($key, ($is_array ? \Format::forge($value)->to_json() : $value), $expiration);
			return true;
		}
	
		public static function delete($keys)
		{
			if(is_array($keys))
			{
				foreach($keys as $key) \Cache::delete_all($key);
				return true;
			}
			\Cache::delete_all($keys);
			return true;
			
		}

	}