<?php
	namespace Util;
	class Option
	{
		
		public static function get_inputs_by($value_tree, $depth = 0, $chosen = 'chosen')
		{
			if(empty($value_tree)) return null;

			$html = '<div class="value-tree-holder tree-depth-'.$depth.'">';
			foreach($value_tree as $k => $v)
			{
				if(empty($v['id'])) continue;
	
				$delete = \Html::anchor('javascript:;', '', array(
					'class' => 'fa fa-times customAction delete',
					'data-action' => 'removeFilterValues',
					'data-id' => !empty($v['id']) ? $v['id'] : '-1'
				));

				$html .= '<div class="label '.$chosen.'">'.($depth ? '⊥ ' : '').$v['value'].' '.$delete.'</div>';
				if(!empty($v['sub'])) $html .= self::get_inputs_by($v['sub'], $depth + 1);
			}
	
			$html .= '</div>';
			return $html;
		}
	
		public static function get_value_tree($value_tree, $depth = 0, $chosen = 'chosen', $is_multi = false)
		{
			if(empty($value_tree)) return null;

			$html = '';
			if($is_multi)
			{
				$html = '<div class="value-tree-holder tree-depth-'.$depth.'">';
			}
			
			$i = 1;
			foreach($value_tree as $k => $v)
			{
				$group = array('', '');
				if(!$depth)
				{
					$group = array('<div class="value-group">', '</div>'.($i > 0 && (!is_float($i/4)) ? '<div class="split"><div class="fading-border-top"></div></div>' : ''));
				}
				
				if(empty($v['id'])) continue;

				if($is_multi)
				{
					$html .= $group[0];
					$html .= '<div class="label '.$chosen.' label-'.$depth.'">'.$v['value'].'</div>';
				}
				else
				{
					$html .= $v['value'].', ';
				}
				if(!empty($v['sub'])) $html .= self::get_value_tree($v['sub'], $depth + 1, $chosen, $is_multi);
				
				if($is_multi) $html .= $group[1];
				$i++;
			}
	
			if($is_multi) $html .= '</div>';

			return $html;
		}
	
		private static function collect_filter_value_ids($value_tree, $current_ids = null)
		{
			$ids = array();
			foreach($value_tree as $v)
			{
				if(!empty($v['id']))
				{
					$ids[] = $v['id'];
					if(!empty($v['sub'])) $ids = \Arr::merge($ids, self::collect_filter_value_ids($v['sub']));
				}
			}
			return $ids;
		}
	
	
	
	
	
	
	}
