<?php 
	
	namespace Util;
	class Twitter
	{
		private static $tma = null;
		
		private static $cache_expire = 1;
		
		public static function get_feeds($limit = 20, $params = array())
		{
			require APPPATH.'vendor'.DS.'tma'.DS.'tma.php';
			self::$tma = new \tmhOAuthExample();
			
			$cache_feeds = \Util\Cache::get('tweets');
			if(!empty($cache_feeds)) return $cache_feeds;
	
			$mentions = !empty($params['keyword']) ? self::get_mentions($limit, $params['keyword']) : array();
			$tweets = !empty($params['username']) ? self::get_tweets($limit, $params['username']) : array();
			
			if(empty($tweets) && empty($mentions)) return array();
			
			$mentions_and_tweets = \Arr::merge($tweets, $mentions);
			krsort($mentions_and_tweets);

			$feeds = array();
			$i = 1;
			foreach($mentions_and_tweets as $v)
			{
				$feeds[] = $v;				
				if($i >= $limit) break;
				$i++;
			}
			
			\Util\Cache::save('tweets', $feeds, 60 * self::$cache_expire);
			
			return $feeds;
			
		}
		
		private static function get_mentions($limit = 10, $keyword = null)
		{
			$code = self::$tma->request('GET', self::$tma->url('1.1/search/tweets.json?q='.$keyword), array(
				'q' => $keyword,
				'count' => $limit));
				
			if(!self::$tma->response['response']) return null;
			
			$response = self::$tma->response['response'];
			$twitFeed = json_decode($response, true);

			$feeds = array();
			foreach ($twitFeed['statuses'] as $k => $v) 
			{
				$time = strtotime($v['created_at']);
				$feeds[$time] = array(
					'created_at' => $time,
					'date' => date('d.m.Y H:i:s', $time),				
					'text' => self::update_data($v['text'])
				
				);

			}
			return $feeds;			
		}
		
		private static function get_tweets($limit = 10, $username = null)
		{

			$code = self::$tma->request('GET', self::$tma->url('1.1/statuses/user_timeline'), array(
				'screen_name' => $username,
				'count' => $limit));
			if(!self::$tma->response['response']) return null;
			
			$response = self::$tma->response['response'];
			$twitFeed = json_decode($response, true);

			$feeds = array();
			foreach ($twitFeed as $k => $v) 
			{
				$time = strtotime($v['created_at']);
				$feeds[$time] = array(
					'created_at' => $time,
					'date' => date('d.m.Y H:i:s', $time),
					'text' => self::update_data($v['text'])
				);
			}	
			
			return $feeds;
			
		}
		
		private static function update_data($string)
		{
			$stromg = trim(strip_tags(trim($string)), "\x00..\x1F");
			$string = self::make_urls($string);
			return $string;
		}
		
		private static function make_urls($text)
		{
			$regex = '#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#';
					return preg_replace_callback($regex, function ($matches) {
				return '<a href="'.$matches[0].'" target="_blank">'.$matches[0].'</a>';
			}, $text);
		}
		
		
		
		
	}