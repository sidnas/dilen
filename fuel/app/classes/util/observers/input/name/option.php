<?php 
Namespace Util\Observers\Input\Name;

class Option extends \Orm\Observer
{
	private function input_name_exists($option_id = null, $input_name)
	{
		if(empty($input_name)) $input_name = \Str::random('numeric', 16);
		$input_name = \Inflector::friendly_title($input_name, '_', true);
		if(empty($input_name) && !empty($option_id)) $input_name = $option_id;

		$where =  array(
			'where' => array(
				array('input_name', '=', $input_name),
				array('id', '!=', $option_id)
			)		
		);

		if(empty($option_id)) unset($where['where'][1]);

		if(current(\Model_Option::find('all', $where)))
		{
			$new_input_name = $input_name.'_'.(!empty($option_id) ? $option_id : \Str::random('numeric', 5));
			return $new_input_name;
		}

		return $input_name;
	}

    public function before_insert(\Orm\Model $model)
    {
		$input_name = !empty($model->input_name) ? $model->input_name : null;
		$model->input_name = $this->input_name_exists(!empty($model->id) ? $model->id : null, $input_name);
	}
	
    public function before_update(\Orm\Model $model)
    {
		$input_name = !empty($model->input_name) ? $model->input_name : null;
		$model->input_name = $this->input_name_exists(!empty($model->id) ? $model->id : null, $input_name);
	}
	
	public function before_save(\Orm\Model $model)
	{
		$input_name = !empty($model->input_name) ? $model->input_name : null;
		$model->input_name = $this->input_name_exists(!empty($model->id) ? $model->id : null, $input_name);
	}

}
