<?php 
Namespace Util\Observers\Order\Input\Value;

class Option extends \Orm\Observer
{
    public function before_insert(\Orm\Model $model)
    {
		if(empty($model->order))
		{
			$r = current(\DB::select(\DB::expr('COUNT(*) as c'))->from(\Model_Options_Value::get_table_name())
				->where('parent_id', '=', $model->parent_id)
				->and_where('option_id', '=', $model->option_id)
				->execute()->as_array());
			$order = !empty($r['c']) ? $r['c'] : 1;
			$model->order = $order;
		}
	}

}