<?php 
Namespace Util\Observers\Slug;

class Content extends \Orm\Observer
{
	private function slug_exists($content_id, $slug, $language_id)
	{
		$slug = \Inflector::friendly_title($slug, '-', true);
		
		if(empty($slug)) $slug = $content_id;
		
		if(current(\Model_Contents_Lang::find('all', array(
			'where' => array(
				array('slug', '=', $slug),
				array('language_id', '=', $language_id),
				array('content_id', '!=', $content_id)
			)
		))))
		{
			return $slug.'-'.$content_id;
		}
		return $slug;
	}
	
    public function before_update(\Orm\Model $model)
    {
		$slug = empty($model->slug) ? $model->title : $model->slug;
		$model->slug = $this->slug_exists($model->content_id, $slug, $model->language_id);
	}	
	
    public function before_insert(\Orm\Model $model)
    {
		$slug = empty($model->slug) ? $model->title : $model->slug;
		$model->slug = $this->slug_exists($model->content_id, $slug, $model->language_id);
	}

	public function before_save(\Orm\Model $model)
	{
		$slug = empty($model->slug) ? $model->title : $model->slug;
		$model->slug = $this->slug_exists($model->content_id, $slug, $model->language_id);
	}
	
	
}