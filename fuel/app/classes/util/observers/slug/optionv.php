<?php 
Namespace Util\Observers\Slug;

class Optionv extends \Orm\Observer
{
	private function slug_exists($option_value_id, $slug, $language_id)
	{
		$slug = \Inflector::friendly_title($slug, '-', true);
		if(empty($slug)) $slug = $option_value_id;
		
		if(current(\Model_Options_Values_Lang::find('all', array(
			'where' => array(
				array('slug', '=', $slug),
				array('language_id', '=', $language_id),
				array('option_value_id', '!=', $option_value_id)
			)
		))))
		{
			return $slug.'-'.$option_value_id;
		}
		return $slug;
	}
	
	
    public function before_insert(\Orm\Model $model)
    {
		$slug = empty($model->slug) ? $model->value : $model->slug;
		$model->slug = $this->slug_exists($model->option_value_id, $slug, $model->language_id);
	}
	
    public function before_update(\Orm\Model $model)
    {
		$slug = empty($model->slug) ? $model->value : $model->slug;
		$model->slug = $this->slug_exists($model->option_value_id, $slug, $model->language_id);
	}
	
	public function before_save(\Orm\Model $model)
	{
		$slug = empty($model->slug) ? $model->value : $model->slug;
		$model->slug = $this->slug_exists($model->option_value_id, $slug, $model->language_id);
	}
	
	
}