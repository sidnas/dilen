<?php 
	namespace Util;
	class Sys
	{	
		public static function created_before($created_at, $time_chars)
		{
			$seconds_ago = time() - $created_at;
			if($seconds_ago < 60) return $seconds_ago.$time_chars['s'];
			if($seconds_ago < 3600) return round($seconds_ago / 60).$time_chars['m'];
			if($seconds_ago < 86400) return round($seconds_ago / 3600).$time_chars['h'];		
			return round($seconds_ago / 86400).$time_chars['d'];			
		}
		
		public static function filter_params()
		{
			$params = array(
				'where' => array()
			);
			if(is_array(\Input::get('filter')))
			{
				$content_filters = array();
				foreach(\Input::get('filter') as $value)
				{	
					$value = (int)$value;
					if(empty($value)) continue;
					$content_filters[] = $value;
				}
				
				if(!empty($content_filters))
				{			
					$filtered_content_ids = \Model_Contents_Filter::get_content_ids_by_filter_values($content_filters);
					if(empty($filtered_content_ids)) $filtered_content_ids[] = -1;
				
					$params = array(
						'related' => array(
							'content' => array(
								'where' => array(
									array('id', 'in', $filtered_content_ids)
								)
							)
						)
					);
				}
			}
			
			if(\Input::get('sub_filter'))
			{
				if(is_array(\Input::get('sub_filter.offer_type')))
				{
					$chosen_both = 0;
					$chosen_offer_type = false;
					foreach(\Input::get('sub_filter.offer_type') as $offer_type)
					{
						$chosen_offer_type = $offer_type;
						$chosen_both++;
					}
					
					if($chosen_both != 2)
					{
						$params['where'][] = array('allow_best_offer', '=', ($chosen_offer_type == 2 ? 1 : 0));
						
					}
				}
				
				$price_from = (int)\Input::get('sub_filter.price.from');
				$price_to = (int)\Input::get('sub_filter.price.to');
				
				if($price_from > 0) $params['where'][] = array('price', '>=', $price_from);
				if($price_to > 0) $params['where'][] = array('price', '<=', $price_to);
	
			}	

			return $params;	
				
		}
		
		public static function current_params($clear = array('filter', 'sub_filter'))
		{
			$params_string = '';
			foreach(\Input::get() as $k => $v)
			{
				if(in_array($k, $clear)) continue;
				$params_string .= $k.'='.$v.'&';
			}
			return substr($params_string, 0, -1);
		}
		
		public static function discount($price, $discount)
		{
	
		
		}
		
        public static function share($type, $params)
		{
			$url = null;
			$w = 525;
			$h = 290;		

			if(empty($params['url'])) $params['url'] = \Uri::base(false);

			foreach($params as $key => $value)
			{
				$params[$key] = !is_array($value) ? urlencode($value) : $value;
			}

			$target_url = $params['url'];
			$title = !empty($params['title']) ? $params['title'] : null;
			$desc = !empty($params['meta_description']) ? $params['meta_description'] : null;
			$image = null;
			if(!empty($params['value']['image']))
			{
				$tmp = current($params['value']['image']);
				if(!empty($tmp[0])) $image = urlencode(\Uri::create($tmp[0]));
			}


			switch($type)
			{
				case 'facebook':
					$url = 'https://www.facebook.com/sharer/sharer.php?u='.$target_url.'&display=popup';
				break;
				case 'twitter':

					$url = 'https://twitter.com/intent/tweet?text='.$title.'&url='.$target_url;
				break;
				case 'gplus':
					$w = 535;
					$h = 400;					
					$url = 'https://plus.google.com/share?url='.$target_url;
				break;
				case 'pinterest':
					$w = 700;
					$h = 300;	
					$url = 'https://www.pinterest.com/pin/create/button/?url='.$target_url.'&media='.$image.'&description='.$desc;
				break;
				case 'draugiem':
				case 'frype':
					$url = 'http://www.draugiem.lv/say/ext/add.php?link='.$target_url.'&title='.$desc.'&nopopup=1'; //text=
				break;	
                case 'odnakalsniki':
                    //$url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl='.$target_url.'&st.comments='.$desc;
                    //$url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl='.$target_url.'&st.title=test1&st.description=test2&st.comments=test3';
                    $url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl='.$target_url;
                break;
			}



			if(!$url) return null;
			return "onClick=\"window.open('".$url."?v=1','_blank', 'width=".$w.", height=".$h."');\"";
		}
		
	}