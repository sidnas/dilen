<?php 
	namespace Util;
	class Translation
	{	
		private $template_id;
		
		private $language_id;
	
		private $translations = array();
	
		private $links = array();
	
		public $cache_name = 'translations';

		public function init_($template_id, $language_id)
		{
			$this->template_id = $template_id;
			$this->language_id = $language_id;
			
			$this->cache_name .= '.'.$language_id;
			$translations = $this->get_all();
			
			$this->translations = $translations['translations'];
			$this->links = $translations['links'];

			return $this;
			
		}
	
		public function get($input_name, $value = '', $type = 0)
		{
			if(empty($this->translations[$input_name]))
			{
				return $this->create_entry($input_name, $value, $type);
			}

			if(!empty($this->links[$input_name]['id']) && !in_array($this->template_id, $this->links[$input_name]['templates']))
			{
				$this->create_link_entry($input_name, $this->links[$input_name]['id']);
			}

			return $this->translations[$input_name];
		}

		private function create_link_entry($input_name, $translation_id)
		{	

			if(!$link_entry = current(\Model_Translations_Template::find('all', array(
				 'where' => array(
					array('translation_id', '=', $translation_id),
					array('template_id', '=', $this->template_id)
				)
			))))
			{	

				$link_entry = new \Model_Translations_Template(array(
					'translation_id' => $translation_id,
					'template_id' => $this->template_id
				));
				$link_entry->save();	

				if(empty($this->links[$input_name]))
				{
					$this->links[$input_name] = array(
						'id' => $translation_id,
						'templates' => array()
					);
				}
				
				$this->links[$input_name]['templates'][] = $this->template_id;
			
				$this->save_to_cache(array(
					'translations' => $this->translations,
					'links' => $this->links
				));
				
			}

			return array(
				'translation_id' => $link_entry->translation_id,
				'tempalte_id' => $link_entry->template_id
			);			
		
			
		}
		
		private function create_entry($input_name, $value, $type)
		{	
			if(!$entry = current(\Model_Translation::find('all', array(
				'where' => array(
					array('input_name', '=', $input_name)
				)
			))))
			{
				$entry = new \Model_Translation(array(
					'input_name' => $input_name,
					'field_type' => $type
				));
				$entry->save();			

				$this->create_link_entry($input_name, $entry->id);
				
			}
			else
			{	
				if(!current(\Model_Translations_Lang::find('all', array(
					'where' => array(
						array('translation_id', '=', $entry->id),
						array('language_id', '=', $this->language_id)
					)
				)))) $value = $input_name;

				$this->translations[$input_name] = $value;	

				$this->save_to_cache(array(
					'translations' => $this->translations,
					'links' => $this->links
				));		
				
				return $value;
			}

			if(!empty($entry->id))
			{
				$entry_lang = new \Model_Translations_Lang(array(
					'translation_id' => $entry->id,
					'language_id' => $this->language_id,
					'value' => $value
				));
				$entry_lang->save();
			}
			
			$this->translations[$input_name] = $value;		

			$this->save_to_cache(array(
				'translations' => $this->translations,
				'links' => $this->links
			));	
				
			return $value;
		}
		
		private function get_all()
		{
			try
			{
				return \Format::forge(\Cache::get($this->cache_name), 'json')->to_array();
			}
			catch (\CacheNotFoundException $e)
			{
				$_translations = \Model_Translation::get_all($this->language_id);

				$translations = array(
					'translations' => array(),
					'links' => array()
				);
				
				foreach($_translations as $k => $v)
				{
					$translations['translations'][$k] = ($v['field_type'] == 1 ? nl2br($v['value']) : $v['value']);
					$translations['links'][$k] = array(
						'id' => $v['id'],
						'templates' => $v['template_ids']
					);
				}

				$this->save_to_cache($translations);

				return $translations;
			}
		}
		
		private function save_to_cache($data)
		{
			\Cache::set($this->cache_name, \Format::forge($data)->to_json());
		}
		
	}
