<?php 
	namespace Util;
	class Contents{
	
	
		public static function remove_by_template_id($template_id)
		{
			set_time_limit(3600);
			foreach(\Model_Content::find('all', array(
				'where' => array(
					array('template_id', '=', $template_id)
				)
			)) as $content)
			{
				self::remove_by_id($content->id);
			}
		}
	
		public static function remove_by_id($content_id)
		{
			if(!$content = \Model_Content::find($content_id)) return false;
		
			foreach(\Model_Contents_Lang::find('all', array(
				'where' => array(
					array('content_id', '=', $content_id)
				)
			)) as $content_lang) $content_lang->delete();
		
			foreach(\Model_Contents_Tree_Node::find('all', array(
				'where' => array(
					array('parent_content_id', '=', $content->id)
				)
			)) as $node)
			{
				self::remove_by_id($node->content_id);
				$node->delete();
			}
		
			self::remove_content_field_values_by_content_id($content->id);
			
			\Model_Contents_Filter::remove_by_content_id($content->id);
			\Model_Contents_Poster::remove_by_content_id($content->id);
			
			
			$content->delete();
			
			foreach(\Model_Contents_Tree_Node::find('all', array(
				'where' => array(
					array('content_id', '=', 0)
				),
				'from_cache' => false
			)) as $node)
			{
				$node->delete();
			}
		}
	
		public static function remove_content_field_values_by_id($field_id)
		{
			if(!$field_value = \Model_Contents_Fields_Value::find($field_id)) return false;
		
			foreach(\Model_Contents_Fields_File::find('all', array(
				'where' => array(
					array('content_field_id', '=', $field_value->id)
				)
			)) as $content_file)
			{
				\Util\Files::remove_file_by_id($content_file->id);
			}
			$field_value->delete();
		}
	
		public static function remove_content_field_values_by_template_field_id($template_field_id)
		{
			foreach(\Model_Contents_Fields_Value::find('all', array(
				'where' => array(
					array('template_field_id', '=', $template_field_id)
				)
			)) as $field_value)
			{
				self::remove_content_field_values_by_id($field_value->id);
			}
		}
	
	
		public static function remove_content_field_values_by_content_id($content_id)
		{
			foreach(\Model_Contents_Fields_Value::find('all', array(
				'where' => array(
					array('content_id', '=', $content_id)
				)
			)) as $field_value)
			{
				foreach(\Model_Contents_Fields_File::find('all', array(
					'where' => array(
						array('content_field_id', '=', $field_value->id)
					)
				)) as $content_file)
				{
					\Util\Files::remove_file_by_id($content_file->file_id);
				}
				$field_value->delete();
			}
		}

	}
