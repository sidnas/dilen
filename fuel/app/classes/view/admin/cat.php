<?php

class View_Admin_Cat extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
	public function view()
	{
		$cats = array();
		foreach(Model_Admin_Cat::find('all', array(
			'related' => array(
				'permission' => array(
					'where' => array(
						array('group', '=', $this->get('user_group'))
					)
				),
				'parents' => array(
					'releated' => array(
						'permission' => array(
							'where' => array(
								array('group', '=', $this->get('user_group'))
							)			
						)
					),
					'order_by' => array(
						array('order', 'asc')
					)
				)
			),
			'where' => array(
				array('parent_id', '=', 0)
			),
			'order_by' => array(
				array('order', 'asc')
			)
		)) as $v)
		{
			if(empty($v->hide))
			{
				$cats[$v->id] = array(
					'id' => $v->id,
					'title' => $v->title,
					'controller' => $v->controller,
					'css_class' => $v->css_class,
					'sub' => array()
				);
			}
			
			if(!empty($v->parents) && empty($v->hide))
			{
				foreach($v->parents as $v2)
				{
					if(empty($v2->hide))
					{
						$cats[$v->id]['sub'][$v2->id] = array(
							'id' => $v2->id,
							'title' => $v2->title,
							'controller' => $v2->controller,			
							'css_class' => $v2->css_class,		
						);
					}
				}
			}
		
		}

		$this->cats = $this->request()->param('cats', $cats);
	}
}
