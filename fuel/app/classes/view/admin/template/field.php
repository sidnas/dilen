<?php
class View_Admin_Template_Field extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
	 
	 private $is_files = array('images', 'files');
	 
	public function view()
	{

		$fields = Model_Templates_Field::find('all', array(
			'where' => array(
				array('template_id', '=', $this->get('template_id'))
			),
			'order_by' => array(
				array('order', 'asc')
			)
		));
		
		$templates_file_custom_fields = $custom_values = array();
		$filter_ids = array();
		
		foreach($fields as $v)
		{
			$custom_value = !empty($v->custom_value) ? \Format::forge($v->custom_value, 'json')->to_array() : array();
			$custom_values[$v->id] = $custom_value;
			
			if(!empty($v->filter_id))
			{
				$filter_ids[] = $v->filter_id;
			}
			if(in_array($v['type'], $this->is_files))
			{

				if(!empty($custom_value['custom_fields']))
				{
					$templates_file_custom_fields[$v->key_name] = $custom_value['custom_fields'];
				
				}
				
			}
		}

		$this->fields = $this->request()->param('fields', $fields);
		
		$files = $this->get('files');
		if(!empty($files))
		{
			foreach($files as $key_name => $file_group)
			{
				if(is_array($file_group))
				{
					$have_custom = !empty($templates_file_custom_fields[$key_name]) ? true : false;
					foreach($file_group as $file_id => $file)
					{
						$files[$key_name][$file_id]['have_custom'] = $have_custom;
					}
				}
			}
			
		}

		$filter_values = array();
		if($this->get('content_id'))
		{
			$filter_values = Model_Contents_Filter::get_value_names($this->get('content_id'));
		}
		
		$this->filter_values = $filter_values;
		$this->values_obj = $this->request()->param('values_obj', $this->get('values_obj'));
		$this->values = $this->request()->param('values', $this->get('values'));
		$this->errors = $this->request()->param('errors', $this->get('errors'));
		$this->update_status = $this->request()->param('update_status', $this->get('update_status'));
		$this->files = $this->request()->param('files', $files);
		$this->templates_file_custom_fields = $templates_file_custom_fields;
		$this->custom_values = $custom_values;
	}
}
