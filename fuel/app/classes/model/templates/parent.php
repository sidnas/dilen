<?php

class Model_Templates_Parent extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'template_id',
		'parent_template_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'templates_parents';

	protected static $_belongs_to = array(
		'templates' => array(
			'key_from' => 'template_id',
			'model_to' => 'Model_Template',
			'key_to' => 'id'	
		)
	);
	
	
}
