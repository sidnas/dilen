<?php

class Model_Templates_Field extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'template_id',
		'name',
		'key_name',
		'filter_id',
		'type',
		'custom_value',
		'searchable',
		'active',
		'required',
		'order',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'templates_fields';

	protected static $_belongs_to = array(
		'templates' => array(
			'key_from' => 'template_id',
			'model_to' => 'Model_Template',
			'key_to' => 'id'			
		)
	);	
	
	public static $_has_one = array(
		'single_content_field' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Fields_Value',
			'key_to' => 'template_field_id'	
		)
	);
	
	public static function get_fields_by_template_id($template_id)
	{
		$fields = array();
		foreach(self::find('all', array(
			'where' => array(
				array('template_id', '=', $template_id)
			)
		)) as $field)
		{
			$fields[$field->key_name] = array(
				'id' => $field->id,
				'key_name' => $field->key_name,
				'required' => $field->required,
				'custom_value' => !empty($field->custom_value) ? \Format::forge($field->custom_value, 'json')->to_array() : array()
			);
		}
		return $fields;
	}
	
	
	public static function get_filter_ids_by_content_ids($content_ids)
	{
		$filter_ids = array(
			'ids' => array(),
			'multi_ids' => array()
		);
		
		if(empty($content_ids)) return $filter_ids;
		
		
		
		foreach(self::find('all', array(
			'where' => array(
				array('filter_id', '>', 0)
			),
			'related' => array(
				'templates' => array(
					'related' => array(
						'contents' => array(
							'where' => array(
								array('id', (is_array($content_ids) ? 'in' : '='), $content_ids)
							)
						)
					)
				)
			)
		
		)) as $field)
		{
			$custom_value = !empty($field->custom_value) ? \Format::forge($field->custom_value, 'json')->to_array() : null;		
			$filter_ids['ids'][] = $field->filter_id;
			if(!empty($custom_value['multi_select'])) $filter_ids['multi_ids'][] = $field->filter_id;
		}
		return $filter_ids;
	}
	
	
	
	
}
