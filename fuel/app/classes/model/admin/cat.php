<?php

class Model_Admin_Cat extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'title',
		'controller',
		'css_class',
		'parent_id',
		'order',
		'hide',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	protected static $_table_name = 'admin_cats';

	protected static $_has_many = array(
		'permission' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Admin_Cats_Permission',
			'key_to' => 'cats_id'			
		),
		'parents' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Admin_Cat',
			'key_to' => 'parent_id'			
		)
	);
	
}
