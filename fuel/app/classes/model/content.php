<?php

class Model_Content extends \Orm\Model
{
	public static $basic_contents = array('view', 'view_product', 'view_blog');
	
	protected static $_properties = array(
		'id',
		'template_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'contents';


	protected static $_belongs_to = array(
		'templates' => array(
			'key_from' => 'template_id',
			'model_to' => 'Model_Template',
			'key_to' => 'id'			
		)
	);

	public static function get_table_name()
	{
		return self::$_table_name;
	}
	
	protected static $_has_one = array(
		'poster' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Poster',
			'key_to' => 'content_id'			
		)
	);
	
	protected static $_has_many = array(
		'content_langs' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Lang',
			'key_to' => 'content_id'			
		),
		'fields' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Fields_Value',
			'key_to' => 'content_id'
		),
		'nodes' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Tree_Node',
			'key_to' => 'content_id'
		),
		'content_filters' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Filter',
			'key_to' => 'content_id'			
		)
	);	

	public static function get_parent_id($child_id)
	{
		if($tmp_child = current(Model_Contents_Tree_Node::find('all', array(
			'where' => array(
				array('content_id', '=', $child_id)
			)
		)))) return $tmp_child['parent_content_id'];
		return null;
	}
	
	public static function get_parent($child_id, $language_id)
	{

		$node = current(
			Model_Contents_Tree_Node::find('all', array(
				'related' => array(
					'contents' => array(
						'related' => array(
							'content_langs' => array(
								'where' => array(
									array('language_id', '=', $language_id)
								)
							)
						)
					)
				),
				'where' => array(
					array('content_id', '=', $child_id)
				),
				'from_cache' => false
			)
		));

		if(empty($node->id)) return null;

		$params = array();
		$contents = !empty($node->contents) ? current($node->contents) : null;
		if(!$contents) return null;
		$lang = !empty($contents->content_langs) ? current($contents->content_langs) : null;
		if(!$lang) return null;
		
		return array(
			'id' => $contents->id,
			'title' => empty($node->parent_content_id) ? \Lang::get('admin.back_to_home') : $lang->title,
			'parent_id' => $node->parent_content_id,
			'position' => $node->position
		);
		
		
	}
	
	public static function get_by_content_id($content_id, $language_id, $full_relation = false, $single_file = true, $from_cache = true)
	{
		$full_relation_data = array();
		if($full_relation)
		{
			$full_relation_data = array(
				'fields' => array(
					'where' => array(
						array('language_id', '=', $language_id)
					),
					'related' => array(
						'template_fields',
						'field_files' => array(
							'related' => array(
								'files'
							)
						),						
					)
				),
				'nodes' => array(
					'related' => array(
						'positions',
					),
					'order_by' => array(
						array('order', 'asc')
					)
				)			
			);
		}
		
		$page = self::find($content_id, array(
			'related' => \Arr::merge($full_relation_data, array(
				'templates',
				'content_langs' => array(
					'where' => array(
						array('language_id', '=', $language_id)
					)
				)
			)), 
			'from_cache' => $from_cache
		));

		if($full_relation) return self::set_content_base_values($page, $single_file);
		
		
		$content_langs = !empty($page->content_langs) ? current($page->content_langs) : null;
		
		return array(
			'basic' => array(
				'id' => $page->id,
				'title' => ($content_langs ? $content_langs->title : null),
				'slug' => ($content_langs ? $content_langs->slug : null),
				'meta_keywords' => ($content_langs ? $content_langs->meta_keywords : null),
				'meta_description' => ($content_langs ? $content_langs->meta_description : null),
				'type' => $page->templates->type,
				'template_id' => $page->template_id
			)		
		);
		
	}
	
	
	public static function get_by_template_type($template_type, $language_id)
	{
		$cache_key = 'content.'.$language_id.'.'.$template_type;
		
		if($cache = \Util\Cache::get($cache_key)) return $cache;

		$contents = array();
		foreach(self::find('all', array(
			'related' => array(
				'templates' => array(
					'related' => array(
						'parents'
					),
					'where' => array(
						array('type', '=', $template_type)
					)
				),
				'content_langs' => array(
					'where' => array(
						array('language_id', '=', $language_id),
						array('active', '=', 1)
					)
				),
				'fields' => array(
					'where' => array(
						array('language_id', '=', $language_id)
					),
					'related' => array(
						'template_fields',
						'field_files' => array(
							'related' => array(
								'files'
							)
						),						
					)
				),
				'nodes' => array(
					'related' => array(
						'positions',
					),
					'order_by' => array(
						array('order', 'asc')
					)
				)
			)
		)) as $content)
		{
			if($tmp_content = self::set_content_base_values($content))
			{
				$contents[$content->id] = $tmp_content;
			}
		}

		\Util\Cache::save($cache_key, $contents);
		return $contents;
	}

	
	public static function set_content_base_values($content, $single_file = true)
	{
		$structure = array();
		
		$content_langs = !empty($content->content_langs) ? current($content->content_langs) : null;
		if($content_langs)
		{		
			$structure = array(
				'id' => $content->id,
				'template_id' => $content->id,
				'title' => $content_langs->title,
				'slug' => $content_langs->slug,
				'meta_keywords' => $content_langs->meta_keywords,
				'meta_description' => $content_langs->meta_description,
				'type' => $content->templates->type,	
				'created_at' => $content->created_at,
				'value' => array()
			);	

			foreach($content->fields as $field_id => $field)
			{	

				if(!empty($field->template_fields->key_name))
				{	
					$key = $field->template_fields->key_name;
					if(!empty($field->field_files))
					{
						$structure['value'][$key] = ($single_file ? current(\Util\Files::set_files($field->field_files, $field->template_field_id)) : \Util\Files::set_files($field->field_files, $field->template_field_id));
					}
					else
					{
						$structure['value'][$key] = $field->value;
					}
				}			

			}			
			
	
			return $structure;
		}
		return null;
	}
	
}
