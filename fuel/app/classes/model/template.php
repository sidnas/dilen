<?php

class Model_Template extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'type',
		'controller',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'templates';

	public static function get_table_name()
	{
		return self::$_table_name;
	}
	
	protected static $_has_many = array(
		'fields' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Templates_Field',
			'key_to' => 'template_id'			
		),
		'contents' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Content',
			'key_to' => 'template_id'			
		),
		'parents' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Templates_Parent',
			'key_to' => 'template_id'
		),
		'translations_templates' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Translations_Template',
			'key_to' => 'template_id'			
		)
			
	);

	public static function get_content_by_type($type, $language_id)
	{
		$data = current(self::find('all', array(
			'related' => array(
				'contents' => array(
					'related' => array(
						'content_langs' => array(
							'where' => array(
								array('language_id', '=', $language_id)
							)
						)
					)
				)
			),	
			'where' => array(
				array('type', '=', $type)
			),
			'limit' => 1
		)));
		
		if(empty($data->contents)) return null;
		
		$content = current($data->contents);
		if(empty($content)) return null;
		
		if(empty($content->content_langs)) return null;
		$content_langs = current($content->content_langs);
		if(empty($content_langs)) return null;
		
		
		return array(
			'basic' => array(
				'id' => $content->id,
				'title' => ($content_langs ? $content_langs->title : null),
				'slug' => ($content_langs ? $content_langs->slug : null),
				'meta_keywords' => ($content_langs ? $content_langs->meta_keywords : null),
				'meta_description' => ($content_langs ? $content_langs->meta_description : null),
				'type' => $data->type,
				'template_id' => $data->id
			)		
		);
	}
	
}
