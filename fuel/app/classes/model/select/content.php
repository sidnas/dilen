<?php

class Model_Select_Content extends Model_Content
{
	public static function get_list($language_id, $position, $parent_content_id = null, $params = array())
	{
		$nodes = Model_Contents_Tree_Node::query();
		$nodes->related('contents');
		$nodes->related('contents.templates');
		$nodes->related('contents.content_langs', array(
			'where' => array(
				array('language_id', '=', $language_id),
				array('active', '=', 1)				
			)
		));

		$nodes->related('contents.fields', array(
			'related' => array(
				'template_fields',
				'field_files' => array(
					'related' => array(
						'files'
					)
				)
			), 
			'where' => array(
				array('language_id', '=', $language_id)
			)
		));

		$nodes->related('contents.fields.field_files.files');

		$nodes->where('position', '=', $position);

		if($parent_content_id >= 0)
		{
			$nodes->where('parent_content_id', '=', $parent_content_id);	
		}

		if(!empty($params['where']))
		{
			foreach($params['where'] as $arr)
			{
				if(empty($arr[1]))
				{
					$nodes->$arr[0];
				}
				else
				{
					$nodes->$arr[0]($arr[1], $arr[2], $arr[3]);
				}
			}
		}

		if(!empty($params['order_by']))
		{
			$nodes->order_by($params['order_by'][0], $params['order_by'][1]);
		}
		else
		{
			$nodes->order_by('order', 'asc');
		}


		if(!empty($params['count_only']))
		{
			return $nodes->count();
		}
		//$nodes->from_cache(false);
		$_nodes = array();
		foreach($nodes->get() as $node_id => $obj)
		{
			$_nodes[$node_id] = self::set_base_data(current($obj->contents));
		}

		return $_nodes;
	}

	public static function get_by_content_ids($ids, $language_id)
	{
		$nodes = self::query();
		$nodes->related('templates');
		$nodes->related('content_langs', array(
			'where' => array(
				array('language_id', '=', $language_id),
				array('active', '=', 1)				
			)
		));

		$nodes->related('fields', array(
			'related' => array(
				'template_fields',
				'field_files' => array(
					'related' => array(
						'files'
					)
				)
			), 
			'where' => array(
				array('language_id', '=', $language_id)
			)
		));

		$nodes->where('id', (is_array($ids) ? 'in' : '='), $ids);

		$_nodes = array();
		foreach($nodes->get() as $node_id => $obj)
		{
			$_nodes[$node_id] = self::set_base_data($obj);
		}

		return (!is_array($ids) ? current($_nodes) : $_nodes);
	}




	public static function set_base_data($content, $single_file = null)
	{
		$structure = array();

		$content_langs = !empty($content->content_langs) ? current($content->content_langs) : null;
		if($content_langs)
		{		
			$structure = array(
				'id' => $content->id,
				'template_id' => $content->id,
				'title' => $content_langs->title,
				'slug' => $content_langs->slug,
				'meta_keywords' => $content_langs->meta_keywords,
				'meta_description' => $content_langs->meta_description,
				'type' => $content->templates->type,	
				'created_at' => $content->created_at,
				'value' => array()
			);	

			foreach($content->fields as $field_id => $field)
			{	
				if(!empty($field->template_fields->key_name))
				{	
					$key = $field->template_fields->key_name;
					if(!empty($field->field_files))
					{
						$structure['value'][$key] = ($single_file ? current(\Util\Files::set_files($field->field_files, $field->template_field_id)) : \Util\Files::set_files($field->field_files, $field->template_field_id));
					}
					else
					{
						$structure['value'][$key] = $field->value;
					}
				}			

			}			
			
	
			return $structure;
		}
		return null;
	}
}
