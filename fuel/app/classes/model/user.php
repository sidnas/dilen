<?php

class Model_User extends \Orm\Model
{

	protected static $_properties = array(
		'id',
		'username',
		'fullname',
		'email',
		'group' ,
		'password',
		'last_login',
		'login_hash',
		'profile_fields',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	protected static $_has_many = array(
		'posters' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Poster',
			'key_to' => 'user_id'		
		)
	);
	
	protected static $_table_name = 'users';
	
	public static function set_data_array($data)
	{
		$d = array();

		foreach(self::$_properties as $v)
		{
			if(!isset($data[$v])) return null;
			$d[$v] = $data[$v];
		}
		return $d;
	}
	
	public static function get_single($user_id)
	{
		$user = self::set_data_array(self::find($user_id));
		if(empty($user)) return null;
		
		if(empty($user['confirmed']))
		{
			$user['email'] = '';
		}

		return $user;
	}
	
	public static function groups()
	{
		return array(
			0 => '----',
			1 => \Lang::get('admin.public_user'),
			100 => \Lang::get('admin.developer'),
			50 => \Lang::get('admin.manager')
		);
	}
	
	
}
