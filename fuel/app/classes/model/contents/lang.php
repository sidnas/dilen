<?php

class Model_Contents_Lang extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'content_id',
		'title',
		'slug',
		'meta_keywords',
		'meta_description',
		'visible',
		'active',
		'language_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
	    'Util\Observers\Slug\Content' => array(
            'events' => array('before_insert', 'before_update')
        ),	
		
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	
	//before_insert
	
	
	protected static $_table_name = 'contents_langs';

	public static function get_table_name()
	{
		return self::$_table_name;
	}
	
	
	protected static $_belongs_to = array(
		'content' => array(
			'key_from' => 'content_id',
			'model_to' => 'Model_Content',
			'key_to' => 'id'			
		)
	);
	
	public static function get_by_search_in_title($title_keyword, $parent_page_id = false)
	{
		$data = $where_parent = array();	

		if($parent_page_id)
		{
			$where_parent = array(
				'where' => array(
					array('parent_content_id', '=', $parent_page_id)
				)			
			);		
		}
		
		foreach(Model_Content::find('all', array(
			'related' => array(
				'content_langs' => array(
					'where' => array(
						array('title', 'like', '%'.$title_keyword.'%'),
						array('active', '=', 1)
					)
				),
				'nodes' => $where_parent
			),
			'order_by' => array(
				array('id', 'desc')
			)
		)) as $content_id => $content)
		{
			$content_langs = !empty($content->content_langs) ? current($content->content_langs) : null;
			if(!$content_langs) continue;
			$data[$content_id] = array(
				'id' => $content->id,
				'title' => $content_langs->title,
				'slug' => $content_langs->slug
			);
		}
		return $data;
	}
	
	public static function get_by_unneeded_content_ids($content_ids, $parent_page_id = false)
	{
		$data = $where = $where_parent =  array();		
		
		if(!empty($content_ids))
		{
			$where = array(
				array('id', 'not in', $content_ids)						
			);
		}
		
		if($parent_page_id)
		{
			$where_parent = array(
				'where' => array(
					array('parent_content_id', '=', $parent_page_id)
				)			
			);		
		}
		
		foreach(Model_Content::find('all', array(
			'related' => array(
				'content_langs' => array(
					'where' => array(
						array('active', '=', 1)
					)
				),
				'nodes' => $where_parent
			),
			'where' => !empty($where) ? array($where) : array(),
			'order_by' => array(
				array('id', 'desc')
			)
		)) as $content_id => $content)
		{
			$content_langs = !empty($content->content_langs) ? current($content->content_langs) : null;
			if(!$content_langs) continue;
			$data[$content_id] = array(
				'id' => $content->id,
				'title' => $content_langs->title,
				'slug' => $content_langs->slug
			);
		}
		return $data;
	}
	
}
