<?php

class Model_Contents_Poster extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'content_id',
		'user_id',
		'price',
		'quantity',
		'discount',
		'discount_from',
		'discount_to',
		'created_at',
		'updated_at',
		'allow_best_offer',
		'offer_accept',
		'offer_decline'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	
	
	protected static $_table_name = 'contents_posters';

	protected static $_belongs_to = array(
		'content' => array(
			'key_from' => 'content_id',
			'model_to' => 'Model_Content',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		)
	);

	/* WEB */
	
	public static function get_by_content_ids($content_ids, $language_id, $params = array(), $count_only = false)
	{
		$posters = array();
		if(empty($content_ids)) return $posters;
	
		/*
		$cache_key = 'posters.';	
		$cache_sub_key = '.'.$language_id.'.'.implode('_', $params);
		
		foreach($content_ids as $k => $poster_content_id)
		{
			if($cache = \Util\Cache::get($cache_key.$poster_content_id.$cache_sub_key))
			{	
				$posters[$poster_content_id] = $cache;
				unset($content_ids[$k]);
			}
		}
	
		if(empty($content_ids)) return $posters;
		*/
		
		$params = \Arr::merge(array(
			'related' => array(
				'content' => array(
					'related' => array(
						'templates',
						'content_langs' => array(
							'where' => array(
								array('language_id', '=', $language_id),
								array('active', '=', 1)
							)
						),
						'fields' => array(
							'where' => array(
								array('language_id', '=', $language_id)
							),
							'related' => array(
								'template_fields' => array(
									'order_by' => array(
										array('order', 'asc')
									)
								),
								'field_files' => array(
									'related' => array(
										'files'
									)								
								)
							)
						),
					)
				)
			),
			'where' => array(
				array('content_id', (is_array($content_ids) ? 'in' : '='), $content_ids)
			),
			'order_by' => array(
				!empty($content_ids) ? array(\DB::expr('field(`t0`.`content_id`, '.implode(',', $content_ids).')'), 'asc') : array()
			)
		), $params);
		
		if($count_only) return self::count($params);

		foreach(self::find('all', $params) as $poster)
		{
			$posters[$poster->content->id] = Model_Content::set_content_base_values($poster->content);
			$posters[$poster->content->id]['poster'] = self::set_poster_base_values($poster);
		//	\Util\Cache::save($cache_key.$poster->content->id.$cache_sub_key, $posters[$poster->content->id]);
		}
		return $posters;
	}		
		
	public static function set_poster_base_values($poster)
	{
		return array(
			'id' => $poster->id,
			'fullname' => $poster->fullname,
			'email' => $poster->email,
			'phone' => $poster->phone,
			'phone_is_public' => $poster->phone_is_public,
			'quantity' => $poster->quantity
		);
	}	
		
		
	/* ADMIN */
	
	public static function get_table_name()
	{
		return self::$_table_name;
	}

	public static function sub_form_fields()
	{
		return array(
			'quantity' => array(
				'type' => 'text_field',
				'default_value' => 1
			),		
			'fullname' => array(
				'type' => 'text_field',
				'required' => true
			),
			'email' => array(
				'type' => 'text_field',
				'required' => true
			),
			'phone' => array(
				'type' => 'text_field',
				'required' => true
			),
			'phone_is_public' => array(
				'type' => 'checkbox'
			)
		);
	}

	public static function get_by_content_id($content_id)
	{
		if($poster = current(self::find('all', array(
			'where' => array(
				array('content_id', '=', $content_id)
			)
		))))
		{
			$values = array();
			foreach(self::sub_form_fields() as $name => $v)
			{
				if(in_array($v['type'], array('date_time_picker', 'date_picker')))
				{
					if(empty($poster->$name))
					{
						$values[$name] = 0;
					}
					else
					{
						$values[$name] = \Date::forge($poster->$name)->format(($v['type'] == 'date_time_picker' ? 'eu_full' : 'eu'));
					}
				}
				else
				{
					$values[$name] = $poster->$name;
				}
			}
			return $values;
		}
		return null;
	}
	
	public static function update_values_by_content_id($content_id, $values, $user_id)
	{
		if($poster = current(self::find('all', array(
			'where' => array(
				array('content_id', '=', $content_id)
			)
		))))
		{
			if(!empty($values['user_id'])) unset($values['user_id']);
			
			foreach($values as $key => $value)
			{
				$poster->$key = $value;
			}
			$poster->save();
			return $poster->id;
		}
		
		$values['content_id'] = $content_id;
		$values['user_id'] = $user_id;
		$poster = new self($values);		
		$poster->save();
		return $poster->id;
	}
	
	public static function remove_by_content_id($content_id)
	{
		foreach(self::find('all', array(
			'where' => array(
				array('content_id', '=', $content_id)
			)
		)) as $poster)
		{
			$poster->delete();
		}		
	}

	
	
	
	
	
}
