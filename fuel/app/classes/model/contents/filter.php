<?php

class Model_Contents_Filter extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'content_id',
		'option_id',
		'option_value_id',
		'priority',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	protected static $_table_name = 'contents_filters';

	

	protected static $_belongs_to = array(
		'option' => array(
			'key_from' => 'option_id',
			'model_to' => 'Model_Option',
			'key_to' => 'id'				
		),
		'option_values' => array(
			'key_from' => 'option_value_id',
			'model_to' => 'Model_Options_Value',
			'key_to' => 'id'			
		),
		'option_values_langs' => array(
			'key_from' => 'option_value_id',
			'model_to' => 'Model_Options_Values_Lang',
			'key_to' => 'option_value_id'			
		),		
	);		
	
	
	
	/* WEB */
	public static function get_option_id_by_content_ids($content_ids)
	{
		$filter_ids = array();
		foreach(self::find('all', array(
			'where' => array(
				array('content_id', 'in', $content_ids)
			)
		)) as $filter_node)
		{
			$filter_ids[] = $filter_node->option_id;
		}
		return $filter_ids;
	}
	
	public static function get_content_ids_by_filter_values($filter_values)
	{
		$content_ids = array();

		foreach(\DB::select('content_id')
			->from(self::$_table_name)
			->where('option_value_id', 'in', $filter_values)
			->group_by('content_id')
			->having(\DB::expr('COUNT(*)'), '>=', count($filter_values))
			->execute()
			->as_array() as $v)
		{
			$content_ids[] = $v['content_id'];
		}
		
		return $content_ids;
	}
	
	
	/* ADMIN */
	public static function delete_filters_by_ids($ids, $content_id)
	{
		$cleared = array();
		foreach($ids as $filter_id)
		{
			if($filter = self::find($filter_id, array(
				'where' => array(
					array('content_id', '=', $content_id)
				)
			)))
			{
				$cleared[] = $filter->id;
				$filter->delete();
			}
		}
	
		if(empty($cleared)) return null;
		return $cleared;
	}
	
	public static function get_value_names($content_id, $language_id = null, $option_ids = null)
	{
		if(!$language_id) $language_id = Model_Language::get_primary(true);
		
		$value_names = array();
		
		$temp = array(
			'value' => '',
			'option_value_id' => 0,
			'sub' => array()
		
		);
		
		function search_sub($current_sub, $filter_value)
		{	
			if(!empty($current_sub[$filter_value->option_values->parent_id]))
			{
				$current_sub[$filter_value->option_values->parent_id]['sub'][$filter_value->option_values->id] = array(
					'id' => $filter_value->id,
					'value' => $filter_value->option_values_langs->value,
					'sub' => array()
				);
			}
			else
			{
				foreach($current_sub as $k => $v)
				{
					if(!empty($current_sub[$k]['sub'])) $current_sub[$k]['sub'] = search_sub($current_sub[$k]['sub'], $filter_value);
				}
			}	
			return $current_sub;
		}
		
		foreach(self::find('all', array(
			'related' => array(
				'option' => array(
					'related' => array(
						'field' => array(
							'related' => array(
								'single_content_field' => array(
									'where' => array(
										array('content_id', '=', $content_id)
									)
								)
							)	
						)
					)
				),
				'option_values',
				'option_values_langs' => array(
					'where' => array(
						array('language_id', '=', $language_id)
					)
				)
			),
			'where' => array(
				array('content_id', '=', $content_id)
			),
			'order_by' => array(
				array('priority', 'asc')
			)
		)) as $filter_value)
		{
			if(empty($value_names[$filter_value->option_values->option_id])) $value_names[$filter_value->option_values->option_id][$filter_value->option_values->id] = array();
			
			if(empty($filter_value->option_values->parent_id))
			{
				if(empty($value_names[$filter_value->option_values->option_id][$filter_value->option_values->id]))
				{
					$value_names[$filter_value->option_values->option_id][$filter_value->option_values->id] = array(
						'id' => $filter_value->id,
						'value' =>   $filter_value->option_values_langs->value,
						'sub' => array()
					);
				}
				continue;
			}

			$value_names[$filter_value->option_values->option_id] = search_sub($value_names[$filter_value->option_values->option_id], $filter_value);

		}

		if(empty($value_names)) return null;	

		return $value_names;
	}
	
	
	public static function update_content_filters($content_id, $_values, $update = true)
	{
		if(empty($_values) && !is_array($_values)) return false;
		
		$__values = array();
		foreach($_values as $option_id => $values)
		{
			foreach($values['value'] as $k => $v)
			{
				if($k == -1 || empty($v)) continue;
				$__values[$option_id][$v] = array(
					'value' => $v,
					'priority' => !empty($values['priority'][$k]) ? $values['priority'][$k] : 0
				);
			}
		}

		$option_ids = array();
		$values = array();

		foreach($__values as $option_id => $option_values)
		{		

			if(empty($option_values)) continue;

			$values_ids = array();
			$option_id_ids = array();
			
			foreach($option_values as $d)
			{
				$option_value_id = $d['value'];
				$priority = !empty($d['priority']) ? $d['priority'] : 0;
				
				$values[$option_value_id] = array(
					'content_id' => $content_id,
					'option_id' => $option_id,
					'option_value_id' => $option_value_id,
					'priority' => $priority
				);
				$option_id_ids[$option_id] = $option_id;
				$values_ids[$option_value_id] = $option_value_id;
			}
		}


		/* Update */
		if($update && !empty($values))
		{
			$existing_values = array();
			foreach(self::find('all', array(
				'where' => array(
					array('content_id', '=', $content_id),
					array('option_id', 'in', $option_id_ids)
				)
			)) as $current_value)
			{
				$existing_values[$current_value->option_value_id] = $current_value->option_value_id;
			}

			foreach($values as $content_filter_data)
			{
				if(!in_array($content_filter_data['option_value_id'], $existing_values))
				{
					$new_filter_value = new self($content_filter_data);
					$new_filter_value->save();						
				}
			}

		}/* Create */
		elseif(!$update && !empty($values))
		{
			foreach($values as $content_filter_data)
			{

				$new_filter_value = new self($content_filter_data);
				$new_filter_value->save();
			}
		}		
	
		return true;
	}
	
	public static function remove_by_content_id($content_id)
	{
		foreach(self::find('all', array(
			'where' => array(
				array('content_id', '=', $content_id)
			)
		)) as $filter_value)
		{
			$filter_value->delete();
		}
	}
	
}
