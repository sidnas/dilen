<?php

class Model_Contents_Fields_Value extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'content_id',
		'template_field_id',
		'value',
		'language_id',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'contents_fields_values';

	protected static $_belongs_to = array(
		'template_fields' => array(
			'key_from' => 'template_field_id',
			'model_to' => 'Model_Templates_Field',
			'key_to' => 'id'			
		),
		'content' => array(
			'key_from' => 'content_id',
			'model_to' => 'Model_Content',
			'key_to' => 'id'			
		)		
	);	
	
	protected static $_has_many = array(
		'field_files' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Contents_Fields_File',
			'key_to' => 'content_field_id'	
		)	
	);
	
	public static function get_by_search_in_value($value_keyword, $parent_page_id = false)
	{
		$data = $where_parent = array();
		
		if($parent_page_id)
		{
			$where_parent = array(
				'where' => array(
					array('parent_content_id', '=', $parent_page_id)
				)			
			);		
		}
		
		foreach(self::find('all', array(
			'related' => array(
				'template_fields' => array(
					'where' => array(
						array('searchable', '=', 1)
					)
				),
				'content' => array(
					'related' => array(
						'content_langs',
						'nodes' => $where_parent
					)
				)
			),
			'where' => array(
				array('value', 'like', '%'.$value_keyword.'%')
			)
		)) as $value)
		{	

			if(empty($value->content)) continue;
			$content_langs = !empty($value->content->content_langs) ? current($value->content->content_langs) : null;
			if(!$content_langs) continue;		
			$data[$value->content->id] = array(
				'id' => $value->content->id,
				'content_value_id' => $value->id,
				'title' => $content_langs->title,
				'slug' => $content_langs->slug,
				'found_at' => strip_tags(trim(preg_replace('/\s+/', ' ', $value->value)))
			);			
			
		}
		return $data;
	}
	
	public static function get_by_unneeded_value_ids($value_ids, $content_ids, $parent_page_id = false)
	{
		$data = $where = $content_where = $where_parent = array();
		
		if(!empty($value_ids))
		{
			$where[] = array('id', 'not in', $value_ids);
		}
		
		if(!empty($content_ids))
		{
			$content_where[] = array('id', 'not in', $content_ids);
		}
		
		if($parent_page_id)
		{
			$where_parent = array(
				'where' => array(
					array('parent_content_id', '=', $parent_page_id)
				)			
			);		
		}
		
		foreach(self::find('all', array(
			'related' => array(
				'template_fields' => array(
					'where' => array(
						array('searchable', '=', 1)
					)
				),
				'content' => array(
					'where' => $content_where,
					'related' => array(
						'content_langs',
						'nodes' => $where_parent
					)
				)
			),
			'where' => \Arr::merge($where, array(
				array('value', '!=', '')
				))
			)
		) as $value)
		{	

			if(empty($value->content)) continue;
			$content_langs = !empty($value->content->content_langs) ? current($value->content->content_langs) : null;
			if(!$content_langs) continue;		
			$data[$value->id] = array(
				'id' => $value->content->id,
				'content_value_id' => $value->id,
				'title' => $content_langs->title,
				'slug' => $content_langs->slug,
				'value' => strip_tags(trim(preg_replace('/\s+/', ' ', $value->value)))
			);			
			
		}
		return $data;
	}	
	
	
}
