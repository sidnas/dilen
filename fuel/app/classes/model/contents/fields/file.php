<?php

class Model_Contents_Fields_File extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'file_id',
		'content_field_id',
		'order',
		'primary',
		'custom_value',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'contents_fields_files';

	protected static $_belongs_to = array(
		'field_value' => array(
			'key_from' => 'content_field_id',
			'model_to' => 'Model_Contents_Fields_Value',
			'key_to' => 'id'	
		),
		'files' => array(
			'key_from' => 'file_id',
			'model_to' => 'Model_File',
			'key_to' => 'id'
		)
	);	
	
	
}
