<?php

class Model_Contents_Tree_Node extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'content_id',
		'parent_content_id',
		'position',
		'order',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'contents_tree_nodes';
	
	protected static $_belongs_to = array(
		'positions' => array(
			'key_from' => 'position',
			'model_to' => 'Model_Contents_Position',
			'key_to' => 'id'				
		)
	);
	
	protected static $_has_many = array(
		'contents' => array(
			'key_from' => 'content_id',
			'model_to' => 'Model_Content',
			'key_to' => 'id'			
		)
	);
	
	public static function get_child_tree_data($content_id, $language_id, $params = array(), $childs = null)
	{
		$tree = array();

		foreach(self::find('all', \Arr::merge(array(
			'related' => array(
				'contents' => array(
					'related' => array(
						'templates',					
						'content_langs' => array(
							'where' => array(
								array('language_id', '=', $language_id),
								array('active', '=', 1)
							)
						),
						'fields' => array(
							'where' => array(
								array('language_id', '=', $language_id)
							),
							'related' => array(
								'template_fields',
								'field_files' => array(
									'related' => array(
										'files'
									)
								),						
							)
						)					
					)
				)
			),
			'order_by' => array(
				array('order', 'asc')
			)), $params)
		) as $node)
		{

			if(empty($node->contents)) continue;
			$content = current($node->contents);

			if($tmp_value = Model_Content::set_content_base_values($content))
			{
				$tree[$content->id] = $tmp_value;
				if(!empty($params['content_posters']))
				{
					$tree[$content->id]['poster'] = Model_Contents_Poster::set_poster_base_values($content->poster);
				}
				$tree[$content->id]['sub'] = ($childs ? self::get_child_tree_data($node->content_id, $language_id, $params) : array());				
			}
		}

		return $tree;

	}
	
	
	public static function get_childs($content_id, $group_by = false)
	{
		$child_ids = array();
		$group = array();
		if($group)
		{
			$group = array(
				'group_by' => array(
					'parent_content_id'
				)
			);
		}
		foreach(self::find('all', array(
			'where' => array(
				array('parent_content_id', '=', $content_id)
			)
		)) as $node)
		{
			$child_ids[] = $node->content_id;
		}
		return $child_ids;
	}
	
	
	
}
