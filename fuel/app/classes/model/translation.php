<?php

class Model_Translation extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'field_type',
		'input_name',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'translations';
	
	protected static $_has_many = array(
		'translation_lang' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Translations_Lang',
			'key_to' => 'translation_id'
		),
		'translation_template' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Translations_Template',
			'key_to' => 'translation_id'
		)		
	);
	
	public static function get_all($language_id)
	{
		$data = array();
		$inputs = self::find('all', array(
			'related' => array(
				'translation_template'
			)
		));
		
		$ids = array();
		foreach($inputs  as $v) $ids[] = $v->id;
		if(empty($ids)) return array();
		
		
		$translations = Model_Translations_Lang::find('all', array(
			'where' => array(
				array('translation_id', 'in', $ids),
				array('language_id', '=', $language_id)
			)			
		));
		
		$lang = array();
		foreach($translations as $v) $lang[$v->translation_id] = !empty($v->value) ? $v->value : null;

		foreach($inputs as $v)
		{
			$data[$v->input_name] = array(
				'id' => $v->id,
				'value' => !empty($lang[$v->id]) ? $lang[$v->id] : $v->input_name,
				'field_type' => $v->field_type,
				'template_ids' => array()
			);
			
			if(!empty($v->translation_template))
			{
				foreach($v->translation_template as $v2) $data[$v->input_name]['template_ids'][] = $v2->template_id;
			}
		}

		return $data;
	}
	
	public static function get_by_template_id($template_id, $language_id)
	{
		$data = array();
		$inputs = self::find('all', array(
			'related' => array(
				'translation_template' => array(
					'where' => array(
						array('template_id', '=', $template_id)
					)
				)
			)		
		));
		$ids = array();
		foreach($inputs  as $v) $ids[] = $v->id;
		if(empty($ids)) return array();
		
		
		$translations = Model_Translations_Lang::find('all', array(
			'where' => array(
				array('translation_id', 'in', $ids),
				array('language_id', '=', $language_id)
			)			
		));
		
		$lang = array();
		foreach($translations as $v) $lang[$v->translation_id] = !empty($v->value) ? $v->value : null;

		foreach($inputs as $v) $data[$v->input_name] = array(
			'value' => !empty($lang[$v->id]) ? $lang[$v->id] : $v->input_name,
			'field_type' => $v->field_type
		);

		return $data;
		
	}
	
}
