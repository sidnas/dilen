<?php

class Model_Language extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'title',
		'val',
		'icon',
		'primary',
		'active',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	protected static $_table_name = 'languages';

	public static function get_primary($id_only = false)
	{
		$language = current(
			self::find('all', array(
				'where' => array(
					array('primary', '=', 1)
				)
			))
		);
		
		if(!empty($language->id))
		{
			if($id_only) return $language->id;
			return $language;
		}
		
		throw new Exception('Primary language not found!');
	}
	
	public static function get_all($active_only = true)
	{
		$where = array();
		if($active_only)
		{
			$where = array(
				'where' => array(
					array('active', '=', 1)
				)
			);
		}
		
		$languages = array();
		foreach(Model_Language::find('all', \Arr::merge($where, array(
			$where,
			'order_by' => array(
				array('primary', 'desc')
			)
		))) as $v)
		{
			$languages[] = array(
				'id' => $v->id,
				'title' => $v->title
			);
		}	
		return $languages;
	}
}
