<?php
namespace Model;
class Routing extends \Orm\Model
{
	/* Namespace */
	private static $prefix = 'web/';
	
	private static $language_id;
	
	public static $main_translation = null;

	private static function get_predefined($language_id, $uri_1, $uri_2, $uri_3)
	{
		if($uri_1 == 'ua')
		{
			$main = self::home_page();
		
			$main['controller'] = self::$prefix.$uri_1.'/'.$uri_2.'/'.$uri_3;

			return $main;
			
		}
		return null;
	
	}
	
	public static function target($language_id, $uri_1 = null, $uri_2 = null, $uri_3 = null)
	{
		
		if($predefined = self::get_predefined($language_id, $uri_1, $uri_2, $uri_3))
		{
			return $predefined;		
		}

		self::$language_id = $language_id;
		
		if($uri_1)
		{
			if($controller = self::page($uri_1))
			{
				return $controller;
			}
		}

		return self::home_page();
	}

	public static function home_page()
	{
		$first_page = current(\Model_Content::find('all', array(
			'related' => array(
				'templates' => array(
					'where' => array(
						array('type', '=', 'home')
					)
				),
				'nodes' => array(
					'where' => array(
						array('parent_content_id', '=', 0)
					),
					'order_by' => array(
						array('order', 'asc')
					)
				)
			),
			'limit' => 1
		)));
		
		if(empty($first_page->id)) exit('Page Content Not Found');
		
		$inst = new \Util\Translation;

		return array(
			'id' => $first_page->id,
			'template_id' => $first_page->template_id,
			'controller' => self::$prefix.$first_page->templates->controller,
			'main_translation' => $inst->init_($first_page->template_id, self::$language_id)
		);
	}
	
	public static function page($slug)
	{
		if($page = current(\Model_Content::find('all', array(
			'related' => array(
				'templates',		
				'content_langs' => array(
					'where' => array(
						array('slug', '=', $slug),
						array('language_id', '=', self::$language_id)
					)
				)
			)
		))))
		{
		
			$inst = new \Util\Translation;

			return array(
				'id' => $page->id,
				'template_id' => $page->template_id,
				'controller' => self::$prefix.$page->templates->controller,
				'main_translation' => $inst->init_($page->template_id, self::$language_id)
			);

		}
		return null;
	}
	
}
