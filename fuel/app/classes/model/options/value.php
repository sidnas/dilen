<?php

class Model_Options_Value extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'option_id',
		'order',
		'equal_lang', /* Translation equal in all languages */
		'parent_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
	    'Util\Observers\Order\Input\Value\Option' => array(
            'events' => array('before_insert')
        ),			
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'options_values';
	
	protected static $_has_many = array(
		'option_value_langs' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Options_Values_Lang',
			'key_to' => 'option_value_id',
			 'cascade_save' => true,
			 'cascade_delete' => true		
		)
	);
	
	public static function get_table_name()
	{
		return self::$_table_name;
	}
	
	private static $child_count = 0;
	
	public static function child_count($filter_id, $parent_id = 0)
	{

		$r = \DB::select('id')
			->from(self::$_table_name);

		$r->where('parent_id', '=', $parent_id);
		$r->where('option_id', '=', $filter_id);
		
		$r = $r->group_by('parent_id')
			->limit(1)
			->execute()->as_array();
			
		$r = current($r);

		if(!empty($r['id']))
		{	
			self::$child_count++;
			return self::child_count($filter_id, $r['id']);
		}
		$counter = self::$child_count;
		self::$child_count = 0;
		return $counter;
	}

	public static function get_values($option_id, $language_id = null, $parent_value_id = 0, $merge_with_primary = false, $get_childs = true, $select_list = false)
	{
		$cache_key = 'options.values.'.$option_id;
		$cache_key .= '.'.$parent_value_id.'.'.(int)$merge_with_primary.'.'.(int)$get_childs.'.'.(int)$select_list;

		$cache = \Util\Cache::get($cache_key, true);
		if(!empty($cache)) return $cache;

		if(!$language_id) $language_id = Model_Language::get_primary(true);
		
		$primary_language_id = null;
		
		/* Check if base language_id added */
		if($merge_with_primary && $language_id) $primary_language_id = Model_Language::get_primary(true);
		
		/* Check if primary language not equal with base language*/
		if($primary_language_id && $primary_language_id == $language_id) $primary_language_id = null;
		

		$language_where = array(
			'where' => array(
				array('language_id', '=', $language_id)
			)
		);

		if($primary_language_id)
		{
			/* Get needed language + primary */
			$language_where = array(
				'where' => array(
					array('language_id', 'in', array($language_id, $primary_language_id))
				)
			);		
		}

		$where = array(
			'where' => array(
				array('option_id', '=', $option_id),
				array('parent_id', '=', $parent_value_id)
			),
			'related' => array(
				'option_value_langs' => $language_where
			),
			'order_by' => array(
				array('order', 'asc')
			)
		);
		
		$_values = self::find('all', $where);
		$values = array();
		foreach($_values as $value)
		{
			$count = 0;
			if($get_childs == 'count')
			{
				$r = current(\DB::select(\DB::expr('COUNT(*) as c'))
					->from(self::$_table_name)
					->where('parent_id', '=', $value->id)
					->group_by('parent_id')
					->execute()
					->as_array());
					
				$count = (!empty($r['c']) ? $r['c'] : 0);
				
			}
			
			$values[$value->id] = array(
				'id' => $value->id,
				'order' => $value->order,
				'equal_lang' => (int)$value->equal_lang,
				'parent_id' => $value->parent_id,
				'created_at' => $value->created_at,
				'updated_at' => $value->updated_at,
				'option_value_langs' => array(),
				'sub' => ($get_childs) ? ($get_childs == 'count' ? $count : self::get_values($option_id, $language_id, $value->id, $merge_with_primary, false)) : array()
			);
		
			if($primary_language_id)
			{
				$show_base = false;
				$langs = array();
				/* Group by base and primary languages */
				foreach($value->option_value_langs as $v) $langs[$v->language_id] = $v;			
				/* If base language exists, get base languge if not get primary language */			
				if(!empty($langs[$language_id]))
				{
					$lang = $langs[$language_id];
					$show_base = true;
				}
				else
				{
					/* Primary always exists */
					$lang = $langs[$primary_language_id];
				}			
				
				$values[$value->id]['option_value_langs'] = array(
					'id' => $lang->id,
					'option_value_id' => $lang->option_value_id,
					'value' => ($show_base || $value->equal_lang ? $lang->value : ''),
					'slug' => ($show_base || $value->equal_lang ? $lang->slug : ''),
					'value_placeholder' => $lang->value,
					'slug_placeholder' => $lang->slug,					
					'language_id' => $lang->language_id,
					'created_at' => $lang->created_at,
					'updated_at' => $lang->updated_at	
				);				
			}
			else
			{
				/* get base language*/
				$lang = current($value->option_value_langs);	
				$values[$value->id]['option_value_langs'] = array(
					'id' => $lang->id,
					'option_value_id' => $lang->option_value_id,
					'value' => $lang->value,
					'slug' => $lang->slug,
					'value_placeholder' => $lang->value,
					'slug_placeholder' => $lang->slug,					
					'language_id' => $lang->language_id,
					'created_at' => $lang->created_at,
					'updated_at' => $lang->updated_at				
				);
			}

		}

		if($select_list)
		{
			$list = array();
			foreach($values as $v)
			{
				$list[$v['id']] = $v['option_value_langs']['value_placeholder'];
				
			}
			\Util\Cache::save($cache_key, $list);
			return $list;
		}

		\Util\Cache::save($cache_key, $values);
		return $values;
	}	
	
	
}
