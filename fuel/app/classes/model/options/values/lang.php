<?php

class Model_Options_Values_Lang extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'option_value_id',
		'value',
		'slug',
		'language_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
	    'Util\Observers\Slug\Optionv' => array(
            'events' => array('before_insert', 'before_update')
        ),	
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'options_values_langs';
	
}
