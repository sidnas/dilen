<?php

class Model_Translations_Lang extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'translation_id',
		'language_id',
		'value',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'translations_langs';


	protected static $_belongs_to = array(
		'translation' => array(
			'key_from' => 'translation_id',
			'model_to' => 'Model_Translation',
			'key_to' => 'id'			
		)
	);	
	
	
	
}
