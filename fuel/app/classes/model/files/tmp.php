<?php

class Model_Files_Tmp extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'path',
		'name',
		'orginal_name',
		'size',
		'custom_value',
		'created_at',
		'updated_at',
		'group_id',
		'session_hash'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'files_tmp';

	public static function set_data_array($file)
	{
		$n_data = array();
		foreach(self::$_properties as $v)
		{
			$n_data[$v] = !empty($file->$v) ? $file->$v : '';
		
		}
		return $n_data;
	}
	
}
