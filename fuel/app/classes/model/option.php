<?php

class Model_Option extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'input_name',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
	    'Util\Observers\Input\Name\Option' => array(
            'events' => array('before_insert', 'before_update')
        ),		
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	protected static $_table_name = 'options';

	protected static $_belongs_to = array(
		'field' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Templates_Field',
			'key_to' => 'filter_id'			
		),	
	);
	
	protected static $_has_many = array(
		'option_langs' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Options_Lang',
			'key_to' => 'option_id'			
		),
		'option_values' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Options_Value',
			'key_to' => 'option_id'			
		),		
	);
	
	
	
	public static function get_all($language_id = null)
	{
		if(!$language_id) $language_id = Model_Language::get_primary(true);

		return self::find('all', array(
			'where' => array(
				array('input_name', '=', 'car_brand')
			),
			'related' => array(
				'option_langs' => array(
					'where' => array(
						array('language_id', '=', $language_id)
					)
				),
				'option_values' => array(
					'related' => array(
						'option_value_langs',
						'option_value_links' => array(
							'related' => array(
								'option_value' => array(
									'related' => array(
										'option_value_langs'
									)
								)
							)
						)
					)
				)
			)
		));
	}
	
	public static function get_options($language_id = null, $merge_with_primary = false, $options_ids = null)
	{

		if(!$language_id) $language_id = Model_Language::get_primary(true);	
		
		$cache_key = 'options.';
		$cache_key_sub = '.'.(int)$language_id.'.'.(int)$merge_with_primary;
		
		$options = array();
		
		if(!empty($options_ids))
		{
			$cache_key = 'options.';
			foreach($options_ids as $k => $option_id)
			{
				if($cache = \Util\Cache::get($cache_key.'.'.$option_id.$cache_key_sub))
				{
					$options[$option_id] = $cache;
					unset($options_ids[$k]);
				}
			}

			if(empty($options_ids)) return $options;
		}
		else
		{
			$cache_key .= 'all.';
		}		

		
		
		$primary_language_id = null;
		
		/* Check if base language_id added */
		if($merge_with_primary && $language_id) $primary_language_id = Model_Language::get_primary(true);
		
		/* Check if primary language not equal with base language*/
		if($primary_language_id && $primary_language_id == $language_id) $primary_language_id = null;	

		$language_where = array(
			'where' => array(
				array('language_id', '=', $language_id)
			)
		);

		if($primary_language_id)
		{
			/* Get needed language + primary */
			$language_where = array(
				'where' => array(
					array('language_id', 'in', array($language_id, $primary_language_id))
				)
			);		
		}

		$where = array(
			'related' => array(
				'field' => array(
					
				),
				'option_langs' => $language_where
			)
		);
		
		
		
		if($options_ids)
		{
			$where['where'] = array(
				array('id', 'in', $options_ids)
			);
		}

		$_options_list = self::find('all', $where);
		foreach($_options_list as $k => $_options)
		{
			if(empty($_options->option_langs)) continue;

			
			$custom_value = !empty($_options->field->custom_value) ? \Format::forge($_options->field->custom_value, 'json')->to_array() : null;

			$options[$k] = array(
				'id' => $_options->id,
				'input_name' => $_options->input_name,
				'created_at' => $_options->created_at,
				'updated_at' => $_options->updated_at,
				'is_multi' => !empty($custom_value['multi_select']) ? $custom_value['multi_select'] : null,
				'option_langs' => array()
			);
			
			if($primary_language_id)
			{
				$show_base = false;
				$langs = array();
				/* Group by base and primary languages */
				foreach($_options->option_langs as $v) $langs[$v->language_id] = $v;

				/* If base language exists, get base languge if not get primary language */			
				if(!empty($langs[$language_id]))
				{
					$lang = $langs[$language_id];
					$show_base = true;
				}
				else
				{
					/* Primary always exists */
					$lang = $langs[$primary_language_id];
				}

				$options[$k]['option_langs'] = array(
					'id' => $lang->id,
					'slug' => ($show_base ? $lang->slug : ''),
					'name' => ($show_base ? $lang->name : ''),
					'slug_placeholder' => $lang->slug,
					'name_placeholder' => $lang->name,				
					'created_at' => $lang->created_at,
					'updated_at' => $lang->updated_at
				);
			}
			else
			{	
				/* get base language*/
				$lang = current($_options->option_langs);	
				$options[$k]['option_langs'] = array(
					'id' => $lang->id,
					'slug' => $lang->slug,
					'name' => $lang->name,
					'slug_placeholder' => $lang->slug,
					'name_placeholder' => $lang->name,
					'created_at' => $lang->created_at,
					'updated_at' => $lang->updated_at
				);		
			}	
			//\Util\Cache::save($cache_key.'.'.$k.$cache_key_sub, $options[$k]);
		}

		return  $options;
	}
	
	public static function get_option($filter_key, $language_id = null, $merge_with_primary = false)
	{
		if(!$language_id) $language_id = Model_Language::get_primary(true);
		
		$primary_language_id = null;
		
		/* Check if base language_id added */
		if($merge_with_primary && $language_id) $primary_language_id = Model_Language::get_primary(true);
		
		/* Check if primary language not equal with base language*/
		if($primary_language_id && $primary_language_id == $language_id) $primary_language_id = null;

		
		$fetch = (is_int($filter_key) ? $filter_key : 'all');

		$language_where = array(
			'where' => array(
				array('language_id', '=', $language_id)
			)
		);

		if($primary_language_id)
		{
			/* Get needed language + primary */
			$language_where = array(
				'where' => array(
					array('language_id', 'in', array($language_id, $primary_language_id))
				)
			);		
		}
		
		$by_key = array();
		
		if($fetch == 'all')
		{
			$by_key = array(
				'where' => array(
					array('input_name', '=', $filter_key) 
				)
			);
		}
		else
		{
			$language_where['where'][] = array('option_id', '=', $filter_key);
		}

		$where = \Arr::merge($by_key, array(
			'related' => array(
				'option_langs' => $language_where
		)));

		/* Select option from database */
		$_options = self::find($fetch, $where);
		$_options = ($fetch == 'all' ? current($_options) : $_options);

		if(empty($_options->option_langs)) return null;
		
		$options = array(
			'id' => $_options->id,
			'input_name' => $_options->input_name,
			'created_at' => $_options->created_at,
			'updated_at' => $_options->updated_at,
			'option_langs' => array()
		);
		
		if($primary_language_id)
		{
			$show_base = false;
			$langs = array();
			/* Group by base and primary languages */
			foreach($_options->option_langs as $v) $langs[$v->language_id] = $v;

			/* If base language exists, get base languge if not get primary language */			
			if(!empty($langs[$language_id]))
			{
				$lang = $langs[$language_id];
				$show_base = true;
			}
			else
			{
				/* Primary always exists */
				$lang = $langs[$primary_language_id];
			}

			$options['option_langs'] = array(
				'id' => $lang->id,
				'slug' => ($show_base ? $lang->slug : ''),
				'name' => ($show_base ? $lang->name : ''),
				'slug_placeholder' => $lang->slug,
				'name_placeholder' => $lang->name,				
				'created_at' => $lang->created_at,
				'updated_at' => $lang->updated_at
			);
		}
		else
		{	
			/* get base language*/
			$lang = current($_options->option_langs);	
			$options['option_langs'] = array(
				'id' => $lang->id,
				'slug' => $lang->slug,
				'name' => $lang->name,
				'slug_placeholder' => $lang->slug,
				'name_placeholder' => $lang->name,
				'created_at' => $lang->created_at,
				'updated_at' => $lang->updated_at
			);		
		}

		return $options;
	}

	
}
