	<div class="row">
	  <div class="col-lg-12 col-md-12">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  <?=\Lang::get('admin.options');?>
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
		 <?=\Form::open(array(
			'class' => 'form-horizontal row-border'
		 ));?>	
			<table class="table table-responsive table-striped table-bordered table-hover no-margin" id="value-table">
				<thead>
					<tr>
						<th><?=\Lang::get('admin.name');?></th>
						<th><?=\Lang::get('admin.key');?></th>
						<th><?=\Lang::get('admin.slug');?></th>
						<th style="width:10%" class="t-center"><?=\Lang::get('admin.edit');?></th>
					</tr>
				</thead>		
				<tbody>
					<?php foreach($options as $option): ?>
						<tr>
							<td><?=$option['option_langs']['name'];?></td>
							<td><?=$option['input_name'];?></td>
							<td><?=$option['option_langs']['slug'];?></td>
							<td class="t-center">
								<a href="<?=\Uri::create('admin/option/edit/'.$option['id']);?>" class="fa fa-pencil-square-o"></a>
							</td>
						</tr>					
					<?php endforeach; ?>
				</tbody>
			</table>	
			<?=\Form::close();?>
		  </div>
		</div>
	  </div>
	</div>
