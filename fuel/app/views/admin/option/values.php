<tr data-node-id="<?=$value['id'];?>" id="content-<?=$value['id'];?>" class="input-holder">
		<td class="t-center">
			<div class="fa fa-arrows-alt movable-content"></div>
		</td>
		<td>
			<?=\Form::input('values['.$value['id'].'][value]', !empty($value['option_value_langs']['value']) ? $value['option_value_langs']['value'] : \Input::post('values.'.$value['id'].'.value'), array(
				'class' => 'form-control',
				'placeholder' => !empty($value['option_value_langs']) ? $value['option_value_langs']['value_placeholder'] : ''
			));?>	
		</td>
		<td>
			<?=\Form::input('values['.$value['id'].'][slug]', !empty($value['option_value_langs']['slug']) ? $value['option_value_langs']['slug'] : \Input::post('values.'.$value['id'].'.slug'), array(
				'class' => 'form-control',
				'placeholder' => !empty($value['option_value_langs']) ? $value['option_value_langs']['slug_placeholder'] : ''
			));?>		
		</td>

		<td class="t-center">
			<?=\Form::checkbox('values['.$value['id'].'][equal_lang]', 1, $value['equal_lang']);?>		
		</td>
		<td class="t-center open_parent">
			<a href="<?=\Uri::create('admin/option/edit/'.\Uri::segment(4).'/'.$value['id']);?>#value-table">(<?=!empty($value['sub']) ? $value['sub'] : 0;?>)</a>
		</td>
		<td>				 
			<div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
					<?=\Lang::get('admin.action');?> 
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="javascript:;" class="delete-content customAction" data-action="deleteValue" data-id="<?=$value['id'];?>" data-quest="<?=\Lang::get('admin.are_you_sure');?>"><?=\Lang::get('admin.delete');?></a>
					</li>
				</ul>
			</div>				  
			<?=\Form::hidden('values['.$value['id'].'][id]', $value['id'], array(
				'disabled' => true
			));?>				
		</td>
	</tr>
								
