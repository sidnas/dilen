	<?=\Form::open(array(
		'class' => 'form-horizontal row-border',
		'action' => $form_action,
		'id' => 'content-form'
	));?>
			<?php if(\Uri::segment(3) != 'create'): ?>
				<div class="form-group">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.choose_content_language');?>:</label>
					<div class="col-md-10">
						<div class="btn-group">
						<?php foreach($languages as $v): ?>
							  <a href="?language_id=<?=$v['id'];?>" class="btn <?=($v['id'] == $language_id) ? 'btn-info' : 'btn-default';?>"><?=$v['title'];?></a>
						  <?php endforeach; ?>
						</div>			
					</div>
				</div>
			<?php endif; ?>
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.input_name');?>:<?=(\Uri::segment(3) == 'create' ? '<span class="required">*</span>': '');?> </label>
				<div class="col-md-3">
					<?=\Form::input('input_name', !empty($option['input_name']) ? $option['input_name'] : \Input::post('input_name'), array(
						'class' => 'form-control',
						'readonly' => (\Uri::segment(3) == 'create' ? false : true)
					));?>
				</div>						
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.title');?>:<span class="required">*</span> </label>
				<div class="col-md-3">
					<?=\Form::input('name', !empty($option['option_langs']['name']) ? $option['option_langs']['name'] : \Input::post('name'), array(
						'class' => 'form-control',
						'placeholder' => !empty($option['option_langs']) ? $option['option_langs']['name_placeholder'] : ''
					));?>
				</div>						
			</div>				
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.slug');?>:<span class="required">*</span> </label>
				<div class="col-md-3">
					<?=\Form::input('slug', !empty($option['option_langs']['slug']) ? $option['option_langs']['slug'] : \Input::post('slug'), array(
						'class' => 'form-control',
						'placeholder' => !empty($option['option_langs']) ? $option['option_langs']['slug_placeholder'] : ''
					));?>
				</div>						
			</div>			
			
				<div class="form-group">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.values');?></label>
					<div class="col-md-10">				
						<table class="table table-responsive table-striped table-bordered table-hover no-margin" id="value-table">
							<thead>
								<?php if($parent_value_id !== false): ?>
									<tr>
										<th colspan="6"><a href="<?=\Uri::create('admin/option/edit/'.\Uri::segment(4).'/'.$parent_value_id.'?language_id='.\Input::get('language_id'));?>#value-table"><?=\Lang::get('admin.back');?></a></th>
									</tr>
								<?php endif; ?>
								<tr>
									<th style="width:10%" class="t-center"><?=\Lang::get('admin.order');?></th>
									<th style="width:30%"><?=\Lang::get('admin.value');?></th>
									<th style="width:30%"><?=\Lang::get('admin.slug');?></th>

									<th style="width:10%"><?=\Lang::get('admin.equal_in_langs');?></th>
									<th style="width:10%" class="hidden-xs t-center"><?=\Lang::get('admin.sub_values');?></th>
									<th style="width:10%" class="hidden-xs">Actions</th>
								</tr>
							</thead>
							<tbody id="content-data">		
								<?php if(!empty($option_values)): ?>
									<?php foreach($option_values as $v): ?>	
										<?=\View::forge('admin/option/values', array(
											'value' => $v,
											'values' => (\Uri::segment(5) ? $option_values : null)
										), false);?>								
									<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
							<tbody>
								<tr>
									<td colspan="7">
										<a href="javascript:;" class="customAction" data-parent_id="<?=(int)\Uri::segment(5);?>" data-action="createValueFields"><span class="fa fa-plus-circle"></span> <?=\Lang::get('admin.add_more');?></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>	
			
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-5">
					<?=\Form::submit('submit', \Lang::get('admin.save'), array(
						'class' => 'btn btn-success'
					));?>
				</div>
			</div>				
	<?=\Form::close();?>
	<script type="text/x-jqote-template" id="value-field">
		
		<?=str_replace(array(
			'__id__',
			'__parent_id__'
		), array(
			'<%= this.id %>',
			'<%= this.parent_id %>'
		), \View::forge('admin/option/values', array(
			'value' => array(
				'id' => '__id__',
				'parent_id' => '__parent_id__',
				'equal_lang' => 0				
			),
			'values' => null
		), false));?>		
		
		
	</script>