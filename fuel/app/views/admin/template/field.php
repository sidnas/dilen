<?php if(!empty($fields)): ?>
	<?php foreach($fields as $field): ?>
			<?php $custom_value = !empty($custom_values[$field->id]) ? $custom_values[$field->id] : array(); ?>			

			<?php if($field['type'] == 'filter'): ?>
				<?php
					$multi_select = !empty($custom_value['multi_select']) ? $custom_value['multi_select'] : false;
				?>
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8 filter-drops">
						<?php if(!$multi_select || $multi_select == 'select'): ?>			
							<div class="filters">
								<?=\Form::hidden('field['.$field->key_name.']',' ');?>							
								<?=\Form::select('filter['.$field->filter_id.'][value]['.($multi_select ? '-1' : '').']', !empty($values[$field->key_name]) ? $values[$field->key_name] : '', \Arr::merge(array(0 => '---'), \Model_Options_Value::get_values($field->filter_id, null, 0, true, false, true)), array(
									'class' => 'form-control'.(!empty($custom_value['multi_select']) ? ' multiple' : ''),
									'onChange' => 'sys.loadParentValues(this)',
									'data-filter_id' => $field->filter_id,
									'style' => 'width:20%;float:left;'.((!empty($filter_values[$field->filter_id]) && !$multi_select) ? 'display:none' : ''),
									'size' => !empty($custom_value['multi_select']) ? 8 : 0,
								));?>
								<?php if(!$multi_select): ?>
									<?php for($i = 1; $i < 10; $i++): ?>
										<?=\Form::hidden('filter['.$field->filter_id.'][priority][]', $i);?>
									<?php endfor; ?>
								<?php endif; ?>
								
							</div>
							
								<div id="filter-<?=$field->filter_id;?>" class="col-md-12"></div>			
								<div class="value-tree-holder base-tree-node <?=$multi_select;?>">
									<?=\Util\Option::get_inputs_by(!empty($filter_values[$field->filter_id]) ? $filter_values[$field->filter_id] : null); ?>	
								</div>
						<?php endif; ?>
					</div>						
				</div>	
			<?php elseif($field['type'] == 'color_picker'): ?>
			
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-2">
						<?=\Form::input('field['.$field->key_name.']', !empty($values[$field->key_name]) ? $values[$field->key_name] : '#fff', array(
							'class' => 'form-control color-picker'
						));?>
					</div>						
				</div>	
				
			<?php elseif($field['type'] == 'date_picker' || $field['type'] == 'date_time_picker'): ?>
				<?php
					$date = !empty($values[$field->key_name]) ? $values[$field->key_name] : null;
					$date_format = array('class' => 'date-picker', 'format' => 'd.m.Y');
					if($field['type'] == 'date_time_picker')
					{
						$date_format = array('class' => 'date-time-picker', 'format' => 'd.m.Y H:i:s');	
					}
			
					if(!is_numeric($date))
					{
						try
						{
							$date = current(\Date::create_from_string($date, 'eu'));
						}
						catch(Exception $e)
						{
							$date = time();
						}
					}

					if($date <= 0) $date = time();

				?>
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::input('field['.$field->key_name.']', date($date_format['format'], $date), array(
							'class' => 'form-control '.$date_format['class']
						));?>
					</div>						
				</div>	

			<?php elseif($field['type'] == 'text_field'): ?>
			
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::input('field['.$field->key_name.']', !empty($values[$field->key_name]) ? $values[$field->key_name] : '', array(
							'class' => 'form-control'
						));?>
					</div>						
				</div>	

			
			<?php elseif($field['type'] == 'checkbox'): ?>
			
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8 checkbox-holder">
						<?=\Form::checkbox('field_['.$field->key_name.']', 1, !empty($values[$field->key_name]) ? true : false, array(
							'onChange' => 'sys.updateCheckboxRelative(this);'
						));?>
						<?=\Form::hidden('field['.$field->key_name.']', !empty($values[$field->key_name]) ? 1 : 0);?>
					</div>						
				</div>	

			<?php elseif($field['type'] == 'text'): ?>
	
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::textarea('field['.$field->key_name.']', !empty($values[$field->key_name]) ? $values[$field->key_name] : '', array(
							'class' => 'form-control'
						));?>
					</div>						
				</div>	
			
			<?php elseif($field['type'] == 'text_editor'): ?>
				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">			
						<?=\Form::textarea('field['.$field->key_name.']', !empty($values[$field->key_name]) ? $values[$field->key_name] : '', array(
							'class' => 'ckeditor form-control'
						));?>
					</div>
				</div>
			
			
			<?php elseif(in_array($field['type'], array('files', 'images'))): ?>	

				<div class="form-group">
					<label class="col-md-2 control-label"><?=$field->name;?>:<?=!empty($field->required) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?php
							$key = $field->key_name;
						?>
						<?=html_tag('div',  array(
							'class' => 'form-control uploadify',
							'data-template_field_id' => $field->id,
							'data-template_content_id' => (\Uri::segment(3) == 'edit' && \Uri::segment(4) ? \Uri::segment(4) : -1),
							'data-template_key_name' => $field->key_name,
							'id' => 'upload-'.$field->key_name,
							'style' => 'width:150px;float:left;'
						), '&nbsp;');?>
						<table class="table table-responsive table-striped table-bordered table-hover no-margin">
							<thead>
								<tr>
									<td><?=\Lang::get('admin.file_name');?></td>
									<td><?=\Lang::get('admin.created_at');?>
									<td></td>
								</tr>
							</thead>
							<tbody id="files-<?=$field->key_name;?>">
								<?php if(!empty($files[$field->key_name])): ?>
									<?php foreach($files[$field->key_name] as $content_field_file_id => $v): ?>
										<tr id="file-<?=$v['data']['file_id'];?>">
											<td>
												<a class="fancy" href="<?=\uri::base(false).$v[0];?>"><?=$v['data']['name'];?></a>
											</td>
											<td><?=date('d.m.Y H:i:s', $v['data']['created_at']);?></td>
											<td>
												<div class="btn-group">
												  <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
													<?=\Lang::get('admin.action');?> 
													<span class="caret"></span>
												  </button>
												  <ul class="dropdown-menu pull-right">
													<li><a href="javascript:;" class="customAction" data-action="deleteFile" data-file-id="<?=$v['data']['file_id'];?>"><?=\Lang::get('admin.delete');?></a></li>
													<?php if(\Uri::segment(3) == 'edit'): ?>
													<?php if(!empty($v['have_custom'])): ?>
														<li>
															<a href="javascript:;" class="customAction" data-action="editFile" data-key-name="<?=$field->key_name;?>" data-content_field_file_id="<?=$content_field_file_id;?>"><?=\Lang::get('admin.edit');?></a>
														</li>
													<?php endif; ?>
													
													<li>
														<a href="javascript:;" class="customAction" data-file-id="<?=$v['data']['file_id'];?>" data-template_field_id="<?=$field->id;?>" data-content_value_id="<?=$v['data']['content_field_id'];?>" data-action="fileToOtherLangs"><?=\Lang::get('admin.use_in_other_langs');?></a>
													</li>
													<?php endif; ?>
												  </ul>
												</div>											
											</td>
										</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>						
				</div>			
			<?php endif; ?>
			
	<?php endforeach; ?>
	<script>
		var templates_file_custom_fields = <?=\Format::forge($templates_file_custom_fields)->to_json();?>,
			content_files = <?=\Format::forge($files)->to_json();?>;
	</script>
<?php endif; ?>
<?php if(!empty($errors)): ?>
<div class="form-group">
	<label class="col-md-2 control-label"></label>
	<div class="col-md-8">
		<div class="alert alert-block alert-danger fade in" id="have-errors">
			<button data-dismiss="alert" class="close" type="button">× </button>
				<h4 class="alert-heading"><?=\Lang::get('error');?></h4>
					<?php foreach($errors as $v): ?>
				<p><?=$v;?></p>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<script>window.location.href = '#have-errors';</script>	
<?php endif; ?>

<?php if(!empty($update_status)): ?>
	<div class="form-group">
		<label class="col-md-2 control-label"></label>
		<div class="col-md-8">
			<div class="alert alert-block alert-success fade in" id="ok">
				<button data-dismiss="alert" class="close" type="button">× </button>
				<h4 class="alert-heading"><?=\Lang::get('ok');?></h4>
				<p><?=\Lang::get('admin.data_updated');?></p>
			</div>
		</div>
	</div>
	<script>window.location.href = '#ok';</script>	
<?php endif; ?>


<script type="text/x-jqote-template" id="template-field">
	<tr id="file-<%= this.id %>">
		<td>
			<a class="fancy" href="<%= this.url %>"><%= this.name %></a>
		</td>
		<td><%= this.created_at %></td>
		<td>
			<div class="btn-group">
			  <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
				<?=\Lang::get('admin.action');?> 
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu pull-right">
				<li><a href="javascript:;" class="customAction" data-action="deleteFile" data-file-id="<%= this.id %>"><?=\Lang::get('admin.delete');?></a></li>
				<?php if(\Uri::segment(3) == 'edit'): ?>
					<li>
						<a href="javascript:;" class="customAction" data-file-id="<%= this.id %>" data-template_field_id="<%= this.template_field_id %>" data-content_value_id="<%= this.content_value_id %>" data-action="fileToOtherLangs"><?=\Lang::get('admin.use_in_other_langs');?></a>
					</li>
				<?php endif; ?>
			  </ul>
			</div>											
		</td>
	</tr>
</script>

	<div class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="fileModalTrigger" style="display:none;"></div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		 
		 <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel"><?=\Lang::get('admin.edit_files');?></h4>
		  </div>
		  <div class="modal-body"></div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?=\Lang::get('admin.close');?></button>
			<button type="button" class="btn btn-primary customAction" data-action="saveFileData" id="saveFileData"><?=\Lang::get('admin.save_file_data');?></button>
		  </div>
		  
		</div>
	  </div>
	</div>
