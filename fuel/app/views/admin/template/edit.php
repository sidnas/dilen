<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="widget">
			<div class="widget-header">
				<div class="title">
					<?=\Lang::get('edit_template');?>
				</div>
				<span class="tools">
					<i class="fa fa-cogs"></i>
				</span>
			</div>
				<?=$form;?>	
			</div>
		</div>
	</div>
</div>