<div class="widget-body">	
	<?=\Form::open(array(
		'class' => 'form-horizontal row-border'
	));?>
	<div class="form-group">
		<label class="col-md-2 control-label"><?=\Lang::get('admin.template_data');?>: </label>
		<div class="col-md-2">
			<?=\Form::input('name', !empty($template->name) ? $template->name : '', array(
				'class' => 'form-control',
				'placeholder' => \Lang::get('admin.name')
			));?>
		</div>		
		<div class="col-md-2">
			<?=\Form::input('type', !empty($template->type) ? $template->type : '', array(
				'class' => 'form-control',
				'placeholder' => \Lang::get('admin.type')
			));?>
		</div>	
		<div class="col-md-2">
			<?=\Form::input('controller', !empty($template->controller) ? $template->controller : '', array(
				'class' => 'form-control',
				'placeholder' => \Lang::get('admin.controller')
			));?>
		</div>					
	</div>	
	<div class="form-group">
		<label class="col-md-2 control-label"><?=\Lang::get('admin.fields');?>: </label>
		<div class="col-md-6">
		
		</div>
		<div class="col-md-1 t-center">
			<?=\Lang::get('admin.searchable');?>
		</div>
		<div class="col-md-1 t-center">
			<?=\Lang::get('admin.active');?>
		</div>					
	</div>	
	
	<table class="table table-responsive table-striped table-bordered table-hover no-margin">
		  <thead>
			<tr>
			  <th style="width:10%"><?=\Lang::get('admin.name');?></th>
			  <th style="width:10%"><?=\Lang::get('admin.key_name');?> </th>
			  <th style="width:7%" class="t-center"><?=\Lang::get('admin.type');?> </th>
			  <th style="width:15%" class="hidden-xs"><?=\Lang::get('admin.custom_value');?> (JSON)</th>
			  <th style="width:5%" class="t-center"><?=\Lang::get('admin.required');?></th>
			  <th style="width:5%" class="hidden-xs t-center"><?=\Lang::get('admin.searchable');?></th>
			  <th style="width:5%" class="t-center"" class="hidden-xs"><?=\Lang::get('admin.active');?></th>
			  <th style="width:5%" class="t-center"" class="hidden-xs"><?=\Lang::get('admin.delete');?></th>
			</tr>
		  </thead>
		  <tbody id="custom-fields">
			<?php if(!empty($template->fields)): ?>
				<?php $order = 1; ?>
				<?php foreach($template->fields as $field_id => $field): ?>
				<?php
					$custom_value = !empty($field->custom_value) ? \Format::forge($field->custom_value, 'json')->to_array() : array();
				?>				
					<tr class="movable">
						<td>
							<?=\Form::hidden('fields['.$field_id.'][order]', $order++, array(
								'class' => 'order'
							));?>
							<?=\Form::input('fields['.$field_id.'][name]', $field->name, array(
								'class' => 'form-control',
								'placeholder' => \Lang::get('admin.name')
							));?>
						</td>
						<td>
							<?=\Form::input('fields['.$field_id.'][key_name]', $field->key_name, array(
								'class' => 'form-control',
								'placeholder' => \Lang::get('admin.key_name')
							));?>

						</td>
						<td class="t-center">
							<?=\Form::select('fields['.$field_id.'][type]', $field->type, $field_types, array(
								'style' => 'width:100%;'.($field->type == 'filter' ? 'display:none' : '')
							));?>
							
							<?php if($field->type == 'filter'): ?>
								<?php 
									$required_childs = array();
									$filter_required = (int)\Model_Options_Value::child_count($field->filter_id); 
									if($filter_required > 0)
									{
										for($i = 1; $i <= $filter_required; $i++) $required_childs[$i] = $i;
									}
								?>
								<?=\Lang::get('admin.required_filter_childs');?><br/>
								<?=\Form::select('fields['.$field_id.'][custom_value_merge][required_childs]', !empty($custom_value['required_childs']) ? $custom_value['required_childs'] : 1, $required_childs);?>
								<br/><?=\Lang::get('admin.multi_select');?><br/>
								<?=\Form::select('fields['.$field_id.'][custom_value_merge][multi_select]', !empty($custom_value['multi_select']) ? $custom_value['multi_select'] : 1, $multiples);?>
	
							<?php endif; ?>							
							
						</td>
						<td>
							<?=\Form::textarea('fields['.$field_id.'][custom_value]', $field->custom_value, array(
								'class' => 'form-control',
								'placeholder' => \Lang::get('admin.custom_value').'(JSON)'
							));?>					
						</td>
						<td class="t-center">
							<?=\Form::checkbox('fields['.$field_id.'][required]', 1, (!empty($field->required) ? true : false));?>
						</td>			
						<td class="t-center">
							<?=\Form::checkbox('fields['.$field_id.'][searchable]', 1, (!empty($field->searchable) ? true : false));?>
						</td>
						<td class="t-center">
							<?=\Form::checkbox('fields['.$field_id.'][active]', 1, (!empty($field->active) ? true : false));?>
						</td>
						<td class="t-center">
							<?=\Form::checkbox('fields['.$field_id.'][delete]', $field_id);?>
						</td>
					</tr>				
				
				
				<?php endforeach; ?>
			<?php endif; ?>
		  </tbody>
		  <tfoot>
			<tr>
				<td colspan="8">
					<a href="#" class="btn btn-warning customAction" data-action="addField" data-original-title="" title=""><?=\Lang::get('admin.add');?></a>
					<?=\Form::submit('submit', \Lang::get('admin.save'), array(
						'class' => 'btn btn-success'
					));?>					
					<?php if(!empty($options)): ?>
						<?php
							$list = array(0 => '---');
							foreach($options as $v) $list[$v['id']] = $v['option_langs']['name_placeholder'];
						?>
						<?=\Form::select('filters', null, $list, array(
							'class' => 'form-control',
							'onChange' => 'template.createFilterField(this);',
							'style' => 'width:150px;display:inline-block;'
						));?>
						<script>
							var options = <?=\Format::forge($options)->to_json();?>;
						</script>
					<?php endif; ?>					
				</td>
			</tr>
		  </tfoot>
		</table>				

	<div class="form-group">
		<label class="col-md-2 control-label">
			
		</div>
	</div>				
	<?=\Form::close();?>
	
	<script type="text/x-jqote-template" id="template-field">
		<tr class="movable" data-id="<%= this.id %>">
			<td>
				<?=str_replace(array('_order_', '_id_'), array('<%= this.order %>', '<%= this.id %>'), \Form::hidden('fields[_id_][order]', '_order_', array(
								'class' => 'order'
							)));?>
				<?=str_replace('_id_', '<%= this.id %>', \Form::input('fields[_id_][name]', '', array(
					'class' => 'form-control',
					'placeholder' => \Lang::get('admin.name')
				)));?>
			</td>
			<td>
				<?=str_replace('_id_', '<%= this.id %>', \Form::input('fields[_id_][key_name]', '', array(
					'class' => 'form-control',
					'placeholder' => \Lang::get('admin.key_name')
				)));?>					
			</td>
			<td class="t-center">
				<?=str_replace('_id_', '<%= this.id %>', \Form::select('fields[_id_][type]', null, $field_types, array(
					'class' => 'form-control'
				)));?>
			</td>
			<td>
				<?=str_replace('_id_', '<%= this.id %>', \Form::textarea('fields[_id_][custom_value]', '', array(
					'class' => 'form-control',
					'placeholder' => \Lang::get('admin.custom_value').'(JSON)'
				)));?>					
			</td>
			<td class="t-center">
				<?=str_replace('_id_', '<%= this.id %>', \Form::checkbox('fields[_id_][required]', 1));?>
			</td>			
			<td class="t-center">
				<?=str_replace('_id_', '<%= this.id %>', \Form::checkbox('fields[_id_][searchable]', 1));?>
			</td>
			<td class="t-center">
				<?=str_replace('_id_', '<%= this.id %>', \Form::checkbox('fields[_id_][active]', 1));?>
				<?=str_replace('_id_', '<%= this.id %>', \Form::hidden('fields[_id_][filter_id]', 0));?>
			</td>
			<td class="t-center">
				<a href="#" class="fa fa-times customAction" data-action="removeField"></a>
			</td>
		</tr>
	</script>