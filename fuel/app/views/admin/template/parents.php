	<div class="row">
	  <div class="col-lg-12 col-md-12">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  <?=\Lang::get('admin.template_parents');?>
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
		 <?=\Form::open(array(
			'class' => 'form-horizontal row-border',
			'action' => \Uri::create('admin/template/parents?template_id='.\Input::get('template_id'))
		 ));?>
		<div class="form-group">
			<label class="col-md-1 control-label"><?=\Lang::get('admin.template');?>:</label>
			<div class="col-md-3">
				<?=\Form::select('template_id', $current_template, $template_list, array(
					'class' => 'form-control',
					'onChange' => "window.location.href = '?template_id='+this.value"
				));?>
			</div>						
		</div>		
		<?php if(!empty($current_template)): ?>
			<div class="form-group">
				<label class="col-md-1 control-label"><?=\Lang::get('admin.choose_parents');?>:</label>
				<div class="col-md-3">			
					<?php unset($template_list2[$current_template]); ?>
					<?=\Form::select('parent_template_ids[]', $current_parents, $template_list2, array(
						'class' => 'form-control',
						'multiple' => 'multiple',
						'style' => 'height:200px;'
					));?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label">&nbsp;</label>
				<div class="col-md-5">
					<?=\Form::submit('submit', \Lang::get('admin.save'), array(
						'class' => 'btn btn-success'
					));?>
				</div>
			</div>
		<?php endif; ?>
		<?=\Form::close();?>
		  </div>
		</div>
	  </div>
	</div>
