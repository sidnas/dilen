<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="widget">
			<div class="widget-header">
				<div class="title">
					<?=\Lang::get('admin.templates');?>
				</div>
				<span class="tools">
					<i class="fa fa-cogs"></i>
				</span>
			</div>
			<?=\Form::open();?>
				<table class="table table-responsive table-striped table-bordered table-hover no-margin">
					  <thead>
						<tr>
						  <th><?=\Lang::get('admin.id');?></th>
						  <th><?=\Lang::get('admin.name');?> </th>
						  <th><?=\Lang::get('admin.type');?> </th>
						  <th><?=\Lang::get('admin.controller');?></th>
						  <th class="t-center"><?=\Lang::get('admin.delete');?></th>
						  <th class="t-center"><?=\Lang::get('admin.edit');?></th>
						</tr>
					  </thead>
					  <tbody>			
						<?php foreach($templates as $v): ?>
							<tr>
								<td><?=$v->id;?></td>
								<td><?=$v->name;?></td>
								<td><?=$v->type;?></td>
								<td><?=$v->controller;?></td>
								<td class="t-center"><?=\Form::checkbox('delete['.$v->id.']', $v->id);?></td>
								<td class="t-center"><a href="<?=\Uri::create('admin/template/edit/'.$v->id);?>" class="fa fa-pencil-square-o"></a></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					  <tfoot>
						<tr>
							<td colspan="6" class="t-right">
								<?=\Form::submit('submit', \Lang::get('admin.save'), array(
									'class' => 'btn btn-success'
								));?>
							</td>
						</tr>
					  </tfoot>					
				</table>
				<?=\Form::close();?>
			</div>
		</div>
	</div>
</div>			
			