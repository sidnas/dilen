    <div class="dashboard-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-md-offset-4">
            <div class="sign-in-container">
              <?=\Form::open(array(
				'class' => 'login-wrapper'
			  ));?>
                <div class="header">
                  <div class="row">
                    <div class="col-md-12 col-lg-12">
                      <h3>Login<?=\Asset::img('admin/logo1.png', array(
						'class' => 'pull-right'
					  ));?></h3>
                      <p><?=\Lang::get('admin.fill_out_the_form');?></p>
                    </div>
                  </div>
                </div>
                <div class="content">
                  <div class="form-group">
                    <label for="userName"><?=\Lang::get('admin.email');?></label>
					<?=\Form::input('username', '', array(
						'placeholder' => 'Username',
						'class' => 'form-control'
					));?>					
                  </div>
                  <div class="form-group">
                    <label for="Password1"><?=\Lang::get('admin.password');?></label>
					<?=\Form::password('password', '', array(
						'placeholder' => 'Password',
						'class' => 'form-control'
					));?>
                  </div>
                </div>
                <div class="actions">
                  <input class="btn btn-danger" name="Login" type="submit" value="Login">
                  <div class="clearfix"></div>
                </div>
				<?=\Form::close();?>
            </div>
          </div>
        </div>
      </div>
    </div>