	<div class="row">
	  <div class="col-lg-6 col-md-6">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  <?=\Lang::get('admin.edit');?>
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
			<?=\Form::open(array(
				'id' => 'content-form',
				'enctype' => 'multipart/form-data'
			)); ?>
			<table class="table table-responsive table-striped table-bordered table-hover no-margin">
			  <thead>
				<tr>
				  <th style="width:20%;"><?=\Lang::get('admin.title');?></th>
				  <th><?=\Lang::get('admin.country_code');?></th>
				  <th><?=\Lang::get('admin.icon');?></th>
				  <th style="width:5%" class="hidden-xs t-center"><?=\Lang::get('admin.active');?></th>
				  <th style="width:20%" class="hidden-xs t-center"><?=\Lang::get('admin.main_language');?></th>
				  <th style="width:5%" class="hidden-xs"><?=\Lang::get('admin.delete');?></th>
				</tr>
			  </thead>
			  <tbody>
				<?php foreach($languages as $v): ?>
					<?php if(empty($v->id)) continue; ?>
					<tr>
						<td>
							<?=\Form::input('title['.$v->id.']', $v->title, array(
								'class' => 'form-control'
							));?>
						</td>
						<td>
							<?=\Form::input('val['.$v->id.']', $v->val, array(
								'class' => 'form-control',
								'disabled' => true
							));?>
						</td>			  
						<td>
							<?php if(!empty($v->icon)): ?>
								<?=\Asset::img(\Uri::create('upload/icons/'.$v->icon), array(
									'style' => 'max-width:50px;'
								));?>
							<?php endif; ?>
							<?=\Form::file('icon_'.$v->id); ?>
						</td>
						<td class="t-center">
							<?=\Form::checkbox('active['.$v->id.']', 1, ($v->active ? true : false));?>
						</td>
						<td class="t-center">
							<?=\Form::radio('primary', $v->id, ($v->primary ? true : false));?>
						</td>
						<td class="t-center">
							<?=\Form::checkbox('delete['.$v->id.']', $v->id, array(
								'disabled' => ($v->primary ? true : false)
							));?>
							<?=\Form::hidden('id['.$v->id.']', $v->id);?>
						</td>
					</tr>
				<?php endforeach; ?>
					<tr>	
						<td colspan="6">
							<?=\Form::submit('update', \Lang::get('admin.save'), array(
								'class' => 'btn btn-success'
							));?>
						</td>
					</tr>	
			  </tbody>
			</table>
			<?=\Form::close();?>
		  </div>
		</div>
	  </div>
	  
	  <div class="col-lg-6 col-md-6">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  <?=\Lang::get('admin.create');?>
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
			<?=\Form::open(array(
				'id' => 'content-form',
				'enctype' => 'multipart/form-data'
			)); ?>
			<table class="table table-responsive table-striped table-bordered table-hover no-margin">
			  <thead>
				<tr>
				  <th><?=\Lang::get('admin.title');?></th>
				  <th><?=\Lang::get('admin.country_code');?></th>
				  <th><?=\Lang::get('admin.icon');?></th>
				</tr>
			  </thead>
			  <tbody>
				<tr>
					<td>
						<?=\Form::input('title', (\Input::post('create') ? \Input::post('title') : ''), array(
							'class' => 'form-control'
						)); ?>
					</td>
					<td>
						<?=\Form::input('val', (\Input::post('create') ? \Input::post('val') : ''), array(
							'class' => 'form-control'
						)); ?>
					</td>			  
					<td>
						<?=\Form::file('icon'); ?>
					</td>			  
				</tr>
					<tr>	
						<td colspan="3">
							<?=\Form::submit('create', \Lang::get('admin.create'), array(
								'class' => 'btn btn-success'
							));?>
						</td>
					</tr>					
			  </tbody>
			</table>
			<?=\Form::close();?>
			
			<?php if(\Session::get_flash('create_error')): ?>
				<div class="alert alert-block alert-danger fade in alert-spacer">
					<button data-dismiss="alert" class="close" type="button">× </button>
					<h4 class="alert-heading"><?=\Lang::get('error');?></h4>
					<p><?=\Session::get_flash('create_error');?></p>
				</div>
			<?php endif; ?>
			
			<?php if(\Session::get_flash('create_ok')): ?>
				<div class="alert alert-block alert-success fade in alert-spacer">
					<button data-dismiss="alert" class="close" type="button">× </button>
					<h4 class="alert-heading"><?=\Lang::get('ok');?></h4>
					<p><?=\Session::get_flash('create_ok');?></p>
				</div>
			<?php endif; ?>			

		  </div>
		</div>
	  </div>	  
	  
	</div>
