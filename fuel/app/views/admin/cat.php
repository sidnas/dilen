	<header>
	  <a href="<?=\Uri::create('admin/dashboard');?>" class="logo">
		<?=\Asset::img('admin/logo.png');?>
	  </a>
	  <div class="user-profile">
		<a data-toggle="dropdown" class="dropdown-toggle">
		  <?=\Asset::img('admin/profile1.png');?>
		  <span class="caret"></span>
		</a>
		<ul class="dropdown-menu pull-right">
		  <li>
			<a href="<?=\Uri::create('admin/?logout=ok');?>">
			  <?=\Lang::get('admin.logout');?>
			</a>
		  </li>
		</ul>
	  </div>
	</header>

   <div class="dashboard-container">
      <div class="container">
        <!-- Top Nav Start -->
        <div class="top-nav hidden-xs hidden-sm">
          <ul>
			<?php foreach($cats as $v): ?>
				<li>
				  <a href="<?=\Uri::create('admin/'.$v['controller']);?>" <?=($current_cat['id'] == $v['id']) ? 'class="selected"' : '';?>>
					<i class="fa <?=$v['css_class'];?>"></i>
					<?=$v['title'];?>
				  </a>
				</li>
			<?php endforeach; ?>
          </ul>
          <div class="clearfix"></div>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav">
          <ul>
            <li><a href="" class="heading"><?=$current_cat['title'];?></a></li>
			<?php foreach($cats[$current_cat['id']]['sub'] as $v): ?>
				<li class="hidden-sm hidden-xs">
				  <a href="<?=\Uri::create('admin/'.$v['controller']);?>"><?=$v['title'];?></a>
				</li>
			<?php endforeach; ?>
			
          </ul>
          <input type="search" placeholder="Search" class="input-search hidden-xs hidden-sm" />
          <div class="btn-group pull-right hidden-lg hidden-md">
            <button class="btn btn-danger btn-sm">
              <i class="fa fa-bars"></i>
            </button>
            <button data-toggle="dropdown" class="btn btn-danger btn-sm dropdown-toggle">
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right">
			<?php foreach($cats as $v): ?>
              <li>
                <a href="<?=\Uri::create('admin/'.$v['controller']);?>" data-original-title="<?=$v['title'];?>">
                  <?=$v['title'];?>
                </a>
              </li>asdasd
			  <?php endforeach; ?>
            </ul>
          </div>
        </div>
	</div>
</div>