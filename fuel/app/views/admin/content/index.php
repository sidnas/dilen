	<div class="row">
			<?php if(!empty($child['id'])): ?>
			<div class="form-group">
				<a class="content-back" href="<?=\Uri::create('admin/content/index/'.(!empty($child['parent_id']) ? $child['parent_id'] : '').'?position_id='.$child['position']);?>"><?=\Lang::get('admin.back_to_parent');?></b></a>
			</div>
			<?php endif; ?>		
	  <div class="col-lg-12 col-md-12">
	  
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  Tables
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
		 <?=\Form::open(array(
			'class' => 'form-horizontal row-border'
		 ));?>
		<div class="form-group">
			<label class="col-md-1 control-label"><?=\Lang::get('admin.position');?>:</label>
			<div class="col-md-3">
				<?=\Form::select('template_id', $current_position, $positions, array(
					'class' => 'form-control',
					'onChange' => "window.location.href = '?position_id='+this.value"
				));?>
			</div>
			<div class="col-md-1">
				<a href="<?=\Uri::create('admin/content/create/'.$current_position);?>" class="form-control btn btn-default"><?=\Lang::get('admin.create');?></a>
			</div>						
		</div>
		<?=\Form::close();?>
			<?=\Form::open(array(
				'id' => 'content-form',
				'action' => \Uri::create().'?position_id='.$current_position
			)); ?>
			<table class="table table-responsive table-striped table-bordered table-hover no-margin">
			  <thead>
				<tr>
				  <th style="width:5%" class="t-center"><?=\Lang::get('admin.order');?></th>
				  <th style="width:40%"><?=\Lang::get('admin.title');?></th>
				  <th style="width:20%" class="hidden-xs t-center"><?=\Lang::get('admin.sub_contents');?></th>
				  <th style="width:10%" class="hidden-xs t-center"><?=\Lang::get('admin.create_sub_content');?></th>
				  <th style="width:15%" class="hidden-xs"><?=\Lang::get('admin.created_at');?></th>
				   <th style="width:15%" class="hidden-xs"><?=\Lang::get('admin.updated_at');?></th>
				  <th style="width:10%" class="hidden-xs">
					Actions
				  </th>
				</tr>
			  </thead>
			  <tbody id="content-data">
				<?php foreach($content_tree as $v): ?>
				<tr data-node-id="<?=$v['node_id'];?>" id="content-<?=$v['id'];?>">
				  <td class="t-center"><div class="fa fa-arrows-alt movable-content"></div></td>
				  <td>
					<span class="label label-<?=$v['status']['status'];?>"><?=\Lang::get('admin.'.$v['status']['name']);?></span>
					<span class="name"><?=$v['title'];?></span>
				  </td>
				  <td class="t-center">
					<a href="<?=($v['sub_cat_count'] > 0) ? \Uri::create('admin/content/index/'.$v['id'].'?position_id='.$current_position) : '#';?>">( <?=$v['sub_cat_count'];?> )</a>
				  </td>
				  <td class="t-center">
					<?php if(!empty($v['allowed_parents'])): ?>
						<div class="btn-group t-left">
						  <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
							<?=\Lang::get('admin.create');?> 
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu pull-right">
						  <?php foreach($v['allowed_parents'] as $template_id => $template_name): ?>
							<li>
							  <a href="<?=\Uri::create('admin/content/create/'.$current_position.'/'.$v['id'].'/?template_id='.$template_id);?>"><?=$template_name;?></a>
							</li>
							<?php endforeach; ?>
						  </ul>
						</div>
					<?php else: ?>
						---
					<?php endif; ?>
					<span class="label label-info"></span>
				  </td>
				  <td><?=date('d.m.Y H:i:s', $v['created_at']);?></td>
				  <td><?=!empty($v['updated_at']) ? date('d.m.Y H:i:s', $v['updated_at']) : '---';?></td>
				  <td class="hidden-xs">
					<div class="btn-group">
					  <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
						<?=\Lang::get('admin.action');?> 
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu pull-right">
						<li>
						  <a href="<?=\Uri::create('admin/content/edit/'.$v['id']);?>"><?=\Lang::get('admin.edit');?></a>
						</li>
						<li><a href="javascript:;" class="delete-content" data-id="<?=$v['id'];?>" data-quest="<?=\Lang::get('admin.delete_content_quest');?>"><?=\Lang::get('admin.delete');?></a></li>
					  </ul>
					</div>
				  </td>
				</tr>
				<?php endforeach; ?>
			  </tbody>
			</table>
			<?=\Form::close();?>
		  </div>
		</div>
	  </div>
	</div>
