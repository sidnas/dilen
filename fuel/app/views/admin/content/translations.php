<div class="modal fade" id="translation-pop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" id="myModalLabel"><?=\Lang::get('admin.translate_static_data');?></h4>
	  </div>
	  <?=\Form::open(\Uri::base(false).'admin/content/edit/'.\Uri::segment(4).'?language_id='.\Input::get('language_id'));?>
	  <div class="modal-body">
		<?php if(!empty($translations)): ?>
			<?php foreach($translations as $k => $v): ?>	
				<div class="form-group">
					<div class="col-md-12">
						<?php $field_type = ($v['field_type'] ? 'textarea' : 'input'); ?>
						<?=\Form::$field_type('static_value['.$k.']', $v['value'], array(
							'class' => 'form-control'
						));?>
						<?=\Form::input('static_key['.$k.']', $k, array(
							'class' => 'form-control key',
							'data-key' => $k,
							'readonly' => 'readonly'
						));?>	
						<a href="<?=\Uri::base(false).'admin/content/edit/'.\Uri::segment(4).'?language_id='.\Input::get('language_id');?>" class="fa fa-times customAction" data-action="removeStaticTranslation" data-key="<?=$k;?>"></a>
					</div>						
				</div>			
			<?php endforeach; ?>
		<?php else: ?>
			<?=\Lang::get('admin.no_static_data_found');?>
		<?php endif; ?>
	  </div>
	  
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal" data-original-title="" title=""><?=\Lang::get('admin.close');?></button>
		<button type="button" class="btn btn-primary customAction" data-original-title="" data-action="saveStaticData" title=""><?=\Lang::get('admin.save');?></button>
	  </div>
	  <?php \Form::close();?>
	</div>
  </div>
</div>
<button class="btn btn-primary" data-toggle="modal" data-target="#translation-pop" data-original-title="" title="" style="display:none;" id="translationModal"></button>