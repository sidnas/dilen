<?php if(!empty($fields)) :?>
	<?php foreach($fields as $name => $field): ?>
		<?php if($field['type'] ==  'text_field'): ?>
				<div class="form-group" id="sub_field_<?=$name;?>">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.'.$name);?>:<?=!empty($field['required']) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::input('sub_field['.$name.']', (!empty($sub_value[$name]) ? $sub_value[$name] : (\Input::post('sub_field.'.$name) ? \Input::post('sub_field.'.$name) : !empty($field['default_value']) ? $field['default_value'] : '')), array(
							'class' => 'form-control small'
						));?>
					</div>						
				</div>	
		<?php elseif($field['type'] == 'date_picker'): ?>
				<div class="form-group" id="sub_field_<?=$name;?>">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.'.$name);?>:<?=!empty($field['required']) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::input('sub_field['.$name.']', (!empty($sub_value[$name]) ? $sub_value[$name] : \Input::post('sub_field.'.$name)), array(
							'class' => 'form-control date-picker small'
						));?>
					</div>						
				</div>			
		<?php elseif($field['type'] == 'date_time_picker'): ?>
				<div class="form-group" id="sub_field_<?=$name;?>">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.'.$name);?>:<?=!empty($field['required']) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::input('sub_field['.$name.']', (!empty($sub_value[$name]) ? $sub_value[$name] : \Input::post('sub_field.'.$name)), array(
							'class' => 'form-control date-time-picker small'
						));?>
					</div>						
				</div>			
		<?php elseif($field['type'] == 'checkbox'): ?>
				<div class="form-group" id="sub_field_<?=$name;?>">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.'.$name);?>:<?=!empty($field['required']) ? '<span class="required">*</span>' : '';?> </label>
					<div class="col-md-8">
						<?=\Form::checkbox('sub_field['.$name.']', 1, (!empty($sub_value[$name]) ? $sub_value[$name] : \Input::post('sub_field.'.$name)), array(
							'onChange' => 'sys.showHideChilds(this, \''.(!empty($field['childs']) ? implode(',', $field['childs']) : '').'\');'
						));?>
					</div>						
				</div>			
		<?php endif; ?>
		
		
		<?php if(!empty($field['childs']) && !empty($field['childs_hide_if_parent_inactive'])): ?>
			<?php if(empty($sub_value[$name]) && !\Input::post('sub_field.'.$name)): ?>
				<?php $c = count($field['childs']) - 1; ?>
				<style><?php foreach($field['childs'] as $k => $v): ?> #sub_field_<?=$v;?><?=($c > $k) ? ', ' : '';?><?php endforeach; ?>{display:none;}</style>
			<?php endif; ?>
		
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>
