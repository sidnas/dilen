<div class="row">
	<?php if(!empty($child['id'])): ?>
	<div class="form-group">
		<a class="content-back" href="<?=\Uri::create('admin/content/index/'.(!empty($child['parent_id']) ? $child['parent_id'] : '').'?position_id='.$child['position']);?>"><?=\Lang::get('admin.back_to_parent');?></b></a>
	</div>
	<?php endif; ?>	
	<div class="col-lg-12 col-md-12">
		<div class="widget">
			<div class="widget-header">
				<div class="title">
					<?=\Lang::get('admin.edit_content');?>
				</div>
				<span class="tools">
					<i class="fa fa-cogs"></i>
				</span>
			</div>
			
			<div class="widget-body">	
				<?=$form;?>
			</div>
		</div>
	</div>
</div>