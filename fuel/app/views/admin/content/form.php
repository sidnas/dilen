	<?=\Form::open(array(
		'class' => 'form-horizontal row-border',
		'action' => $form_action
	));?>
			<?php if(\Uri::segment(3) != 'create'): ?>
				<div class="form-group">
					<label class="col-md-2 control-label"><?=\Lang::get('admin.choose_content_language');?>:</label>
					<div class="col-md-10">
						<div class="btn-group">
						<?php foreach($languages as $v): ?>
							  <a href="?language_id=<?=$v['id'];?>" class="btn <?=($v['id'] == $language_id) ? 'btn-info' : 'btn-default';?>"><?=$v['title'];?></a>
						  <?php endforeach; ?>
						<a href="<?=$form_action;?>" class="btn btn-warning" id="staticTranslations"><?=\Lang::get('admin.static_data');?></a>
						</div>			
					</div>
				</div>
			<?php endif; ?>
		<div class="form-group">
			<label class="col-md-2 control-label"><?=\Lang::get('admin.template');?>:<span class="required">*</span> </label>
			<div class="col-md-3">
				<?=\Form::select('template_id', $picked_template, $templates, array(
					'class' => 'form-control',
					'onChange' => "window.location.href='?template_id='+this.value",
					'disabled' => !empty($content) ? true : false
				));?>
			</div>						
		</div>

		<?php if($picked_template): ?>
		
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.title');?>:<span class="required">*</span> </label>
				<div class="col-md-5">
					<?=\Form::input('title', !empty($content->content_langs->title) ? $content->content_langs->title : \Input::post('title'), array(
						'class' => 'form-control'
					));?>
				</div>						
			</div>					
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.slug');?>: </label>
				<div class="col-md-5">
					<?=\Form::input('slug', !empty($content->content_langs->slug) ? $content->content_langs->slug : \Input::post('slug'), array(
						'class' => 'form-control'
					));?>
				</div>						
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.meta_keywords');?>: </label>
				<div class="col-md-5">
					<?=\Form::input('meta_keywords', !empty($content->content_langs->meta_keywords) ? $content->content_langs->meta_keywords : \Input::post('meta_keywords'), array(
						'class' => 'form-control'
					));?>
				</div>						
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.meta_description');?>: </label>
				<div class="col-md-5">
					<?=\Form::input('meta_description', !empty($content->content_langs->meta_description) ? $content->content_langs->meta_description : \Input::post('meta_description'), array(
						'class' => 'form-control'
					));?>
				</div>						
			</div>		
			<?=$sub_form;?>
			<?=$custom_fields;?>
			
			<div class="form-group">
				<label class="col-md-2 control-label"><?=\Lang::get('admin.active');?>: </label>
				<div class="col-md-5">
					<?=\Form::checkbox('active', 1, (!empty($content->content_langs->active) ? true : false));?>
				</div>						
			</div>	
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-5">
					<?=\Form::submit('submit', \Lang::get('admin.save'), array(
						'class' => 'btn btn-success'
					));?>
				</div>
			</div>
		
		
		<?php endif; ?>
		
		
	<?=\Form::close();?>