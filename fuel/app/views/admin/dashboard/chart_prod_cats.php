<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() 
  {
	var data = google.visualization.arrayToDataTable(<?=\Format::forge($chart_data)->to_json();?>);
	var options = {
	  title: '<?=\Lang::get('admin.prod_cats');?>',
       pieHole: 1
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('prod_cats'));
	chart.draw(data, options);
  }
</script>
<div class="basic-chart" id="prod_cats" style="width:600px;"></div>