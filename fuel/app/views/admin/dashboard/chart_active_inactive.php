<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() 
  {
	var data = google.visualization.arrayToDataTable(<?=\Format::forge($chart_data)->to_json();?>);
	var options = {
	  title: '<?=\Lang::get('admin.active_inactive_posters');?>',
       pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById('active_inactive'));
	chart.draw(data, options);
  }
</script>
<div class="basic-chart" id="active_inactive"></div>