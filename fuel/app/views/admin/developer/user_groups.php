	<div class="row">
	  <div class="col-lg-12 col-md-12">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  Tables
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
		 <?=\Form::open(array(
			'class' => 'form-horizontal row-border',
			'action' => \Uri::create('admin/developer/user_groups?user_group='.\Input::get('user_group'))
		 ));?>
		<div class="form-group">
			<label class="col-md-1 control-label"><?=\Lang::get('admin.user_group');?>:</label>
			<div class="col-md-3">
				<?=\Form::select('user_group', $current_group, $user_groups, array(
					'class' => 'form-control',
					'onChange' => "window.location.href = '?user_group='+this.value"
				));?>
			</div>					
		</div>
		
		<?php if(!empty($current_group)): ?>
		
			<div class="form-group">
				<label class="col-md-1 control-label"><?=\Lang::get('admin.choose_parents');?>:</label>
				<div class="col-md-3">			
					<?=\Form::select('cat_ids[]', $admin_cats, $cats, array(
						'class' => 'form-control',
						'multiple' => 'multiple',
						'style' => 'height:200px;'
					));?>
				</div>
			</div>

			
			
			<div class="form-group">
				<label class="col-md-1 control-label">&nbsp;</label>
				<div class="col-md-5">
					<?=\Form::submit('submit', \Lang::get('admin.save'), array(
						'class' => 'btn btn-success'
					));?>
				</div>
			</div>		
		
		
		
		
		<?php endif; ?>
		
		<?=\Form::close();?>