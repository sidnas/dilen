	<div class="row">
	  <div class="col-lg-12 col-md-12">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  <?=\Lang::get('admin.edit');?>
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
			<?=\Form::open(array(
				'id' => 'content-form',
				'enctype' => 'multipart/form-data'
			)); ?>
			<table class="table table-responsive table-striped table-bordered table-hover no-margin">
			  <thead>
				<tr>
				  <th style="width:5%;"><?=\Lang::get('admin.posters');?></th>				  
				  <th style="width:15%;"><?=\Lang::get('admin.fullname');?></th>
				  <th style="width:15%;"><?=\Lang::get('admin.email');?></th>
				  <th style="width:15%;"><?=\Lang::get('admin.password');?></th>
				  <th><?=\Lang::get('admin.group');?></th>
				  <th style="width:5%" class="hidden-xs t-center"><?=\Lang::get('admin.last_login');?></th>
				  <th style="width:5%" class="hidden-xs"><?=\Lang::get('admin.delete');?></th>
				</tr>
			  </thead>
			  <tbody>
				<?php foreach($users as $v): ?>
					<?php if(empty($v->id)) continue; ?>
					<tr>
						<td class="t-center"><a href="#">(<?=((int)count($v->posters));?>)</a></td>
						<td>
							<?=\Form::input('fullname['.$v->id.']', $v->fullname, array(
								'class' => 'form-control'
							));?>
						</td>
						<td>
							<?=\Form::input('email['.$v->id.']', $v->email, array(
								'class' => 'form-control'
							));?>
						</td>						
						<td>
							<?=\Form::password('password['.$v->id.']', '', array(
								'class' => 'form-control'
							));?>
						</td>
						<td>
							<?=\Form::select('group['.$v->id.']', $v->group, $groups, array(
								'class' => 'form-control'
							));?>
						</td>
						<td class="t-center">
							<?=!empty($v->last_login) ? date('d.m.Y H:i:s', $v->last_login) : '---';?>
						</td>
						<td class="t-center">
							<?=\Form::checkbox('delete['.$v->id.']', $v->id);?>
							<?=\Form::hidden('id['.$v->id.']', $v->id);?>
						</td>
					</tr>
				<?php endforeach; ?>
					<tr>	
						<td colspan="6">
							<?=\Form::submit('update', \Lang::get('admin.save'), array(
								'class' => 'btn btn-success'
							));?>
						</td>
					</tr>	
			  </tbody>
			</table>
			<?=\Form::close();?>
		  </div>
		</div>
	  </div>
	  
	  <div class="col-lg-12 col-md-12">
		<div class="widget">
		  <div class="widget-header">
			<div class="title">
			  <?=\Lang::get('admin.create');?>
			</div>
			<span class="tools">
			  <i class="fa fa-cogs"></i>
			</span>
		  </div>
		  <div class="widget-body">
			<?=\Form::open(array(
				'id' => 'content-form',
				'enctype' => 'multipart/form-data'
			)); ?>
			<table class="table table-responsive table-striped table-bordered table-hover no-margin">
			  <thead>
				<tr>
				  <th><?=\Lang::get('admin.username');?></th>
				  <th><?=\Lang::get('admin.email');?></th>
				  <th><?=\Lang::get('admin.password');?></th>
				  <th><?=\Lang::get('admin.group');?></th>
				</tr>
			  </thead>
			  <tbody>
				<tr>
					<td>
						<?=\Form::input('username', (\Input::post('create') ? \Input::post('username') : ''), array(
							'class' => 'form-control'
						)); ?>
					</td>
					<td>
						<?=\Form::input('email', (\Input::post('create') ? \Input::post('email') : ''), array(
							'class' => 'form-control'
						)); ?>
					</td>			  
					<td>
						<?=\Form::password('password', '', array(
							'class' => 'form-control'
						)); ?>
					</td>
					<td>
						<?=\Form::select('group', (\Input::post('create') ? \Input::post('group') : ''), $groups, array(
							'class' => 'form-control'
						));?>					
					</td>
				</tr>
					<tr>	
						<td colspan="4">
							<?=\Form::submit('create', \Lang::get('admin.create'), array(
								'class' => 'btn btn-success'
							));?>
						</td>
					</tr>					
			  </tbody>
			</table>
			<?=\Form::close();?>
			
			<?php if(\Session::get_flash('error')): ?>
				<div class="alert alert-block alert-danger fade in alert-spacer">
					<button data-dismiss="alert" class="close" type="button">× </button>
					<h4 class="alert-heading"><?=\Lang::get('error');?></h4>
					<p><?=\Session::get_flash('error');?></p>
				</div>
			<?php endif; ?>
			
			<?php if(\Session::get_flash('create_ok')): ?>
				<div class="alert alert-block alert-success fade in alert-spacer">
					<button data-dismiss="alert" class="close" type="button">× </button>
					<h4 class="alert-heading"><?=\Lang::get('ok');?></h4>
					<p><?=\Session::get_flash('create_ok');?></p>
				</div>
			<?php endif; ?>			

		  </div>
		</div>
	  </div>	  
	  
	</div>
