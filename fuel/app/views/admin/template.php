<!DOCTYPE html>
<html>
  <head>
    <title><?=\Lang::get('admin.cms_name');?><?=!empty($title) ? ' - '.$title : '';?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""/>
    <meta name="keywords" content="" />  
	<meta name="author" content="Sidnas" />
    <link rel="shortcut icon" href="<?=\Uri::base(false);?>assets/img/admin/favicon.ico">
	<?=\Asset::css(\Arr::merge(array(
		'jquery-ui.css',
		'jquery.datetimepicker.css',
		'admin/bootstrap.min.css',
		'../js/fancybox/jquery.fancybox.css',
		'admin/new.css',
		'admin/fonts/font-awesome.min.css',
		'admin/alertify.core.css',
		'admin/custom.css'
	), $css));?>
	<script type="text/javascript">
		var lang = {
				'please_wait' : '<?=\Lang::get('admin.please_wait');?>',
				'action_canceled' : '<?=\Lang::get('admin.action_is_canceled');?>',
				'some_error' : '<?=\Lang::get('admin.some_error');?>',
				'action_ok' : '<?=\Lang::get('admin.action_ok');?>',
				'_delete' : '<?=\Lang::get('admin.delete');?>'
			},
			language_id = '<?=$language_id;?>',
			config = {
				'base' : '<?=\Uri::base(false);?>admin/',
				'web_ajax' : '<?=\Uri::base(false).$current_language->val;?>/ajax/'
			};
	</script>
    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<?=\Asset::js(array(
			'admin/html5shiv.js'
		));?>
    <![endif]-->
	<?=\Asset::js(array(
		'admin/jquery.js',
		'jquery-migrate-1.0.0.js',			
		'//www.google.com/jsapi',
		'jquery.mousewheel-3.0.6.pack.js',
		'fancybox/jquery.fancybox.js',
		'admin/md5.js',		
	));?>
	</head>
	<body>
		<?php if($current_user): ?>
			<?=$header;?>
			<div class="dashboard-container base-content">
				<div class="container">
					<div class="dashboard-wrapper">		
						<div class="left-sidebar">
							<?=$content;?>				
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			
			<?=$content;?>
		
		<?php endif; ?>
		
		<?=\Asset::js(\Arr::merge(array(
			'admin/bootstrap.min.js',
			'admin/alertify.min.js',
			'admin/jquery.uploadifive.min.js',
			'jquery.datetimepicker.js',
			'admin/custom.js',
			'admin/sys.js'
		), $js));?>
	</body>
</html>