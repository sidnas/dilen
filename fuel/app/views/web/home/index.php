<?php
if (!empty($contents['home'])):
    $home = $contents['home'];
    ?>
    <div class="pi-section-w pi-section-base" id="<?= $contents['home']['slug']; ?>">
        <div class="pi-section pi-padding-top-20 pi-padding-bottom-10">
            <h2 class="pi-padding-bottom-10"><?= $home['value']['header']; ?></h2>
            <?= $home['value']['description']; ?>
        </div>
    </div>
    <?php
endif;
?>


<?php if (count($offers['offerProd']) > 0): ?>
    <div class="pi-section-w pi-section-white" id="<?= $contents['holder']['slug']; ?>">
        <div class="pi-section pi-padding-top-30">
            <div class="pi-row pi-margin-bottom-40">
                <div data-isotope-nav="isotope" class="pi-pagenav pi-padding-bottom-20 pi-big pi-text-center">
                    <ul>
                        <?php foreach ($offers['offerCats'] as $cat): ?>
                            <li><a data-filter=".<?= $cat['slug']; ?>" href="#" class="singleCat"><?= $cat['title']; ?></a></li> 
                        <?php endforeach; ?>
                        <li><a data-filter="*" class="pi-active" id="allCat" href="#">Viss</a></li>
                    </ul>
                </div>
                <div class="pi-row">
                    <div class="pi-col-sm-12">
                        <div id="isotope" class="pi-row pi-liquid-col-2xs-2 pi-liquid-col-xs-3 pi-liquid-col-sm-4 pi-gallery pi-gallery-small-margins pi-text-center isotope">

                            <?php
                            foreach ($offers['offerProd'] as $key => $catProducts) {
                                foreach ($catProducts as $prod):

                                    $values = $prod['value'];                                
                                    $desc = (isset($values['description'])) ? $values['description'] : "";
                                    $images = current($values['images']);
                                    $imgUrl = \Uri::create($images['0']);
                                    $smallImgUrl = \Uri::create($images['small']);
                                    $price = (strlen($prod['value']['price'] > 0)) ? $prod['value']['price'].' €' : "";
                                    ?>

                                    <div class="<?= $key; ?> pi-gallery-item pi-padding-bottom-40 isotope-item">
                                        <div class="pi-img-w pi-img-round-corners pi-img-shadow pi-margin-bottom-5">
                                            <a class="pi-colorbox cboxElement fancy" data-rel="<?= $key ?>" rel="all" href="<?= $imgUrl; ?>" title="<?= $desc; ?>">
                                                <img alt="<?= $prod['title']; ?>" src="<?= $smallImgUrl; ?>">
                                                <div class="pi-img-overlay pi-no-padding pi-img-overlay-darker">
                                                    <div class="pi-caption-centered">
                                                        <div><span class="pi-caption-icon pi-caption-scale icon-search"></span></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <h3 class="h6 pi-weight-700 pi-uppercase pi-letter-spacing pi-no-margin-bottom"><a class="pi-link-dark" href="#"><?= $prod['title']; ?></a></h3>
                                        <p class="pi-italic pi-margin-bottom-5"><?= $price; ?></p>
                                    </div>

                                    <?php
                                endforeach;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
<?php endif; ?>

<?php
if (!empty($contents['contacts'])):
    $contacts = $contents['contacts'];
    ?>
    <div class="pi-section-w pi-border-top pi-section-dark" id="<?= $contents['contacts']['slug']; ?>">
        <div class="pi-section pi-padding-top-50 pi-padding-bottom-20">
            <div class="pi-row contacts">
                <div class="pi-col-md-5-cell pi-col-xs-6 pi-padding-bottom-30">
                    <div class="pi-icon-box-vertical pi-text-center">
                        <div class="pi-icon-box-icon pi-icon-box-icon-base"><i class="icon-location"></i></div>
                        <h6 class="pi-margin-bottom-20 pi-weight-700 pi-uppercase pi-letter-spacing"><?= $main_translation->get('adress', 'Adrese'); ?></h6>
                        <?= $contacts['value']['address']; ?>
                    </div>
                </div>
                <div class="pi-col-md-5-cell pi-col-xs-6 pi-padding-bottom-30">
                    <div class="pi-icon-box-vertical pi-text-center">
                        <div class="pi-icon-box-icon pi-icon-box-icon-base"><i class="icon-phone"></i></div>
                        <h6 class="pi-margin-bottom-20 pi-weight-700 pi-uppercase pi-letter-spacing"><?= $main_translation->get('phone', 'Tālrunis'); ?></h6>
                        <?= $contacts['value']['phone']; ?>
                    </div>
                </div>
                <div class="pi-col-md-5-cell pi-col-xs-6 pi-padding-bottom-30">
                    <div class="pi-icon-box-vertical pi-text-center">
                        <div class="pi-icon-box-icon pi-icon-box-icon-base"><i class="icon-mail"></i></div>
                        <h6 class="pi-margin-bottom-20 pi-weight-700 pi-uppercase pi-letter-spacing"><?= $main_translation->get('email', 'Epasts'); ?></h6>
                        <?= $contacts['value']['email']; ?>
                    </div>
                </div>
                <div class="pi-col-md-5-cell pi-col-xs-6 pi-padding-bottom-30">
                    <div class="pi-icon-box-vertical pi-text-center">
                        <div class="pi-icon-box-icon pi-icon-box-icon-base"><i class="icon-clock"></i></div>
                        <h6 class="pi-margin-bottom-20 pi-weight-700 pi-uppercase pi-letter-spacing"><?= $main_translation->get('shop_work_time', 'Veikala darba laiks'); ?></h6>
                        <?= $contacts['value']['shop_wt']; ?>
                    </div>
                </div>
                <div class="pi-col-md-5-cell pi-col-xs-6 pi-padding-bottom-30">
                    <div class="pi-icon-box-vertical pi-text-center">
                        <div class="pi-icon-box-icon pi-icon-box-icon-base"><i class="icon-clock"></i></div>
                        <h6 class="pi-margin-bottom-20 pi-weight-700 pi-uppercase pi-letter-spacing"><?= $main_translation->get('commision_work_time', 'Komisijas darba laiks'); ?></h6>
                        <?= $contacts['value']['commision_wt']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>