<div class="contact-page">
	<div class="map">
		<?=!empty($current_page['value']['embed_code']) ? $current_page['value']['embed_code'] : '';?>
	</div>
	<div class="text">
	<h2><?=!empty($current_page['value']['title']) ? $current_page['value']['title'] : $current_page['title'];?></h2>
		<div class="contact-data">
			<?php if(!empty($current_page['value']['address'])): ?>
				<div class="info-block">	
					<p class="name"><?=$main_translation->get('address', 'Adrese');?>:</p>
					<p><?=nl2br($current_page['value']['address']);?></p>
				</div>
			<?php endif; ?>
			<?php if(!empty($current_page['value']['phones'])): ?>
				<div class="info-block">	
					<p class="name"><?=$main_translation->get('phones', 'Tālruņi');?>:</p>
					<p><?=nl2br($current_page['value']['phones']);?></p>
				</div>
			<?php endif; ?>
			<?php if(!empty($current_page['value']['email'])): ?>
				<div class="info-block">	
					<p class="name"><?=$main_translation->get('email', 'E-pasts');?>:</p>
					<p><?=nl2br($current_page['value']['email']);?></p>
				</div>
			<?php endif; ?>
			<?php if(!empty($current_page['value']['gps'])): ?>
				<div class="info-block">	
					<p class="name"><?=$main_translation->get('gps', 'GPS');?>:</p>
					<p><?=nl2br($current_page['value']['gps']);?></p>
				</div>
			<?php endif; ?>		
			<?php if(!empty($current_page['value']['distances'])): ?>
				<div class="info-block">	
					<p class="name"><?=$main_translation->get('distances', 'Attālumi');?>:</p>
					<p><?=nl2br($current_page['value']['distances']);?></p>
				</div>
			<?php endif; ?>	
		</div>
	</div>
</div>