<div class="article no-bg text-page">
	<?php $image = null; ?>
	<?php
	if(!empty($current_page['value']['image']) && !empty($current_page['value']['image']['medium']))
	{
		$image = array(
			'medium' => \Uri::create($current_page['value']['image']['medium']),
			'orginal' => \Uri::create($current_page['value']['image'][0])
		);
	}
	?>
	<?php if($image): ?>
		<div class="image">
			<a href="<?=$image['orginal'];?>" class="fancy">
				<?=\Asset::img($image['medium'], array(
					'alt' => !empty($current_page['value']['title']) ? $current_page['value']['title'] : $current_page['title']
				));?>
			</a>
		</div>
	<?php endif; ?>
	<div class="content<?=(!$image ? ' fullwidth' : '');?>">
		<h1>Biedrība · Oleru Muiža ·</h1>
		<div class="text">
			<?=!empty($current_page['value']['content']) ? $current_page['value']['content'] : '';?>
		</div>
	</div>
</div>