<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title><?= $main_translation->get('page_name', 'Dilen'); ?></title>
    <meta name="keywords" content="<?= !empty($meta_keywords) ? $meta_keywords : ''; ?>"/>
    <meta name="description" content="<?= !empty($meta_description) ? $meta_description : ''; ?>"/>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="shortcut icon" href="<?= \Uri::create('favicon.ico'); ?>" type="image/x-icon">
    <link rel="icon" href="<?= \Uri::create('favicon.ico'); ?>" type="image/x-icon">
    <?php if (!empty($og_tags)): ?>
      <?php foreach ($og_tags as $k => $v): ?>
        <meta property="og:<?= $k; ?>" content="<?= $v; ?>" />			
      <?php endforeach; ?>
    <?php endif; ?>
    <?=
    \Asset::css(\Arr::merge(array(
                \Uri::create('assets/js/fancybox/jquery.fancybox.css?'),
                //'flexslider/flexslider.css',
                'pi.global.css',
                'pi.typo.css',
                'pi.social.css',
                'pi.portfolio.css',
                'pi.page-nav.css',
                'style.css',
                //'responsive.css',
                \Uri::create('assets/fontello/css/fontello.css'),
                    ), !empty($css) ? $css : array()));
    ?>
    <?= \Security::js_fetch_token(); ?>
    <script type="text/javascript">
      var config = {
        base_url : '<?= \Uri::base(false) . $current_language->val; ?>/',
        full_url : '<?= \Uri::base(false) . implode('/', array_filter($uri)); ?>',
        ajax_url : '<?= \Uri::base(false) . $current_language->val; ?>/ajax/',
        params : '<?= \Util\Sys::current_params(); ?>',
        token_key : '<?= \Config::get('security.csrf_token_key'); ?>',
        current_page_id : '<?=!empty($current_page_id) ? $current_page_id : '';?>'
      },
      lang = {};
    </script>
    
    <?=
    \Asset::js(\Arr::merge(array(
                'jquery-1.11.1.min.js',
                //'jquery-migrate-1.0.0.js',
                //'jquery.mousewheel-3.0.6.pack.js',
                'fancybox/jquery.fancybox.js',
                'sys.js',
                '3dParty/jquery.fitvids.js', 
                '3dParty/isotope.js', 
                'pi.init.fitvids.js', 
                'pi.init.isotope.js',
                'pi.fixedHeader.min.js',
                'site.js'
                //'pi.mobileMenu.js'
                    ), !empty($js) ? $js : array()));
    ?>

    <script type="text/javascript">
      $(function()
      {
        sys.init();
        $('.fancy').fancybox({
          padding : 5
        });
      });
    </script>

  </head>
  <body>
    <div id="pi-all">
      <div class="pi-header">
        <?= \Util\Component::get('header', $current_language); ?>
      </div>
      <div id="page">
        <?= $content; ?>
      </div>
      <?= \Util\Component::get('footer', $current_language); ?>
    </div>
    <script>
      $(function()
      {
        $(window).load(function()
        {
          sys.fixes();
        });
      });
    </script>
  </body>
</html>