<?php
$logo = null;
if (!empty($comp['files']['logo'])) {
    $_logo = current($comp['files']['logo']);
    if (!empty($_logo[0]))
        $logo = \Uri::create($_logo[0]);
}

$bg_image = null;
if (!empty($comp['files']['bg_img'])) {
    $_bgimg = current($comp['files']['bg_img']);
    if (!empty($_logo[0]))
        $bg_image = \Uri::create($_bgimg[0]);
}

//echo '<pre>'.print_r($comp,1).'</pre>';
?>

<div class="pi-section-w pi-section-base">
    <div class="pi-section pi-padding-bottom-20 pi-padding-top-20">
        <div class="pi-row">
            <div class="pi-col-md-3">
                <?php 
                if (strlen($comp['value']['title_1']) > 0) { ?>
                    <h1 class="h4 pi-weight-700 pi-uppercase pi-text-dark"><?= $comp['value']['title_1']; ?></h1>
                <?php
                }
                ?>
                <?php 
                if (strlen($comp['value']['title_2']) > 0) { ?>
                    <h1 class="h4 pi-weight-700 pi-uppercase pi-text-dark"><?= $comp['value']['title_2']; ?></h1>
                <?php
                }
                ?>

            </div>
            <div class="pi-col-md-6 pi-text-center">
                <?php if ($bg_image): ?>
                    <a href="<?= \Uri::create($current_language->val); ?>" title="<?= $main_translation->get('page_name', 'page_name'); ?>" class="logo">
                        <?=
                        \Asset::img($bg_image, array(
                            'alt' => $main_translation->get('page_name', 'page_name')
                        ));
                        ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="pi-col-md-3 pi-text-right pi-hidden-xs">
                <?php if ($logo): ?>
                    <?=
                    \Asset::img($logo, array(
                        'alt' => ""
                    ));
                    ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="pi-header-sticky">
    <div class="pi-section-w pi-section-dark">
        <div class="pi-section pi-row-sm">
            <div class="pi-row-block pi-row-block-txt pi-hidden-xs"><?= $translation->get('share', 'Dalies'); ?>:</div>
            <div class="pi-row-block pi-hidden-xs">
                <ul class="pi-social-icons pi-colored-bg pi-active-bg pi-small pi-round-corners pi-clearfix">
                    <?php if (!empty($comp['value']['draugiem_url'])): ?>
                        <li>
                            <a href="javascript:void(0);" <?= \Util\Sys::share('draugiem', $this->current_page); ?>>
                                <?=
                                \Asset::img(\Uri::create('assets/img/web/dr.jpg'), array(
                                    'alt' => ''
                                ));
                                ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (!empty($comp['value']['facebook_url'])): ?>
                        <li>
                            <a href="javascript:void(0);" <?= \Util\Sys::share('facebook', $this->current_page); ?>>
                                <?=
                                \Asset::img(\Uri::create('assets/img/web/f.jpg'), array(
                                    'alt' => ''
                                ));
                                ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (!empty($comp['value']['odnaklasniki_url'])): ?>
                        <li>
                            <a href="javascript:void(0);" <?= \Util\Sys::share('odnakalsniki', $this->current_page); ?>>
                                <?=
                                \Asset::img(\Uri::create('assets/img/web/ok.jpg'), array(
                                    'alt' => ''
                                ));
                                ?>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="pi-row-block pi-pull-right">
                <ul class="pi-menu pi-has-hover-border pi-items-have-borders pi-full-height">
                    <?php foreach ($main_menu as $menu): ?>
                        <li><a href="#<?= $menu['slug']; ?>"><span><?= $menu['title']; ?></span></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>