var scroll = window.scroll = {

	c: [],

	wait: false,
	
	updateScrollConfig: function()
	{
		var $this = this;
		$('.scroller').each(function(id)
		{
			var $e = $(this);
			if(!$e.is('.inited'))
			{
				$e.attr('data-id', id);
				
				var $scoll_item = $e.find('.scroll-item'),
					count =  $scoll_item.length;
				if(count > 1)
				{				
					var w = $($scoll_item[0]).width() + parseInt($($scoll_item[0]).css('paddingLeft')) + parseInt($($scoll_item[0]).css('paddingRight')),
						$scroll = $e.find('.scroll');
					
					$scroll.css({
						'width' : (w * count)
					});
					
					$this.c[id] = {
						'id' : id,
						'$e' : $e,
						'$scroll' : $scroll,
						'count' : count,
						'current' : 0,
						'w' : w,
						'isAuto' : ($e.attr('data-auto') ? true : false)
					};
					
					if($this.c[id].isAuto)
					{
						for(i = 0; i < 3; i++)
						{
							var $item_clone = $($scoll_item[i]).clone();
							$item_clone.addClass('clone');
							$this.c[id].$scroll.append($item_clone);
						}
						$this.c[id].cOffset = $this.c[id].count + 3;
						$this.c[id].$scroll.css({
							'width' : ($this.c[id].cOffset * 34)+'%'
						});
					}
					
					
					if($e.attr('data-controller') == 'dots')
					{
						$this.setControllers($e, count);
					}
					else if($e.attr('data-controller') == 'arrows')
					{
						$this.setArrowControllers($e, id);
					}
					
					
				}
			}
		});
	},
	
	setArrowControllers: function($e, scroll_id)
	{
		var dirs = ['prev', 'next'],
			dir;
		for(k in dirs)
		{
			var $item = $("<div/>");
			dir = dirs[k];
			$item.addClass('arrow-controller '+dir);
			$e.append($item);
		}
		var $wrapper = $("<div/>");
		$wrapper.addClass('wrapper');
		$e.find('.scroll').wrap($wrapper);
		
		var per_frame = parseInt($e.attr('data-per-frame'));
		if(isNaN(per_frame) || this.c[scroll_id].count < per_frame)
		{
			$e.find('.arrow-controller').remove();
			return false;
		}
		
		this.c[scroll_id].w = (this.c[scroll_id].w * per_frame);
		
		this.c[scroll_id].count = Math.ceil(this.c[scroll_id].count / per_frame);
		

	},
	
	setControllers: function($e, count)
	{
		var $controller_holder = $("<div/>");
		$controller_holder.addClass('frame-switcher');
	
		for(i = 0; i < count; i++)
		{
			var $item = $("<div/>");
			$item.addClass('switch');
			if(i == 0) $item.addClass('active');
			$item.attr('data-key', i);
			$controller_holder.append($item);
		}	
		
		$e.append($controller_holder);
	
	},
	
	scrollTo: function(scroll_id, position_id)
	{
		if(!this.c[scroll_id]) return false;
		this.wait = true;
		
		var c = this.c[scroll_id];
		c.current = position_id;
		
		c.$scroll.animate({
			'left' : (c.w * position_id) * -1
		}, function()
		{
			scroll.wait = false;
			if(c.current >= c.count)
			{
				c.$scroll.css({
					'left' : 0
				});
				c.current = 0;
			}			
		});

	},
	
	switcherListener: function($elem)
	{
		if(this.wait) return false;
		
		var $switch_holder = $elem.parents('.frame-switcher:first'),
			$scroller = $switch_holder.parents('.scroller:first');
	
		$switch_holder.find('.switch').removeClass('active');
		$elem.addClass('active');
		
		this.scrollTo($scroller.attr('data-id'), $elem.attr('data-key'));
	},
	
	arrowSwitcherListener: function($elem)
	{
		if(this.wait) return false;
		
		var $scroller = $elem.parents('.scroller:first'),
			scroll_id = $scroller.attr('data-id'),
			dir = $elem.is('.prev') ? -1 : 1;	
	
		if(!this.c[scroll_id]) return false;
		var c = this.c[scroll_id],
			next = c.current + dir;
		
		if(dir == -1 && next < 0) return false;
		if(dir == 1 && next == c.count) return false;

		this.scrollTo(scroll_id, next);
	},
	
	initSwitcherListener: function()
	{
		$("body").on('click', '.switch', function()
		{
			scroll.switcherListener($(this));
		});	
	},
	
	initArrowSwitcherListener: function()
	{
		$("body").on('click', '.arrow-controller', function()
		{
			scroll.arrowSwitcherListener($(this));
		});	
	},
	
	autoScroll: function()
	{
		var $this = this;
		setInterval(function()
		{
			for(k in $this.c)
			{
				var c = $this.c[k],
					next = c.current + 1;
				if(!c.isAuto) continue;
				
				scroll.scrollTo(k, next);

			}	
		}, 5000);
	},
		
	init: function()
	{
		this.updateScrollConfig();
		this.initSwitcherListener();
		this.initArrowSwitcherListener();
		this.autoScroll();
	}
	
};
$(function(){scroll.init();});