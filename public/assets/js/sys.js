var sys = window.sys = {
    setBtnListener: function ()
    {
        $("body").on('click', '.actionBtn', function ()
        {
            return sys.btnListener($(this));
        });
    },
    btnListener: function ($btn)
    {
        switch ($btn.attr('data-action'))
        {
            case 'submit':
                return 1;
                break;
        }
    },
    fixes: function ()
    {

    },
    init: function ()
    {
        this.setBtnListener();
    }

};


$(function(){
    $('body').on('click', '.singleCat', function(){
        $('.fancy').each(function(){
           $(this).attr('rel', $(this).attr('data-rel'));
        }); 
    })
    
    $('body').on('click', '#allCat', function(){
        $('.fancy').each(function(){
           $(this).attr('rel', 'all');
        }); 
    })
});