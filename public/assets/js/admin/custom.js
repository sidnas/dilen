//Timer for messages and tasks
var i = 3, j=5, k=9;

function incrementI() {
  i++;  
  if(document.getElementById('quickMessages'))
  {
	document.getElementById('quickMessages').innerHTML = i;
  }
}
setInterval('incrementI()', 5000);

function incrementJ() {
  j++;
  if(document.getElementById('quickAlerts'))
  {
	document.getElementById('quickAlerts').innerHTML = j;
	}
}
setInterval('incrementJ()', 12000);

function incrementK() {
  k++;
  if(document.getElementById('quickShop'))
  {
	document.getElementById('quickShop').innerHTML = j;
	}
}
setInterval('incrementK()', 9000);

function showMsg(msgs, status)
{
	var msg = '';
	if(typeof(showMsg) == 'object')
	{
		for(k in msgs) msg += msgs[k]+"<br/>";
	}
	else
	{
		msg = msgs;
	}

	if(status == 'ok')
	{
		alertify.success(msg);
	}
	else if(status == 'error')
	{
		alertify.error(msg);
	}
	else
	{
		alertify.log(msg);
	}	
}
function elemSorting(parent_elem, elem)
{
	$(parent_elem).sortable({
		connectWith: parent_elem,
		stop: function(event, ui) 
		{
			$(parent_elem).each(function()
			{	
				var order = 1;
				$(parent_elem).find('tr').each(function()
				{					
					if($(this).find('.order').length > 0)
					{
						$(this).find('.order').val(order++);
					}
				});
			});
		}
	});
}

$(function() 
{
		try
		{
	  $(".color-picker").minicolors({
		control: $(this).attr('data-control') || 'hue',
		defaultValue: $(this).attr('data-defaultValue') || '',
		inline: $(this).attr('data-inline') === 'true',
		letterCase: $(this).attr('data-letterCase') || 'lowercase',
		opacity: $(this).attr('data-opacity'),
		position: $(this).attr('data-position') || 'bottom left',
		change: function(hex, opacity) {
		  if( !hex ) return;
		  if( opacity ) hex += ', ' + opacity;
		  try {
			console.log(hex);
		  } catch(e) {}
		},
		theme: 'bootstrap'
	  });
	}catch(err){console.log(err)}

	$('.fancy').fancybox();
	$( ".date-picker" ).datetimepicker({
		'timepicker' : false,
		'format' : 'd.m.Y'
	});
	
	$(".date-time-picker").datetimepicker({
		'timepicker' : true,
		'format' : 'd.m.Y H:i:00',
		'step' : 5
	});
	
	//"option", "", $( this ).val()
	try
	{
			CKEDITOR.on( 'instanceReady', function( evt ) 
			{
				var editor = evt.editor;
			})
	}
	catch(err){
		console.log(err);
	}
	
	$(".uploadify").each(function()
	{	
		var elem_id = '#'+$(this).attr('id');
		$(this).addClass('fa fa-upload');
		$(elem_id).uploadifive({
			'auto'             : true,
			'formData'         : {
				'template_field_id' : $(elem_id).attr('data-template_field_id'),
				'content_id' : $(elem_id).attr('data-template_content_id'),
				'key_name' : $(elem_id).attr('data-template_key_name')
			},
			'onInit'   : function(instance) 
			{
			
			
			},
			'onUpload' : function(filesToUpload)
			{
				
			},
			'onUploadComplete': function(file, data)
			{
				
				var $tpl =  $("#template-field"),
					r = $.parseJSON(data);
				if(r.file)
				{
					$('#files-'+$(elem_id).attr('data-template_key_name')).jqoteapp($tpl, r.file);
					if(file.queueItem) $(file.queueItem[0]).remove();
				}

				

			},
			'uploadScript'     : config.base+'upload?language_id='+language_id,
			'height'        : 30,
			'width'         : 120
		});
		$(".uploadifive-button").addClass('fa fa-upload');
		$(".uploadifive-button").css({
			'fontSize' : 16+'px'
		});
	});
});

$("body").on('click', '.customAction', function()
{
	switch($(this).attr('data-action'))
	{
		case 'deleteFile':
			return content.deleteFile($(this));
		break;
		case 'editFile':
			editFile($(this).attr('data-key-name'), $(this).attr('data-content_field_file_id'));
		break;
		case 'saveFileData':
			saveFileData($(this));
		break;
		case 'fileToOtherLangs':
			return content.fileToOtherLangs($(this));
		break;
		case 'removeStaticTranslation':
			return content.removeStaticTranslation($(this));
		break;
		case 'saveStaticData':
			return content.saveStaticData($(this));
		break;
		case 'enableInputs':
			return option.enableInputs($(this));
		break;		
		case 'createValueFields':
			return option.createValueFields($(this));
		break;
		case 'deleteValue':
			return option.deleteValue($(this));
		break;
		case 'removeFilterValues':
			return content.removeFilterValues($(this));
		break;
	}
});

function saveFileData($btn)
{
	$.post($btn.parents('form:first').attr('action')+'?language_id'+language_id, 
	{
		'data' : $btn.parents('form:first').serialize(),
		'content_file_id' : $btn.attr('data-id'),
		'action' : 'editFileData'
	}, function(r)
	{
		if(r.custom_value)
		{
			content_files[$btn.attr('data-key_name')][$btn.attr('data-id')].data.custom_value = r.custom_value;
			showMsg(r.msg, 'ok');
		}
		else
		{
			showMsg(r.msg, 'error');
		}

	}, 'json');
}

function editFile(key_name, content_field_file_id)
{	
	if(!templates_file_custom_fields[key_name]) return false;
	if(!content_files[key_name][content_field_file_id]) return false;
	
	var tpl_data = templates_file_custom_fields[key_name],
		file_data = content_files[key_name][content_field_file_id];
	
	var tmp_values = {};
	for(k in tpl_data)
	{
		var value = "";
		console.log("var value = file_data.data.custom_value."+tpl_data[k].name);
		try{
			eval("var value = file_data.data.custom_value."+tpl_data[k].name);
			if(value == undefined) value = "";
		}
		catch(error){}

		tmp_values[k] = {
			value : value,
			title : tpl_data[k].title,
			name : tpl_data[k].name,
			type : tpl_data[k].type
		};

	}	

	var $formGroup = $("<div/>"),
		$label = $("<label/>"),
		$inputHolder = $("<div/>"),
		$input = $("input");
		
	$label.addClass('col-md-2 control-label');
	$inputHolder.addClass('col-md-2');	
	$formGroup.addClass('');
	$input.addClass('form-control');	

	var html = '';
	
	for(k in tmp_values)
	{
		html += '<div class="form-group">';
			html += '<label class="col-md-2 control-label">'+tmp_values[k].title+'</label>';
			html += '<div class="col-md-8">';

			if(tmp_values[k].type == 'textarea')
			{
				html += '<textarea class="form-control" name="file['+tmp_values[k].name+']">'+tmp_values[k].value+'</textarea>';
			}
			else
			{
				html += '<input class="form-control" name="file['+tmp_values[k].name+']" value="'+tmp_values[k].value+'"/>';
			}			
			html += '</div>';
		html += '</div>';

	}
	$("#myModal").find('.modal-body').html(html);
	$("#fileModalTrigger").trigger('click');
	
	$("#saveFileData").attr('data-id', content_field_file_id);
	$("#saveFileData").attr('data-key_name', key_name);
	return false;

}


