var template = window.template = {
	
	counter: -99999,
	
	options: {},
	
	btnListenerEvent: function()
	{
		$("body").on("click", ".customAction", function()
		{
			return template.btnListener($(this));
		});
	},
	
	btnListener: function($btn)
	{
		switch($btn.attr('data-action'))
		{
			case 'addField':
				return this.addField();
			break;
			case 'removeField':
				return this.removeField($btn);
			break;
		
		}

		return false;
	},
	
	
	current_field_id: false,
	addField: function()
	{
		var $tpl = $("#template-field"),
			temp_id = this.counter++,
			params = {
				'id' : temp_id,
				'order' : ($('#custom-fields').find('tr').length + 1)
			};
		this.current_field_id = temp_id;
		$('#custom-fields').jqoteapp($tpl, params);

		return false;
	},
	
	createFilterField: function($this)
	{
		var $this = $($this),
			value_id = $this.val(),
			option = {};
		
		if(!options[value_id]) return false;
		
		option = options[value_id];
		
		this.addField();
		
		var $tr = $("tr[data-id='"+this.current_field_id+"']");
		
		var $key_name = $tr.find("input[name='fields["+this.current_field_id+"][key_name]']"),
			$input_name = $tr.find("input[name='fields["+this.current_field_id+"][name]']"),
			$filter = $tr.find("input[name='fields["+this.current_field_id+"][filter_id]']"),
			$type = $tr.find("select[name='fields["+this.current_field_id+"][type]']");

		$key_name.val(option.input_name);
		$key_name.prop('readonly', true);
		
		$input_name.val(option.option_langs.name_placeholder);
		
		$filter.val(value_id);
		$type.find("option[value='filter']").prop('selected', true);
		$type.hide();
		$type.parents('td').append('---');
		


	},
	
	removeField: function($btn)
	{
		$btn.parents('tr:first').remove();
		return false;
	},
	
	init: function()
	{
		this.btnListenerEvent();
		elemSorting('#custom-fields', '.form-group');
	}

};

$(function()
{
	template.init();
});