var login = window.login = {
	
	wait: false,
	
	submitInit: function()
	{
		
		$("form input[name='Login']").click(function()
		{
			if(login.wait)
			{
				showMsg(lang.please_wait);
				return false;
			}
			login.wait = true;		
			
			var $form = $(this).parents('form:first'),
				$username = $form.find("input[name='username']"),
				$password = $form.find("input[name='password']");

			$.post($form.attr('action'), 
			{
				'username' : $username.val(),
				'password' : ($password.val() != "") ? CryptoJS.MD5($password.val()).toString() : ""
			}, 
			function(r)
			{
				if(r.c && r.c > 0)
				{
					showMsg(r.msgs, 'ok');
					window.location.reload();
					return false;
				}
				login.wait = false;
				if(r.msgs) showMsg(r.msgs, 'error');
			}, 'json');
			
			
			
			return false;
		});
	},
	
	init: function()
	{
		this.submitInit();
	}
};
$(function()
{
	login.init();
});