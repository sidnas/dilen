var sys = window.sys = {

	wait: false,
	loadParentValues: function($select)
	{
		var $select = $($select),
			value_id = $select.val(),
			is_multiple = false,
			selected_length = $select.find('option:selected').length;
			
		if(this.wait) return false;
		//this.wait = true;

		$select.nextAll('.clone').remove();
		
		
		var value_id = $select.val();

		if(parseInt($select.attr('size')) > 0) is_multiple = true;
		

			var $option = $select.find('option:selected');

			$.post(config.web_ajax+'load_parent_values/'+value_id, {
				'filter_id' : $select.attr('data-filter_id')
			}, function(r)
			{
				if(r.c && r.c > 0)
				{
					var $new_select = $select.clone(),
						option = '<option value="0" '+(is_multiple ? ' disabled="disabled"' : '')+'>'+(is_multiple ? $option.text() : '---')+'</option>';
						$new_select.find('option').remove();
					 $new_select.addClass('clone');
					
					$new_select.attr('data-parent_id', value_id);

					if(r.values)
					{
						var is_chosen = '.chosen';
						$select.parents('.filters:first').find("select").each(function(k)
						{
							if($(this).val() > 0) is_chosen += '-'+$(this).val();
						});
					
						for(k in r.values)
						{
							var value = r.values[k],
								disabled = "";
							if($(is_chosen+'-'+value.id).length > 0) disabled = ' disabled="disabled"';
							option += '<option value="'+value.id+'" '+disabled+'>'+value.option_value_langs.value_placeholder+'</option>';
						}

						$new_select.append(option);
						$select.after($new_select);
						return true;
					}

				}
				else
				{
					if(is_multiple)
					{
						var filter_id = $select.attr('data-filter_id'),
							$input_target = $("#filter-"+filter_id),
							$input_holder = $("<div/>"),
							klass = 'filter-value chosen';
						
						var name = '',
							priority = 1;
						$select.parents('.filters:first').find("select").each(function(k)
						{
							var $input = $("<input/>");
							$input.attr('type', 'hidden');
							$input.attr('name', 'filter['+filter_id+'][value][]');
							$input.val($(this).val());
							$input_holder.append($input);
							name += (k ? ' - ' : '')+$(this).find('option:selected').text();
							klass += '-'+$(this).val();
							
							var $priority = $("<input/>");
							$priority.attr('type', 'hidden');
							$priority.attr('name', 'filter['+$select.attr('data-filter_id')+'][priority][]');
							$priority.val(priority++);							
							$input_holder.append($priority);
							
						});
						
						if($('.'+klass).length > 0) return false;
						
						if($select.is('.clone')) $option.prop('disabled', true);
						
						$input_holder.addClass(klass);
						
						

												
						
						var $a = $("<a/>");
						$a.attr('href', 'javascript:;');
						$a.attr('onClick', 'sys.removeFilterValue(this, '+$option.val()+')');
						$a.html(' &nbsp; /'+lang._delete);

						$input_holder.append(name);
						$input_holder.append($a);
						$input_target.append($input_holder);

					}
				}	

				// $select.removeAttr('onChange');

			}, 'json');

	},
	
	showHideChilds: function($check, fields)
	{
		if(fields == "") return false;
		var field_ids = fields.split(','),
			$elems = {};
		for(k in field_ids)
		{
			var $elem = $("#sub_field_"+field_ids[k]);
			if($($check).is(':checked'))
			{
				$elem.show();
			}
			else 
			{
				$elem.hide();
			}
		}
		
		
	
	
	
	
	},

	updateCheckboxRelative: function($checkbox)
	{
		var $ch = $($checkbox),
			$holder = $ch.parents('.checkbox-holder:first'),
			$hidden = $holder.find("input[type='hidden']");
		$hidden.val(($ch.is(':checked') ? 1 : 0));	
	},

	removeFilterValue: function($this, option_value_id)
	{
		$($this).parents('.filter-value').remove();
		$("option[value='"+option_value_id+"']").prop('disabled', false);

	},

};