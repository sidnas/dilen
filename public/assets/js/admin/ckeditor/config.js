/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

	
CKEDITOR.editorConfig = function( config ) {
	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	
	config.toolbarGroups = [
		{ name: 'document',	   groups: [ 'mode'] },	
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },		
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },		
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'others' },
		{ name: 'paragraph',   groups: [ 'list', 'align'] },
		{ name: 'styles' },
		{ name: 'colors' },		
		{ name: 'about' }
	];	
	
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	
	CKEDITOR.plugins.load('pgrfilemanager');
	
	
	
};
