var option = window.option = {

	tmp_id: -99999,
	
	enableInputs: function($btn)
	{
		var $inputHolder = $btn.parents('.input-holder:first');
		$inputHolder.find('input').prop('disabled', false);
		$inputHolder.find('select').prop('disabled', false);
		$btn.remove();
	},
	
	createValueFields: function($btn)
	{
	
		var $tpl = $("#value-field"),
			tmp_id = this.tmp_id++,
			params = {
				'id' : tmp_id,
				'parent_id' : $btn.attr('data-parent_id')
			
			};
		
		$("#content-data").jqoteapp($tpl, params);
		
		$("#content-"+tmp_id).find("a[data-action='enableInputs']").trigger('click');
		
		var $cont = $("#content-"+tmp_id);

		$cont.find('.open_parent').html('-');

	},

	deleteValue: function($btn)
	{
		var value_id = $btn.attr('data-id'),
			value_id = parseInt(value_id);
		
		if(isNaN(value_id)) return;
	
		alertify.confirm($btn.attr('data-quest'), function (e) 
		{
			if (e)
			{		
				showMsg(lang.action_ok, 'ok');
				$("#content-"+value_id).animate({
					'opacity' : 0
				}, 200, function()
				{
					$(this).remove();
				});		
				
				if(value_id < 1) return;
				
				
				$.post($("#content-form").attr('action'), {
					'action' : 'deleteValue',
					'value_id' : value_id
				}, function(r)
				{				
					if(r && r.c > 0)
					{
						showMsg(r.msg, 'ok');
						return;
					}
				
					showMsg(r.msg, 'ok');
				}, 'json');

			} else {
				showMsg(lang.action_canceled, "error");
			}	
	
		});
	},
	

	ordering: function()
	{
	
		$("#content-data").sortable({
			connectWith: "tr",
			stop: function(event, ui) 
			{
				
				$("tr").each(function() {
					result = "";
					$(this).find("tr").each(function(){
						result += $(this).text() + ",";
					});
				});

				var node_ids = {};
				$("#content-data").find('tr').each(function(k)
				{
					node_ids[k] = $(this).attr('data-node-id');
				});
				
				$.post($("#content-form").attr('action'), {
					'action' : 'updateValueOrder',
					'nodes' : node_ids
				}, function(r)
				{
					showMsg(r.msg, 'ok');
				}, 'json');

			}
		});	
	
	},

	enableInputs: function()
	{
		$("body").on('click', '.input-holder', function()
		{
			var $tr = $(this);
			$tr.find('input').prop('disabled', false);
		});
		
	
	},
	
	init: function()
	{
		this.ordering();
		this.enableInputs();
	}
};

$(function(){option.init();});