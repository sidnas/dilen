var content = window.content = {



	ordering: function()
	{
	
		$("#content-data").sortable({
			connectWith: "tr",
			stop: function(event, ui) 
			{
				
				$("tr").each(function() {
					result = "";
					$(this).find("tr").each(function(){
						result += $(this).text() + ",";
					});
				});
				var node_ids = {};
				$("#content-data").find('tr').each(function(k)
				{
					node_ids[k] = $(this).attr('data-node-id');
				});
				
				$.post($("#content-form").attr('action'), {
					'action' : 'updateNoderOrder',
					'nodes' : node_ids
				}, function(r)
				{
					showMsg(r.msg, 'ok');
				}, 'json');

			}
		});	
	
	},
	
	deleteContent: function()
	{
		$(".delete-content").on('click', function()
		{
			var $btn = $(this);

			alertify.confirm($btn.attr('data-quest'), function (e) 
			{
				if (e)
				{
					var $form = $btn.parents('form:first');
					$.post($form.attr('action'), {
						'action' : 'deleteContent',
						'content_id' : $btn.attr('data-id')
					}, function(r)
					{
						if(r && r.c > 0)
						{
							showMsg(r.msg, 'ok');
							$("#content-"+$btn.attr('data-id')).animate({
								'opacity' : 0
							}, 200, function()
							{
								$(this).remove();
							});
						}
					}, 'json');
					
				} else {
					showMsg(lang.action_canceled, "error");
				}
			});
			return false;

		});
	},
	
	deleteFile: function($btn)
	{
		var $form = $btn.parents('form:first'),	
			file_id = $btn.attr('data-file-id');
		
	$.post($form.attr('data-action'), {
		'action' : 'deleteFile',
		'file_id' : file_id
	}, function(r)
	{
		if(r.c && r.c > 0)
		{
			showMsg(r.msg, 'ok');
			$("#file-"+file_id).animate({
				'opacity' : 0
			}, 500, function()
			{
				$(this).remove();
			});
			return false;
		}
		showMsg(lang.some_error, 'error');
	}, 'json');
	
	},
	
	fileToOtherLangs: function($btn)
	{
		var $form = $btn.parents('form:first'),
			file_id = $btn.attr('data-file-id'),
			template_field_id = $btn.attr('data-template_field_id'),
			content_value_id = $btn.attr('data-content_value_id');
		
		$.post($form.attr('action'), {
			'action' : 'fileToOtherLangs',
			'file_id' : file_id,
			'template_field_id' : template_field_id,
			'content_value_id' : content_value_id
			
		}, function(r)
		{
			if(r.c && r.c > 0)
			{
				showMsg(r.msg, "ok");
				return false;
			}
			showMsg(r.msg, "error");
		},'json');
	
	
	},

	staticTranslations: function()
	{
		$("#staticTranslations").click(function()
		{
			if($("#translation-pop").length > 0)
			{
				$("#translationModal").trigger('click');
				return false;
			}
			$.post($(this).attr('href'), {
				'action' : 'showStaticTranslations'
			}, function(r)
			{
				$("body").append(r);
				$("#translationModal").trigger('click');
			});
			return false;
		});
	},
	
	removeStaticTranslation: function($btn)
	{
		
		var key = $btn.attr('data-key');
		
		$.post($btn.attr('href'), {
			'key' : key,
			'action' : 'deleteStaticData'
		}, function(r)
		{
			if(r.c && r.c > 0)
			{
				showMsg(r.msg, "ok");
				$btn.parents('.form-group:first').remove();
				return false;
			}
			showMsg(r.msg, "error");
		}, 'json');		
		return false;
	},
	
	saveStaticData: function($btn)
	{	
		var $form = $btn.parents('form:first'),
			values = {};
		
		$form.find('.key').each(function()
		{
			values[$(this).attr('data-key')] = $("*[name='static_value["+$(this).attr('data-key')+"]']").val();
		});
		
		$.post($form.attr('action'), {
			'values' : values,
			'action' : 'updateStaticData'
		}, function(r)
		{
			if(r.c && r.c > 0)
			{
				showMsg(r.msg, "ok");
				return false;
			}
			showMsg(r.msg, "error");
		}, 'json');

	},
	
	removeFilterValues: function($btn)
	{
		var $label = $btn.parents('.label'),
			ids = [$btn.attr('data-id')];
		$label.css({
			'opacity' : 0.5
		});
		var ids = this.findChildValues($label, ids),
			$base_node = $btn.parents('.base-tree-node:first'),
			is_multi_select = ($base_node.is('.select') ? true : false);

		if(ids.length > 0)
		{
			if(this.wait) return false;
			this.wait = true;
			$.post($btn.parents('form:first').attr('action'), {
				'action' : 'deleteFilterValues',
				'ids' : ids
			}, function(r)
			{
				content.wait = false;
				if(r.c && r.c > 0)
				{
					$btn.parents('.label:first').next('.value-tree-holder').remove();
					$btn.parents('.label:first').remove();
					showMsg(r.msg, "ok");
					
					if(!is_multi_select)
					{
						if($base_node.find('.label').length == 0)
						{					
							var $form_holder = $base_node.parents('.filter-drops:first');
							$form_holder.find('.filters').find('select').show();
							return true;
						}
					}
					
					return true;					
				}
				showMsg(r.msg, "error");
			}, 'json');
			
			

		}

		return false;
	},
	
	findChildValues: function($label, ids)
	{
		var $deletables = $label.next('.value-tree-holder').find('.delete'),
			i = 1,
			elems = [];
		
		$deletables.each(function(k)
		{
			ids[i++] = $(this).attr('data-id');
		});
		return ids
		
	},
	
	init: function()
	{
		this.ordering();
		this.deleteContent();
		this.staticTranslations();
	}
};

$(function()
{
	content.init();
});